#!/bin/bash

docker stats \
    default-elasticsearch \
    default-logstash \
    default-kibana \
