### Postfix

Check whether your IP address is blacklisted: [https://mxtoolbox.com](https://mxtoolbox.com)    
If it is then attempting to send emails to outside mail servers like Gmail will fail and an error could be noticed in the logs which looks like this:
```
4DAA02AD202: lost connection with gmail-smtp-in.l.google.com[66.102.1.26] while receiving the initial server greeting
```
