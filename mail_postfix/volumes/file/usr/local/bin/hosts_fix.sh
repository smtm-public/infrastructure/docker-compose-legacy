#!/bin/bash

#the hosts file for postfix is /var/spool/postfix/etc/hosts
cp -f /etc/hosts /var/spool/postfix/etc/hosts

if grep "${COMPOSE_PROJECT_NAME:-default}-mysql" /var/spool/postfix/etc/hosts >/dev/null
then
    echo "${COMPOSE_PROJECT_NAME:-default}-mysql entry found."
else
    echo "${COMPOSE_PROJECT_NAME:-default}-mysql entry not found... adding..."
    echo `getent hosts "${COMPOSE_PROJECT_NAME:-default}-mysql"` >> /var/spool/postfix/etc/hosts
fi

#next if is not required.
if grep "${COMPOSE_PROJECT_NAME:-default}-mail_dovecot" /var/spool/postfix/etc/hosts > /dev/null
then
    echo "${COMPOSE_PROJECT_NAME:-default}-mail_dovecot entry found."
else
    echo "${COMPOSE_PROJECT_NAME:-default}-mail_dovecot entry not found... adding..."
    echo `getent hosts "${COMPOSE_PROJECT_NAME:-default}-mail_dovecot"` >> /var/spool/postfix/etc/hosts
fi

cp -f /var/spool/postfix/etc/hosts /etc/hosts
