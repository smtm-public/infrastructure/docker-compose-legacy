#!/bin/bash

docker stats \
    default-portainer \
    default-traefik \
    default-apache24 \
    default-php73-fpm \
    default-mariadb \
    default-mysql \
    default-postgres \
    default-mongo \
    default-telegraf \
    default-influxdb \
    default-chronograf \
    default-kapacitor \
    default-redis \
    default-elasticsearch \
    default-logstash \
    default-kibana \
    default-grafana \
    default-mail_postfix \
    default-mail_dovecot \
    default-sftp \
    default-backup_borg \
    default-test \
    default-fluxbox-vnc \
    default-openapi-gui \
    default-openapigenerator \
    default-centos7 \
    default-debian9_stretch \
    default-debian10_buster \
    default-ubuntu18_cosmic \
    default-ubuntu19_eoan \
    default-alpine3 \
    default-nodejs11 \
    default-jsdetox \
    default-nginx-unit1 \
    default-openjdk14 \
