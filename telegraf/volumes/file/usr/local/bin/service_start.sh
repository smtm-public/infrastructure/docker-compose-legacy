#!/bin/bash

# When using environment variables whose value is a string in telegraf.conf the variable names must be enclosed in
# double quotes and then enclosed inside ${} like so:
#   urls = [
#     "${INFLUXDB_URL}"
#   ]
# The variables CANNOT be concatenated with other string literals so the following is invalid and results in a parse
# error:
#   urls = [
#     "http://${COMPOSE_PROJECT_NAME}-influxdb:8086"
#   ]
# That is why the values must be calculated and stored in new auxiliary environment variables.
export INFLUXDB_URL="http://${COMPOSE_PROJECT_NAME}-influxdb:8086"
export INFLUXDB_DEBUG_VARS_URL="http://${COMPOSE_PROJECT_NAME}-influxdb:8086/debug/vars"
export KAPACITOR_DEBUG_VARS_URL="http://${COMPOSE_PROJECT_NAME}-kapacitor:9092/kapacitor/v1/debug/vars"
export APACHE_STATUS_URL="http://${COMPOSE_PROJECT_NAME}-apache24/server-status?auto"
export PHP_FPM_STATUS_URL="http://${COMPOSE_PROJECT_NAME}-apache24/php-fpm-status?full"

export ARGS=$*
rm -f /var/run/supervisor.pid
# Refer to the notes in /etc/supervisor/conf.d/supervisord.conf on the supervisorctl default lookup sock path as it
# varies across supervisor builds on different Linux distros
rm -f /var/run/supervisor.sock
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
#/usr/sbin/sshd && docker-entrypoint.sh $@
