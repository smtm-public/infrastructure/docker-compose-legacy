<?php
if (PHP_SAPI !== 'cli') {
    echo 'This is a command-line utility!' . "\n";
    die();
}
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('max_execution_time', 0);
ini_set('memory_limit', -1);

// Turn off output buffering
ini_set('output_buffering', 'off');
// Turn off PHP output compression
ini_set('zlib.output_compression', false);
// Implicitly flush the buffer(s)
ini_set('implicit_flush', true);
ob_implicit_flush(true);
// Clear, and turn off output buffering
while (ob_get_level() > 0) {
    // Get the curent level
    $level = ob_get_level();
    // End the buffering
    ob_end_clean();
    // If the current level has not changed, abort
    if (ob_get_level() == $level) {
        break;
    }
}

echo '
  ______                                 __       __         ________                    __       __  __                        __                         
 /      \                               |  \     /  \       |        \                  |  \     /  \|  \                      |  \                        
|  €€€€€€\  ______  __     __   ______  | €€\   /  €€  ______\€€€€€€€€______   _______  | €€\   /  €€ \€€ _______   __    __  _| €€_     ______    _______ 
| €€___\€€ |      \|  \   /  \ /      \ | €€€\ /  €€€ /      \ | €€  /      \ |       \ | €€€\ /  €€€|  \|       \ |  \  |  \|   €€ \   /      \  /       \
 \€€    \   \€€€€€€\\\\€€\ /  €€|  €€€€€€\| €€€€\  €€€€|  €€€€€€\| €€ |  €€€€€€\| €€€€€€€\| €€€€\  €€€€| €€| €€€€€€€\| €€  | €€ \€€€€€€  |  €€€€€€\|  €€€€€€€
 _\€€€€€€\ /      €€ \€€\  €€ | €€    €€| €€\€€ €€ €€| €€    €€| €€ | €€    €€| €€  | €€| €€\€€ €€ €€| €€| €€  | €€| €€  | €€  | €€ __ | €€    €€ \€€    \ 
|  \__| €€|  €€€€€€€  \€€ €€  | €€€€€€€€| €€ \€€€| €€| €€€€€€€€| €€ | €€€€€€€€| €€  | €€| €€ \€€€| €€| €€| €€  | €€| €€__/ €€  | €€|  \| €€€€€€€€ _\€€€€€€\
 \€€    €€ \€€    €€   \€€€    \€€     \| €€  \€ | €€ \€€     \| €€  \€€     \| €€  | €€| €€  \€ | €€| €€| €€  | €€ \€€    €€   \€€  €€ \€€     \|       €€
  \€€€€€€   \€€€€€€€    \€      \€€€€€€€ \€€      \€€  \€€€€€€€ \€€   \€€€€€€€ \€€   \€€ \€€      \€€ \€€ \€€   \€€  \€€€€€€     \€€€€   \€€€€€€€ \€€€€€€€ 
                                                                                                                                                           
                                            ------------------------------------------------------------------
                                                        PROJECT START-UP | DOCKER-COMPOSE WRAPPER
                                                                       TOOOL
                                            We pass command-line arguments to docker-compose for you! (mostly)
                                            ------------------------------------------------------------------
                                                                                                                                               
';

if (!function_exists('system')) {
    echo 'Error: The `system` function, upon which this script depends entirely, has been disabled in your PHP configuration. Check the `disable_functions` configuration directive to enable it. http://php.net/manual/en/ini.core.php#ini.disable-functions' . "\n";
    exit(1);
}

$shortoptsNoValue = [];
$shortopts = implode($shortoptsNoValue);
$shortoptsOptionalValue = [];
if (!empty($shortoptsOptionalValue)) {
    $shortopts .= implode('::', $shortoptsOptionalValue) . '::';
}
$shortoptsRequiredValue = ['w', 'q'];
if (!empty($shortoptsRequiredValue)) {
    $shortopts .= implode(':', $shortoptsRequiredValue) . ':';
}
$longoptsNoValue = [];
$longoptsOptionalValue = [];
$longoptsRequiredValue = [];
$longopts =
    array_merge(
        $longoptsNoValue,
        array_map(
            function ($opt) {
                return $opt . '::';
            },
            $longoptsOptionalValue
        ),
        array_map(
            function ($opt) {
                return $opt . ':';
            },
            $longoptsRequiredValue
        )
    );
$longopts = [
    'what:',
    'help',
];
$args = getopt($shortopts, $longopts);
$argsRaw = $_SERVER['argv'];

function usage()
{
    echo "Usage: php hehe.php [[-q <development|staging|production>]|[-f <DOCKER_COMPOSE_YML_FILE>...]] [DOCKER_COMPOSE_OPTIONS] [DOCKER_COMPOSE_COMMAND] [DOCKER_COMPOSE_ARGS...]";
    return 0;
}

putenv('HEHE_ENVIRONMENT=development');
$argsToPass = [];
foreach ($args as $key => $value) {
    if ($key === 'q') {
        putenv('HEHE_ENVIRONMENT=' . $value);
    } else {
        /*
        if (in_array($key, $shortoptsNoValue)) {
            $argsToPass[] = '-' . $key;
        } else {
            if(in_array($key, $shortoptsOptionalValue)) {
                if (is_bool($value)) {
                    $argsToPass[] = '-' . $key;
                } else {
                    $argsToPass[] = '-' . $key . escapeshellarg($value);
                }
            } else {
                if(in_array($key, $shortoptsRequiredValue)) {
                    $argsToPass[] = '-' . $key . escapeshellarg($value);
                } else {
                    if (in_array($key, $longoptsNoValue)) {
                        $argsToPass[] = '--' . $key;
                    } else {
                        if(in_array($key, $longoptsOptionalValue)) {
                            if (is_bool($value)) {
                                $argsToPass[] = '--' . $key;
                            } else {
                                $argsToPass[] = '--' . $key . '=' . escapeshellarg($value);
                            }
                        } else {
                            if(in_array($key, $longoptsRequiredValue)) {
                                $argsToPass[] = '--' . $key . '=' . escapeshellarg($value);
                            }
                        }
                    }
                }
            }
        }
        */
    }
}
// Why not just shift out the first element in the array which SHOULD be the script filename itself? Well, let's take a bit extra care and make sure we discard every argument which might come before hehe.php!
$argsToPass = array_slice($argsRaw, array_search('hehe.php', $argsRaw) + 1);
//$argsToPass = $argsRaw;
foreach($shortoptsNoValue as $opt) {
    $optPos = array_search('-' . $opt, $argsToPass);
    array_splice($argsToPass, $optPos, 0);
}
foreach($shortoptsRequiredValue as $opt) {
    $optPos = array_search('-' . $opt, $argsToPass);
    if ($optPos !== false) {
        array_splice($argsToPass, $optPos, 1);
    }
}

if ((getenv('HEHE_ENVIRONMENT') != "development") && (getenv('HEHE_ENVIRONMENT') != "staging") && (getenv('HEHE_ENVIRONMENT') != "production")) {
    echo "Wrong value for [-q]. Possible values are development (default), staging, production.\n";
    echo "\n";
    usage();
    exit(1);
}


$keysDir = '~/.ssh';
if ((!is_file($keysDir . '/id_rsa') || !is_file($keysDir . '/id_rsa.pub')) && stripos(php_uname(), 'windows') !== false) {
    $keysDir = ($_SERVER['HOMEDRIVE'] ?? 'C:') . ($_SERVER['HOMEPATH'] ?? ('/Users/' . $_SERVER['CURRENT_USER_NAME'])) . '/.ssh';
}
if (!is_file($keysDir . '/id_rsa') || !is_file($keysDir . '/id_rsa.pub')) {
    if (!is_file($keysDir . '/id_rsa')) {
        echo 'Warning: Could not find the current user\'s private key. Some containers will not be able to run `composer install` due to private repo dependencies.' . "\n";
    }
    if (!is_file($keysDir . '/id_rsa.pub')) {
        echo 'Warning: Could not find the current user\'s public key. Some containers will not be able to run `composer install` due to private repo dependencies.' . "\n";
    }
} else {
    $gitAccessServices =
        [
            'cock',
            'webapi',
        ];
    for ($j = 0; $j < count($gitAccessServices); $j++) {
        if (is_file(__DIR__ . '/volumes-shared/directory/.ssh-git/' . $gitAccessServices[$j] . '/id_rsa')) {
            unlink(__DIR__ . '/volumes-shared/directory/.ssh-git/' . $gitAccessServices[$j] . '/id_rsa');
        }
        if (is_file(__DIR__ . '/volumes-shared/directory/.ssh-git/' . $gitAccessServices[$j] . '/id_rsa.pub')) {
            unlink(__DIR__ . '/volumes-shared/directory/.ssh-git/' . $gitAccessServices[$j] . '/id_rsa.pub');
        }
        copy($keysDir . '/id_rsa', __DIR__ . '/volumes-shared/directory/.ssh-git/' . $gitAccessServices[$j] . '/id_rsa');
        copy($keysDir . '/id_rsa.pub', __DIR__ . '/volumes-shared/directory/.ssh-git/' . $gitAccessServices[$j] . '/id_rsa.pub');
        chmod(__DIR__ . '/volumes-shared/directory/.ssh-git/' . $gitAccessServices[$j] . '/id_rsa', '0600');
    }
}

echo 'Checking the hosts file for some required values...' . "\n";
$hostsEntries = [
    'www.google-analytics.com' => '127.0.0.1',
    'google-analytics.com' => '127.0.0.1',
    'ssl.google-analytics.com' => '127.0.0.1',
];
$hostsFilePath = '/etc/hosts';
if (stripos(php_uname(), 'windows') !== false) {
    $hostsFilePath = ($_SERVER['SYSTEMROOT'] ?? 'C:/Windows') . '/system32/drivers/etc/hosts';
}
if (!is_file($hostsFilePath) || !is_readable($hostsFilePath) || !is_writable($hostsFilePath)) {
    if (!is_file($hostsFilePath)) {
        echo 'Warning: Could not find your hosts file at ' . $hostsFilePath . '.' . "\n";
    } else {
        if (!is_readable($hostsFilePath)) {
            echo 'Warning: The hosts file at ' . $hostsFilePath . ' is not readable.' . "\n";
        }
        if (!is_writable($hostsFilePath)) {
            echo 'Warning: The hosts file at ' . $hostsFilePath . ' is not writable.' . "\n";
        }
    }
    echo '         The script will not be able to populate your hosts file automatically.' . "\n";
    echo '         Either run the script with root/administrator privileges or add the following entries to the hosts file manually:' . "\n";
    echo '         ------------------------- ' . $hostsFilePath . ' -------------------------' . "\n";
    echo '         ...' . "\n";
    foreach ($hostsEntries as $url => $target) {
        echo '         ' . $target . '    ' . $url . "\n";
    }
    echo '         ...' . "\n";
    echo '         ---------------------------' . str_repeat('-', strlen($hostsFilePath)) . '-------------------------' . "\n";
} else {
    $hostsFileContent = '';
    if (function_exists('file_get_contents')) {
        $hostsFileContent = file_get_contents($hostsFilePath);
    } else {
        $hostsFile = fopen($hostsFilePath, 'r');
        while (($line = fgets($hostsFile)) !== false) {
            $hostsFileContent = '';
        }
        fclose($hostsFile);
    }
    $hostsEntriesAdd = [];
    foreach ($hostsEntries as $url => $target) {
        if (!preg_match('#' . preg_quote($target) . '\s+' . preg_quote($url) . '#', $hostsFileContent)) {
            $hostsEntriesAdd[] = PHP_EOL . $target . '    ' . $url;
        }
    }
    if (count($hostsEntriesAdd) > 0) {
        $hostsFileContent .= PHP_EOL . '# SMTM Docker DevStack';
        foreach ($hostsEntriesAdd as $hostsEntryAdd) {
            $hostsFileContent .= $hostsEntryAdd;
        }
        $hostsFileContent .= PHP_EOL . '# END: SMTM Docker DevStack' . PHP_EOL;
        $hostsEntriesAdd = array_merge([PHP_EOL . '# SMTM Docker DevStack'], $hostsEntriesAdd, [PHP_EOL . '# END: SMTM Docker DevStack' . PHP_EOL]);

        if (function_exists('file_put_contents')) {
            file_put_contents($hostsFilePath, $hostsFileContent);
        } else {
            $hostsFile = fopen($hostsFilePath, 'a');
            foreach ($hostsEntriesAdd as $hostsEntryAdd) {
                fputs($hostsFile, $hostsEntryAdd);
            }
            fclose($hostsFile);
        }
    }
}

$bashHistoryServices =
    [
        'backup_borg',
        'mail_dovecot',
        'mail_postfix',
        'mysql',
        'sftp',
        'test',
    ];
for ($j = 0; $j < count($bashHistoryServices); $j++) {
    touch(__DIR__ . '/' . $bashHistoryServices[$j] . '/volumes/file/root/.bash_history');
}

if (function_exists('shell_exec')) {
    if (trim(shell_exec('which git'))) {
        $currentDir = getcwd();
        chdir(__DIR__ . '/webapp/content');
        system('git update-index --assume-unchanged config/environment.js');
        chdir($currentDir);
    } else {
        echo 'Warning: Cannot find `git`. `git update-index --assume-unchanged config/environment.js` needs to be executed manually on the webapp to prevent the docker-specific config from being staged.';
    }
} else {
    echo 'Warning: The shell_exec function has been disabled. Check the `disable_functions` configuration directive to enable it. `git update-index --assume-unchanged config/environment.js` needs to be executed manually on the webapp to prevent the docker-specific config from being staged.';
}

$command = 'docker-compose' . (!empty($argsToPass) ? ' ' . implode(' ', $argsToPass) : '');
if (getenv('HEHE_ENVIRONMENT') === "staging") {
    $command = 'docker-compose -f docker-compose.staging.yml' . (!empty($argsToPass) ? ' ' . implode(' ', $argsToPass) : '');
} else {
    if (getenv('HEHE_ENVIRONMENT') === "production") {
        $command = 'docker-compose -f docker-compose.production.yml' . (!empty($argsToPass) ? ' ' . implode(' ', $argsToPass) : '');
    }
}
/*
if (function_exists('proc_open')) {
    $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin
        1 => array("pipe", "w"),  // stdout
        2 => array("pipe", "w"),  // stderr
    );
    $process = proc_open($command, $descriptorspec, $pipes, dirname(__FILE__), null);
    if (is_resource($process)) {
        while ($s = fgets($pipes[1])) {
            print $s;
        }
    }
    proc_close($process);
}
*/
echo 'After stopping the containers you might get a Python exception:' . "\n";
echo <<< EOT
------------------------------------------ EXCEPTION SAMPLE ------------------------------------------
Exception ignored in: <_io.TextIOWrapper name='<stdout>' mode='w' encoding='cp1251'>
OSError: [Errno 22] Invalid argument
------------------------------------------------------------------------------------------------------

EOT;
echo 'This looks like a Python bug and, while it only hangs the docker-compose process, there is little we can do about it: https://bugs.python.org/issue13444' . "\n";
echo 'Executing `' . $command . '`...' . "\n";
// The `system` function streams both STDOUT and STDERR as opposed to `exec` which handles only STDERR
system($command);
exit(0);
