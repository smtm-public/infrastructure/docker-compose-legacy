#!/bin/bash

export DISPLAY=':99'
export XAUTHORITY='/tmp/xvfb-auth.cookie'
export SCREEN_GEOM='1280x960x16'

x11vnc \
    -display "WAIT:cmd=start-xvfb-fluxbox.sh" \
    -noxdamage -auth ${XAUTHORITY} \
    -env X11VNC_FINDDISPLAY_ALWAYS_FAILS=1 \
    -env X11VNC_CREATE_GEOM=${SCREEN_GEOM} \
    -gone 'killall fluxbox && killall Xvfb' \
    -bg -nopw