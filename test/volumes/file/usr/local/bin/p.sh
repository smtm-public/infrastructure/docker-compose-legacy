#!/bin/bash

while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapi 443; do
    echo 'Waiting for the WebAPI to start...'
    sleep 1
done
while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapp 4200; do
    echo 'Waiting for the WebApp to start...'
    sleep 1
done
while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapp 49152; do
    echo 'Waiting for the WebApp to start...'
    sleep 1
done

# Script vars
export PATH="$PATH:/casperjs/bin/"
scriptsDir="/scripts/casperjs"
webappHostPort="16200"
webappContainerPort="4200"
webapiHostPort="13443"
webapiContainerPort="443"
casperEngine="slimerjs"
#if [[ "$casperEngine" = "slimerjs" ]]; then
#    commonCasperjsCommandLineArguments="--engine=slimerjs --debug --headless --proxy-type=none --ssl-protocol=any"
#else
#    commonCasperjsCommandLineArguments="--proxy-type=none --ignore-ssl-errors=true --ssl-protocol=any"
#fi
commonCommandLineArguments="--proxy-type=none --ignore-ssl-errors=true --ssl-protocol=any"

currentDir="$(pwd)"
# for headless PhantomJS
export QT_QPA_PLATFORM=offscreen
# for headless SlimerJS
export MOZ_HEADLESS=1
#export SLIMERJS_EXECUTABLE="/usr/local/bin/slimerjs"
#export ENGINE_FLAGS="--config=/usr/local/lib/node_modules/slimer/config.js"

echo 'PhantomJS version: '$(phantomjs --version)
#echo 'SlimerJS version: '$(slimerjs --version)
#echo 'CasperJS version: '$(/casperjs/bin/casperjs --version)

ssh -T -O "exit" webapp 2>/dev/null >/dev/null
ssh -T -O "exit" webapi 2>/dev/null >/dev/null

echo "Opening an SSH tunnel to the WebApp to forward port ${webappHostPort} to ${webappContainerPort} on the container..."
# Enforcing IPv4 with -4 otherwise an error is thrown:
# bind: Cannot assign requested address
# https://www.electricmonk.nl/log/2014/09/24/ssh-port-forwarding-bind-cannot-assign-requested-address/
command="ssh -f -N -T -M -4 -L ${webappHostPort}:127.0.0.1:${webappContainerPort} webapp"
${command}
if [[ "$?" -eq 0 ]]; then
    echo 'Success!'
else
    echo '`'${command}'` failed with status: '"$?"
    exit 1
fi

echo 'Checking to see if the WebApp is up and running...'
command="phantomjs ${commonCommandLineArguments} ${scriptsDir}/$@"
echo 'Running `'${command}'`...'
${command}
if [[ "$?" -eq 0 ]]; then
    echo 'Seems like...'
else
    echo 'Could not load the landing page. Exiting.'
fi



if [[ -f "/root/.ssh/webapp.ctl" ]]; then
    ssh -T -O "exit" webapp
fi
if [[ -f "/root/.ssh/webapi.ctl" ]]; then
    ssh -T -O "exit" webapi
fi

echo 'All done. Bye!'
