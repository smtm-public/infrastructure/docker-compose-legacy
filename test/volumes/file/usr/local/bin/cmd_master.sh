#!/bin/bash

. wait_for_mariadb.sh
. wait_for_mysql.sh

. wait_for_ssh_keys.sh -s "mysql" -p "3306" -a

. service_reinitialize.sh
. service_start.sh
