#!/bin/bash

#set -e

read -r -d '' splash << 'EOM'

     )                                      (         (
  ( /(     (  (                    *   )    )\ ) *   ))\ )
  )\())  ( )\ )\ )  (            ` )  /((  (()/` )  /(()/(
 ((_)\  ))((_(()/( ))\ (          ( )(_))\  /(_)( )(_)/(_))
  _((_)/((__  ((_)/((_))\ )   _  (_(_()((_)(_))(_(_()(_))
 | || (_))| | _| (_)) _(_/(  (_) |_   _| __/ __|_   _/ __|
 | __ / -_| / _` / -_| ` \))  _    | | | _|\__ \ | | \__ \
 |_||_\___|_\__,_\___|_||_|  (_)   |_| |___|___/ |_| |___/


EOM

# set verbose level to emerg
__VERBOSE=0

declare -A LOG_LEVELS
# https://en.wikipedia.org/wiki/Syslog#Severity_level
LOG_LEVELS=([0]="emerg" [1]="alert" [2]="crit" [3]="err" [4]="warning" [5]="notice" [6]="info" [7]="debug")

function log () {
    local LEVEL=${1}
    shift
    if [ ${__VERBOSE} -ge ${LEVEL} ]; then
        if [ -t 0 ]; then
            # seems we are in an interactive shell
            echo "[${LOG_LEVELS[$LEVEL]}]" "$@" >&2
        else
            # seems we are in a cron job
            logger -p "${LOG_LEVELS[$LEVEL]}" -t "$0[$$]" -- "$*"
        fi
    fi
}

function logPlain () {
    local LEVEL=${1}
    shift
    if [ ${__VERBOSE} -ge ${LEVEL} ]; then
        if [ -t 0 ]; then
            # seems we are in an interactive shell
            echo "$@" >&2
        else
            # seems we are in a cron job
            logger -p "${LOG_LEVELS[$LEVEL]}" -- "$*"
        fi
    fi
}

manageStaleBrowserProcesses() {
    staleChromiumProcesses=$(pgrep -cf /usr/lib/chromium/chromium)
    staleChromeMasterProcesses=$(pgrep -cf /usr/bin/google-chrome)
    staleChromeSubprocesses=$(pgrep -cf /opt/google/chrome/chrome)

    if [[ ${staleChromeMasterProcesses} != 0 || ${staleChromeSubprocesses} != 0 || ${staleChromiumProcesses} != 0 ]]; then
        log 4 $1
        if [[ ${staleChromeMasterProcesses} != 0 ]]; then
            echo 'Stale Chrome master processes: '${staleChromeMasterProcesses}
            echo 'Attempting `pkill -f /usr/bin/google-chrome`...'
            pkill -f /usr/bin/google-chrome
        fi
        if [[ ${staleChromeSubprocesses} != 0 ]]; then
            echo 'Stale Chrome subprocesses: '${staleChromeSubprocesses}
            echo 'Attempting `pkill -f /opt/google/chrome/chrome`...'
            pkill -f /opt/google/chrome/chrome
        fi
        if [[ ${staleChromiumProcesses} != 0 ]]; then
            echo 'Stale Chromium processes: '${staleChromiumProcesses}
            echo 'Attempting `pkill -f /usr/lib/chromium/chromium`...'
            pkill -f /usr/lib/chromium/chromium
        fi
        logPlain 4 ""
    fi

    return 0
}

export DISPLAY=:99
export XAUTHORITY=/tmp/xvfb-auth.cookie

manageStaleX11Processes() {
    staleFluxboxProcesses=$(pgrep -cf /usr/bin/fluxbox)
    staleXvfbProcesses=$(pgrep -cf "Xvfb ${DISPLAY}")
    staleStartX11VncScriptProcesses=$(pgrep -cf start-x11vnc.sh)
    staleX11VncProcesses=$(pgrep -cf "x11vnc -display")

    if [[ ${staleFluxboxProcesses} != 0 || ${staleXvfbProcesses} != 0 || ${staleStartX11VncScriptProcesses} != 0 || ${staleX11VncProcesses} != 0 ]]; then
        log 4 'Found stale X11 related processes.'
        if [[ ${staleFluxboxProcesses} != 0 ]]; then
            log 4 'Stale Fluxbox Processes: '${staleFluxboxProcesses}
            log 4 'Attempting `pkill -f /usr/bin/fluxbox`...'
            pkill -f /usr/bin/fluxbox
        fi
        if [[ ${staleXvfbProcesses} != 0 ]]; then
            log 4 'Stale Xvfb processes: '${staleXvfbProcesses}
            log 4 "Attempting "'`'"pkill -f Xvfb ${DISPLAY}"'`'"..."
            pkill -f "Xvfb ${DISPLAY}"
        fi
        if [[ ${staleStartX11VncScriptProcesses} != 0 ]]; then
            log 4 'Stale start-x11vnc.sh processes: '${staleStartX11VncScriptProcesses}
            log 4 'Attempting `pkill -f start-x11vnc.sh`...'
            pkill -f start-x11vnc.sh
        fi
        if [[ ${staleX11VncProcesses} != 0 ]]; then
            log 4 'Stale x11vnc processes: '${staleX11VncProcesses}
            log 4 'Attempting `pkill -f "x11vnc -display"`...'
            pkill -f "x11vnc -display"
        fi
        logPlain 4 ""
    fi
}

openSshTunnel() {
    logPlain 6 ""
    log 6 "Opening an SSH tunnel to the $1 to forward port $2 to $3 on the container..."
    # Enforcing IPv4 with -4 otherwise an error is thrown:
    # bind: Cannot assign requested address
    # https://www.electricmonk.nl/log/2014/09/24/ssh-port-forwarding-bind-cannot-assign-requested-address/
    command="ssh -f -N -T -M -4 -L $2:127.0.0.1:$3 $4"
    log 6 ${command}
    ${command}
    lastCode=$?
    if [ ${lastCode} -eq 0 ]; then
        log 6 'Success!'
    else
        log 6 '`'${command}'` failed with status: '"${lastCode}"
        exit 1
    fi
    return $?
}

closeSshTunnel() {
    if [[ -S "/root/.ssh/$1.ctl" ]]; then
        ssh -T -O "exit" $1 2>/dev/null
        log 6 "Info: Exit request for the $1 has been sent to its SSH control socket."
        rm -f "/root/.ssh/$1.ctl"
    else
        if [[ -e "/root/.ssh/$1.ctl" ]]; then
            log 6 "Warning: The $1 SSH control socket file is not a socket. Attempting removal..."
            rm -f "/root/.ssh/$1.ctl"
        fi
    fi
}

databaseSelectCount() {
    query="SELECT count(*) FROM $2 WHERE $3"
    # Don't really have the time for this now: https://dev.mysql.com/doc/refman/5.7/en/mysql-config-editor.html
    commandMysql="/usr/local/mysql/bin/mysql -u root -p${MYSQL_ROOT_PASSWORD} -N $1 -e '${query}'"
    commandSsh="ssh mysql ${commandMysql}"

    echo $(${commandSsh})
}

export DEBUG_OUTPUT='0'
export TEST_USER_EMAIL='test'
export TEST_USER_EMAIL_INDEX='11'
export TEST_USER_EMAIL_DOMAIN='a.a'
export NEW_USER_EMAIL=''
export NEW_USER_EMAIL_INDEX="${TEST_USER_EMAIL_INDEX}"
export LOGIN_USER_EMAIL=''
COMMAND_OPTION_DEBUG=""
export GHOST_IN_A_SHELL_DEBUG='0'
export GHOST_IN_A_SHELL_BROWSER_KEEP_WINDOW_OPEN="0"
export GHOST_IN_A_SHELL_BROWSER_HEADLESS="1"
export USE_X11VNC="0"
export XDEBUG_CONFIG=
export PHP_IDE_CONFIG=

# Query the terminal to establish a default number of columns to use for
# displaying messages to the user.  This is used only as a fallback in the event
# the COLUMNS variable is not set.  ($COLUMNS can react to SIGWINCH while the
# script is running, and this cannot, only being calculated once.)
DEFCOLUMNS=$(stty size 2>/dev/null | awk '{print $2}') || true
if ! expr "$DEFCOLUMNS" : "[[:digit:]]\+$" >/dev/null 2>&1; then
    DEFCOLUMNS=80
fi

PROGNAME='se.sh'

# Display a message, wrapping lines at the terminal width.
message () {
    echo "$PROGNAME: $*" | fmt -t -w ${COLUMNS:-$DEFCOLUMNS}
}

# Display an error message.
error () {
    message "error: $*" >&2
}

# Display a usage message.
usage () {
    if [ -n "$*" ]; then
        message "usage error: $*"
    fi
    cat <<EOF
Usage: $PROGNAME [OPTIONS ...] NODE_JS_SCRIPT
Run NODE_JS_SCRIPT and optionally process the output or run additional tests
Options:
-b        --debug-output            echo debug information while running this
                                    script
-c        --debug-node              breakpoint on the first line of the Node.js
                                    script and start listening for incoming
                                    debugger connections on port 9230 (22230)
-d        --debug-php               start the PHP scripts with Xdebug enabled
                                    on port 9900
-g        --graphical               disable headless browser mode
-h        --help                    display this usage message and exit
-i NUM    --test-email-index=NUM    test{NUM}@a.a to use for email accounts
-l EMAIL  --login-user-email=EMAIL  email to use for logging in
-n EMAIL  --new-user-email=EMAIL    email to use for signing up
-v NUM    --verbosity=NUM           level of verbosity from 0 (default) to 7
-w        --do-not-close-browser    do not close the browser window when
                                    the Node.js process is about to exit
                                    and show a "press any key to continue"
                                    message instead
-x        --use-x11vnc              start x11vnc, Xvfb and fluxbox allowing
                                    the browser to be visualized and monitored
                                    listening on port 5900 (22590) for
                                    incoming VNC connections
EOF
}

# Parse the command line.
ARGS=$(getopt --options +bcdghi:l:n:v:wx \
       --long debug-output,debug-node,debug-php,graphical,help,test-user-email-index:,login-user-email:,new-user-email:,verbosity:,do-not-close-browser,use-x11vnc \
       --name "$PROGNAME" -- "$@")
GETOPT_STATUS=$?

if [ $GETOPT_STATUS -ne 0 ]; then
    error "internal error; getopt exited with status $GETOPT_STATUS"
    exit 6
fi

eval set -- "${ARGS}"

while :; do
    case "$1" in
        -c|--debug-node)
            export GHOST_IN_A_SHELL_DEBUG='1'
            COMMAND_OPTION_DEBUG='--inspect-brk=0.0.0.0:9230'
            ;;
        -d|--debug-php)
            export XDEBUG_CONFIG="remote_enable=1 remote_mode=req remote_port=9900 remote_host=127.0.0.1 remote_connect_back=0"
            export PHP_IDE_CONFIG="serverName=test"
            ;;
        -g|--graphical)
            export GHOST_IN_A_SHELL_BROWSER_HEADLESS="0"
            ;;
        -h|--help)
            SHOWHELP="yes"
            ;;
        -i|--test-user-email-index)
            export TEST_USER_EMAIL_INDEX="$2"
            shift
            ;;
        -l|--login-user-email)
            export LOGIN_USER_EMAIL="$2"
            shift
            ;;
        -n|--new-user-email)
            export NEW_USER_EMAIL="$2"
            shift
            ;;
        -v|--verbosity)
            re='^[0-7]$'
            if ! [[ $2 =~ $re ]] ; then
               error "argument error: verbosity must be and integer between 0 and 7"
               exit 6
            fi
            __VERBOSE="$2"
            shift;
            ;;
        -w|--do-not-close-browser)
            export GHOST_IN_A_SHELL_BROWSER_KEEP_WINDOW_OPEN='1'
            export GHOST_IN_A_SHELL_BROWSER_HEADLESS='0'
            ;;
        -x|--use-x11vnc)
            export USE_X11VNC="1"
            ;;
        --) shift; break ;;
        *) error "internal error; getopt encountered \"$1\" unexpectedly"
           exit 6
           ;;
    esac
    shift
done

logPlain 6 "${splash}"

if [ "$SHOWHELP" ]; then
    usage
    exit 0
fi

if [ -z "$*" ]; then
    usage "need a Node.js script to run" >&2
    exit 2
fi

while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapi 443; do
    log 5 'Waiting for the WebAPI to start...'
    sleep 1
done
while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapp 4200; do
    log 5 'Waiting for the WebApp to start...'
    sleep 1
done
while ! nc -z ${COMPOSE_PROJECT_NAME:-helden}-webapp 49152; do
    log 5 'Waiting for the WebApp to start...'
    sleep 1
done

# Script vars
scriptsDir="/home/node/app/user_scripts/selenium"
webappHostPort="16200"
webappContainerPort="4200"
webapiHostPort="13443"
webapiContainerPort="443"

currentDir="$(pwd)"
cd /home/node

logPlain 6 ""
log 6 'Chromium version: '$(chromium --version)
log 6 'ChromeDriver version: '$(chromedriver --version)
log 6 'selenium-webdriver version: '$(npm view selenium-webdriver version)
logPlain 6 ""

manageStaleBrowserProcesses 'Warning: There are stale browser processes (possibly from a previous test run).'
if [[ -e "/root/.ssh/webapp.ctl" ||  -e "/root/.ssh/webapi.ctl"  ]]; then
    if [[ -e "/root/.ssh/webapp.ctl" ]]; then
        log 4 'Warning: A webapp SSH control socket socket/file exists. Attempting closure/removal...'
        closeSshTunnel 'webapp'
    fi
    if [[ -e "/root/.ssh/webapi.ctl" ]]; then
        log 4 'Warning: A webapi SSH control socket socket/file exists. Attempting closure/removal...'
        closeSshTunnel 'webapi'
    fi
    logPlain 4 ""
fi

if [ ${__VERBOSE} -ge 6 ]; then
    manageStaleX11Processes
else
    manageStaleX11Processes >/dev/null 2>&1
fi

if [[ "${USE_X11VNC}" = "1" ]]; then
    if [ ${__VERBOSE} -ge 6 ]; then
        start-x11vnc.sh &
    else
        start-x11vnc.sh >/dev/null 2>&1 &
    fi
fi

if [[ "${NEW_USER_EMAIL}" = "" ]]; then
    log 6 'Generating a unique email for use with the signup page functionality...'
    declare -A whereClausesCoreapi;
    whereClausesCoreapi["0:column"]="email";
    whereClausesCoreapi["0:value"]="${TEST_USER_EMAIL}${TEST_USER_EMAIL_INDEX}@${TEST_USER_EMAIL_DOMAIN}";
    queryWhereCoreapi=""
    for ((j=0;j<$((${#whereClausesCoreapi[@]} / 2));j++)); do
        if ((${j}>0)); then
            queryWhereCoreapi=${queryWhereCoreapi}', '${whereClausesCoreapi["${j}:column"]}'="'${whereClausesCoreapi["${j}:value"]}'"';
        else
            queryWhereCoreapi=${whereClausesCoreapi["${j}:column"]}'="'${whereClausesCoreapi["${j}:value"]}'"';
        fi
    done
    rowCountCoreapi=$(databaseSelectCount "coreapi" "users" "${queryWhereCoreapi}")
    if [[ "${rowCountCoreapi}" = "1" ]]; then
        export LOGIN_USER_EMAIL="${TEST_USER_EMAIL}${TEST_USER_EMAIL_INDEX}@${TEST_USER_EMAIL_DOMAIN}"
    fi
    while [[ "${rowCountCoreapi}" != "0" ]]; do
        log 6 "Email ${TEST_USER_EMAIL}${TEST_USER_EMAIL_INDEX}@${TEST_USER_EMAIL_DOMAIN} is already registered. Incrementing the test accounts index."
        ((NEW_USER_EMAIL_INDEX++))
        export NEW_USER_EMAIL_INDEX
        whereClausesCoreapi["0:column"]="email";
        whereClausesCoreapi["0:value"]="${TEST_USER_EMAIL}${NEW_USER_EMAIL_INDEX}@${TEST_USER_EMAIL_DOMAIN}";
        queryWhereCoreapi=""
        for ((j=0;j<$((${#whereClausesCoreapi[@]} / 2));j++)); do
            if ((${j}>0)); then
                queryWhereCoreapi=${queryWhereCoreapi}', '${whereClausesCoreapi["${j}:column"]}'="'${whereClausesCoreapi["${j}:value"]}'"';
            else
                queryWhereCoreapi=${whereClausesCoreapi["${j}:column"]}'="'${whereClausesCoreapi["${j}:value"]}'"';
            fi
        done
        rowCountCoreapi=$(databaseSelectCount "coreapi" "users" "${queryWhereCoreapi}")
    done
fi
export NEW_USER_EMAIL="${TEST_USER_EMAIL}${NEW_USER_EMAIL_INDEX}@${TEST_USER_EMAIL_DOMAIN}"
if [[ "${LOGIN_USER_EMAIL}" = "" ]]; then
    log 6 "No registered test users were found (tried with ${TEST_USER_EMAIL}${TEST_USER_EMAIL_INDEX}@${TEST_USER_EMAIL_DOMAIN}). Tests requiring authentication will have to run the registration test first."
else
    log 6 'Using '${LOGIN_USER_EMAIL}' for login.'
fi
log 6 'Using '${NEW_USER_EMAIL}' for signup.'

declare -A whereClausesDovecot;
whereClausesDovecot["0:column"]="userid";
whereClausesDovecot["0:value"]="${TEST_USER_EMAIL}${TEST_USER_EMAIL_INDEX}";
whereClausesDovecot["1:column"]="domain";
whereClausesDovecot["1:value"]="${TEST_USER_EMAIL_DOMAIN}";
queryWhereDovecot=""
for ((j=0;j<$((${#whereClausesDovecot[@]} / 2));j++)); do
    if ((${j}>0)); then
        queryWhereDovecot=${queryWhereDovecot}' AND '${whereClausesDovecot["${j}:column"]}'="'${whereClausesDovecot["${j}:value"]}'"';
    else
        queryWhereDovecot=${whereClausesDovecot["${j}:column"]}'="'${whereClausesDovecot["${j}:value"]}'"';
    fi
done
rowCountDovecot=$(databaseSelectCount "${MAILSERVER_DOVECOT_MYSQL_DATABASE}" "users" "${queryWhereDovecot}")
if [[ "${rowCountDovecot}" = "0" ]]; then
    query='INSERT INTO users (userid, domain, password, home, uid, gid) VALUES("'${TEST_USER_EMAIL}${TEST_USER_EMAIL_INDEX}'", "'${TEST_USER_EMAIL_DOMAIN}'", ENCRYPT("'${EMAILACCOUNTS_PASSWORD}'", CONCAT("\$6\$", SUBSTRING(SHA(RAND()), -16))), "/home/vmail/'${TEST_USER_EMAIL_DOMAIN}'/'${TEST_USER_EMAIL}${TEST_USER_EMAIL_INDEX}'", 103, 104)'
    # Don't really have the time for this now: https://dev.mysql.com/doc/refman/5.7/en/mysql-config-editor.html
    commandMysql="/usr/local/mysql/bin/mysql -u root -p${MYSQL_ROOT_PASSWORD} -N ${MAILSERVER_DOVECOT_MYSQL_DATABASE} -e '"${query}"'"
    commandSsh="ssh mysql ${commandMysql}"
    log 6 ${commandSsh}
    ${commandSsh}
fi

declare -A whereClausesPostfix;
whereClausesPostfix["0:column"]="email";
whereClausesPostfix["0:value"]="${NEW_USER_EMAIL}";
queryWherePostfix=""
for ((j=0;j<$((${#whereClausesPostfix[@]} / 2));j++)); do
    if ((${j}>0)); then
        queryWherePostfix=${queryWherePostfix}', '${whereClausesPostfix["${j}:column"]}'="'${whereClausesPostfix["${j}:value"]}'"';
    else
        queryWherePostfix=${whereClausesPostfix["${j}:column"]}'="'${whereClausesPostfix["${j}:value"]}'"';
    fi
done
rowCountPostfix=$(databaseSelectCount "${MAILSERVER_POSTFIX_MYSQL_DATABASE}" "users" "${queryWherePostfix}")
if [[ "${rowCountPostfix}" = "0" ]]; then
    query='INSERT INTO users (domain_id, password, email) VALUES(5, ENCRYPT('${EMAILACCOUNTS_PASSWORD}', CONCAT("\$6\$", SUBSTRING(SHA(RAND()), -16))), "'${NEW_USER_EMAIL}'")'
    # Don't really have the time for this now: https://dev.mysql.com/doc/refman/5.7/en/mysql-config-editor.html
    commandMysql="/usr/local/mysql/bin/mysql -u root -p${MYSQL_ROOT_PASSWORD} -N ${MAILSERVER_POSTFIX_MYSQL_DATABASE} -e '"${query}"'"
    commandSsh="ssh mysql ${commandMysql}"
    log 6 ${commandSsh}
    ${commandSsh}
fi


openSshTunnel 'WebApp' ${webappHostPort} ${webappContainerPort} 'webapp'

if false; then
    log 6 'Checking to see if the WebApp is up and running...'
    command="node ${scriptsDir}/server-is-responsive.js"
    log 6 'Running `'${command}'`...'
    ${command}
    lastCode=$?
    log 6 'Exit code: '${lastCode}
    if [[ ${lastCode} = 1 ]]; then
        log 3 'Error: The WebApp failed (some of) the basic tests.'
        ssh -T -O "exit" webapp 2>/dev/null >/dev/null
        manageStaleBrowserProcesses 'Warning: The test engine did not make a clean shutdown.'
        exit 1
    fi
    if [[ ${lastCode} = 2 ]]; then
        log 3 'Error: The test engine failed.'
        ssh -T -O "exit" webapp 2>/dev/null >/dev/null
        manageStaleBrowserProcesses 'Warning: The test engine did not make a clean shutdown.'
        exit 2
    fi
    if [ ${lastCode} -ne 0 ]; then
        while [ ${lastCode} -ne 0 ]; do
            log 4 'The WebApp does not appear to be running. Starting 2sec interval polling...'
            sleep 2
            ${command}
            lastCode=$?
        done
    else
        log 6 '...seems like...'
        logPlain 6 ""
    fi
fi

openSshTunnel 'WebApi' ${webapiHostPort} ${webapiContainerPort} 'webapi'

# Add `--inspect-brk=0.0.0.0:9230` to debug inside this container
# Example: `se.sh --inspect-brk=0.0.0.0:9230 app/user_scripts/selenium/suite-onboarding.js`
command="node ${COMMAND_OPTION_DEBUG} $@"
logPlain 6 ""
log 6 'Running `'${command}'`...'
if [ ${__VERBOSE} -ge 6 ]; then
    export outputNode=$(${command})
else
    export outputNode=$(${command} 2>/dev/null)
fi
#${command}
#exit 0
#echo ${outputNode}
lastCode=$?
log 6 'Exit code: '${lastCode}
if [[ ${lastCode} = 1 ]]; then
    log 3 'Error: The WebApp tests failed.'
    ssh -T -O "exit" webapp 2>/dev/null >/dev/null
    manageStaleBrowserProcesses 'Warning: The test engine did not make a clean shutdown.'
    exit 1
fi
if [[ ${lastCode} = 2 ]]; then
    log 3 'Error: The test engine failed.'
    ssh -T -O "exit" webapp 2>/dev/null >/dev/null
    manageStaleBrowserProcesses 'Warning: The test engine did not make a clean shutdown.'
    exit 2
fi

# These next two lines can be used to enable Xdebug
#export XDEBUG_CONFIG="remote_enable=1 remote_mode=req remote_port=9900 remote_host=127.0.0.1 remote_connect_back=0"
#export PHP_IDE_CONFIG="serverName=test"
log 6 'Running the Node.js output against `php app/procxml.php`'
export outputPhp=$(php app/procxml.php)
log 6 'done'

#if [[ "${XSERVER}" = "1" ]]; then
#    read -n 1 -s -r -p "Press any key to exit..."
#    echo
#fi
log 6 'Running the PHP output against xmllint'
export outputXmllint=$(echo ${outputPhp} | xmllint --noblanks --schema app/schemas/junit-10.xsd -)

echo ${outputPhp}

closeSshTunnel 'webapp'
closeSshTunnel 'webapi'
logPlain 6 ""

#if false; then
manageStaleBrowserProcesses 'Warning: The test engine did not make a clean shutdown.'
#fi

cd "${currentDir}"
log 6 'All done. Bye!'
