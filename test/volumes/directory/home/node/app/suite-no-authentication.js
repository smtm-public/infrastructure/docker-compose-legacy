'use strict';

const GhostInAShell = require('./lib/GhostInAShell');
const Test = require('./lib/GhostInAShell/Test');
const config = require('./config/config');
const pages = require('./config/pages');

let gias;

try {
    (async() => {
        gias = await new GhostInAShell(config, pages);

        gias

            .addTestAssembly('Login Page')
                .addTestCollectionLoadPage('Login')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Login')
                .addTestCollection('Validation')
                    .addTestCaseBrowser('Empty values validation')
                        .onPage('Login')
                        .element('Email Input Field')
                        .clear()
                        .element('Password Input Field')
                        .clear()
                        .element('Submit Button')
                        .click()
                        .runAsync(
                            'empty form validation messages to appear',
                            async function () {
                                return await Promise.all(
                                    [
                                        this.waitUntilElementInDomSync('Empty Email Message', 1000),
                                        this.waitUntilElementInDomSync('Empty Password Message', 1000),
                                    ]
                                );
                            }
                        )
                    .addTestCaseBrowser('Invalid email value validation')
                        .onPage('Login')
                        .mapCallable(
                            [
                                'test',
                                'test@asas',
                                'tesqw    et@abv.БГ',
                            ],
                            function (emailInputFieldValue) {
                                this
                                    .element('Email Input Field')
                                    .clear()
                                    .sendKeys(emailInputFieldValue)
                                    .element('Submit Button')
                                    .click()
                                    .waitUntilElementInDom('Invalid Email Message', 1000)
                                ;
                            }
                        )

            .addTestAssembly('Signup Page')
                .addTestCollection('Click on the Register Now Link')
                    .addTestCaseBrowser('Click on the Register Now Link')
                        .onPage('Login')
                        .waitUntilElementInDom('Register Now Link', 2000)
                        .click()
                        .waitUntilPageLoaded('Signup', 2000)
                .addTestCollectionLoadPage('Signup')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Signup')
                .addTestCollection('Validation')
                    .addTestCaseBrowser('Empty values validation')
                        .onPage('Signup')
                        .element('Email Input Field')
                        .clear()
                        .element('Submit Button')
                        .click()
                        .runAsync(
                            'empty form validation messages to appear',
                            async function () {
                                return await Promise.all(
                                    [
                                        this.waitUntilElementInDomSync('Empty Email Message', 1000),
                                        this.waitUntilElementInDomSync('Empty Password Message', 1000),
                                    ]
                                );
                            }
                        )
                    .addTestCaseBrowser('Invalid email value validation')
                        .onPage('Signup')
                        .mapCallable(
                            [
                                'test',
                                'test@asas',
                                'tesqw    et@abv.БГ',
                            ],
                            function (emailInputFieldValue) {
                                this
                                    .element('Email Input Field')
                                    .clear()
                                    .sendKeys(emailInputFieldValue)
                                    .element('Submit Button')
                                    .click()
                                    .waitUntilElementInDom('Invalid Email Message', 1000)
                                ;
                            }
                        )
                    .addTestCaseBrowser('Invalid password value validation')
                        .onPage('Signup')
                        .mapCallable(
                            [
                                '123',
                                'тест',
                            ],
                            function (passwordInputFieldValue) {
                                this
                                    .element('Password Input Field')
                                    .clear()
                                    .sendKeys(passwordInputFieldValue)
                                    .element('Submit Button')
                                    .click()
                                    .waitUntilElementInDom('Invalid Password Message', 1000)
                                ;
                            }
                        )
                    .addTestCaseBrowser('Click on the Go Back To The Previous Page Link')
                        .onPage('Signup')
                        .element('Go Back To The Previous Page Link')
                        .click()
                        .waitUntilPageLoaded('Login', 2000)

            .addTestAssembly('Forgot Password Page')
                .addTestCollection('Click on the Forgot Password Link')
                    .addTestCaseBrowser('Click on the Forgot Password Link')
                        .onPage('Login')
                        .waitUntilElementInDom('Forgot Password Link', 2000)
                        .click()
                        .waitUntilPageLoaded('Forgot Password', 2000)
                .addTestCollectionLoadPage('Forgot Password')
                .addTestCollectionBasic()
                .addTestCollectionEnumeratePageElements('Forgot Password')
        ;

        let testAssemblyResults = await gias.runTests(); // TODO: MK - See if passing the tasks as argument to gias.runTests() would be better

        console.log('asdasdasdsadsadsadsad');
    })().catch(async(error) => !gias || await gias.shutdownWithError(error));
} catch(e) {
    !gias || (async() => {return await gias.shutdownWithError(e);})();
}

!gias || (async() => {return await gias.shutdownWithSuccess();})();