'use strict';

const Keyboard = require('./Keyboard');
const Mouse = require('./Mouse');

class Input
{
    static get Keyboard() {
        return Keyboard;
    }

    static get Mouse() {
        return Mouse;
    }
}

module.exports = Input;