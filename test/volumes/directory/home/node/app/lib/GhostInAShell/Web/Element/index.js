'use strict';

//const State = require('./State');
const Selector = require('./Selector');

class Element
{
    static get State() {
        return State;
    }

    static get Selector() {
        return Selector;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get selectors() {
        return this._selectors;
    }
    set selectors(value) {
        this._selectors = value;
    }

    get behavior() {
        return this._behavior;
    }
    set behavior(value) {
        this._behavior = value;
    }

    get webElement() {
        return this._webElement;
    }
    set webElement(value) {
        this._webElement = value;
    }

    constructor(name, selectors, behavior, webElement) {
        this._name = name;
        this._selectors = selectors || [];
        this._behavior = behavior;
        this._webElement = webElement;
    }

    async clear(browser) {
        return await browser.clear(this);
    }

    async sendKeys(browser, ...keys) {
        return await browser.sendKeys(this, ...keys);
    }

    async click(browser) {
        return await browser.click(this);
    }

    async check(browser) {
        return await browser.check(this);
    }

    async uncheck(browser) {
        return await browser.uncheck(this);
    }

    async mouseMoveToCenter(browser) {
        return await browser.mouseMove(
            [
                {x: 0, y: 0, duration: 0, origin: this},
            ]
        );
    }
}

module.exports = Element;