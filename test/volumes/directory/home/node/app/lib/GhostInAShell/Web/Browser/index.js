'use strict';

const ExceptionHandler = require('../../System/ExceptionHandler');
const Exception = require('../../System/Exception');

const AdapterSelenium = require('./AdapterSelenium');
const BrowserState = require('./BrowserState');
const Page = require('../Page');
const Element = require('../Element');
const Input = require('./Input');

class Browser {
    static get AdapterSelenium() {
        return AdapterSelenium;
    }

    static get BrowserState() {
        return BrowserState;
    }

    static get Input() {
        return Input;
    }

    get gias() {
        return this._gias;
    }
    set gias(value) {
        this._gias = value;
    }

    get adapter() {
        return this._adapter;
    }
    set adapter(value) {
        this._adapter = value;
    }

    get state() {
        return this._state;
    }
    set state(value) {
        this._state = value;
    }

    get pages() {
        return this._pages;
    }
    set pages(value) {
        this._pages = value;
    }

    constructor(gias, pages) {
        this._gias = gias;
        this._state = new BrowserState();
        this._pages = [];
        pages.forEach(
            (page) => {
                this.pages[page.name] = page;
            }
        );

        return (async() => {
            this._adapter = await new Browser[gias.config.browser.adapter](gias.config.browser);

            return this;
        })();
    }

    getPageElement(pageName, elementName) {
        return this.pages[pageName].elements.find(
            (element) => {
                return element.name === elementName;
            }
        );
    }

    async loadPage(name) {
        await this.adapter.loadPage(this.pages[name]);

        this.state.currentPage = this.pages[name];

        return this;
    }

    async getPageSource() {
        return await this.adapter.getPageSource();
    }

    async getHtmlElementsCount() {
        return await this.adapter.executeScript('return document.getElementsByTagName("*").length;');
    }

    /*
    async existsElement(element) {
        return await this.adapter.existsElement(element);
    }
    */

    async findPageElement(pageName, elementName) {
        let element;

        try {
            element =
                this.pages[pageName].elements.find(
                    (element) => {
                        return element.name === elementName;
                    }
                );
        } catch(e) {
            element = ExceptionHandler.createException(e, Exception.Severity.FATAL, Exception.Type.RUNTIME, 'There was an error while trying to find an Element with the given name.');
        }

        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.findElement(element);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async findElement(name) {
        let element;

        try {
            element =
                this.state.currentPage.elements.find(
                    (element) => {
                        return element.name === name;
                    }
                );
        } catch(e) {
            element = ExceptionHandler.createException(e, Exception.Severity.FATAL, Exception.Type.RUNTIME, 'There was an error while trying to find an Element with the given name.');
        }

        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.findElement(element);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async findElementInstance(element) {
        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.findElement(element);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async clear(element) {
        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.clear(element);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async check(element) {
        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.check(element);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async uncheck(element) {
        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.uncheck(element);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async sendKeys(element, ...keys) {
        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.sendKeys(element, ...keys);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async click(element) {
        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.click(element);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async mouseMove(coordinates) {
        const result = await this.adapter.mouseMove(coordinates);

        return result;
    }

    async dragAndDrop(coordinates) {
        const result = await this.adapter.dragAndDrop(coordinates);

        return result;
    }

    async addEventListenerMousePosition() {
        await this.adapter.addEventListenerMousePosition();
    }

    async getCurrentMousePosition() {
        return await this.adapter.getCurrentMousePosition();
    }

    async executeScript(code, ...args) {
        return await this.adapter.executeScript(code, ...args);
    }

    async waitUntilElementInDom(name, milliseconds) {
        let element =
            this.state.currentPage.elements.find(
                (element) => {
                    return element.name === name;
                }
            );

        if(!element instanceof Element) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
        }

        const result = await this.adapter.waitUntilElementInDom(element, milliseconds);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async waitUntilElementInDomInstance(element, milliseconds) {
        if(element instanceof Exception) {
            return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is an exception: ' + element.message);
        } else {
            if(!element instanceof Element) {
                return ExceptionHandler.createException(element, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Element is not defined.');
            }
        }

        const result = await this.adapter.waitUntilElementInDom(element, milliseconds);

        if(!result instanceof Element) {
            return ExceptionHandler.createException(result, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Could not find element ' + element.name + '.');
        }

        return result;
    }

    async waitUntilPageLoaded(name, milliseconds) {
        const page = this.pages[name];

        if(!page instanceof Page) {
            return ExceptionHandler.createException(page, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Page is not defined.');
        }

        const result = await this.adapter.waitUntilUrlIs(page, milliseconds);
        if(result instanceof Page) {
            this.state.currentPage = result; // TODO: MK - Take care of this stateful prop
        }
        return result;
    }

    async waitUntilPageLoadedInstance(page, milliseconds) {
        if(!page instanceof Page) {
            return ExceptionHandler.createException(page, Exception.Severity.FATAL, Exception.Type.BROWSER, 'Page is not defined.');
        }

        const result = await this.adapter.waitUntilUrlIs(page, milliseconds);
        if(result instanceof Page) {
            this.state.currentPage = result; // TODO: MK - Take care of this stateful prop
        }
        return result;
    }

    async quit() {
        return await this.adapter.quit();
    }
}

module.exports = Browser;