'use strict';

const Case = require('./Case');

class Collection
{
    get gias() {
        return this._gias;
    }
    set gias(value) {
        this._gias = value;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get validationCallable() {
        return this._validationCallable;
    }
    set validationCallable(value) {
        this._validationCallable = value;
    }

    get cases() {
        return this._cases;
    }
    set cases(cases) {
        this._cases = cases;
    }

    constructor(gias, name, validationCallable, cases) {
        this._gias = gias;
        this._name = name;
        this._validationCallable = validationCallable || Collection.validateDefault;
        this._cases = cases || [];
    }

    addCase(caseObject) {
        this.cases.push(caseObject);

        return this;
    }

    addCaseValidateHtmlSourceBasic() {
        this.addCase(
            new Case(this.gias, 'Validate Html Source Basic', )
        );
        return /<html[\s\S]+<\/html>/.test(source);
    }

    async run() {
        const now = new Date();
        let results = {
            name: this.name,
            items: [],
        };
        for(let caseIndex in this.cases) {
            if(this.cases.hasOwnProperty(caseIndex)) {
                let result = await this.cases[caseIndex].run();
                results.items[caseIndex] = {
                    case: this.cases[caseIndex],
                    result: result,
                };
            }
        }
        results.time = ((new Date()).getTime() - now.getTime()) / 1000;
        results.validation = this.validationCallable(results);
        results.stats = Collection.produceTestCasesStatistics(results);

        return results;
    }

    static validateDefault(results) {
        return true;
    }

    static produceTestCasesStatistics(collectionResult) {
        return {
            total: collectionResult.items.length,
            passed: collectionResult.items.filter(
                (caseItem) => {
                    return caseItem.result.validation;
                }
             ).length,
            failed: collectionResult.items.filter(
                (caseItem) => {
                    return !caseItem.result.validation;
                }
             ).length,
            skipped: 0,
        };
    }
}

module.exports = Collection;