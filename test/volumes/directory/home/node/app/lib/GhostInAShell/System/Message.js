'use strict';

class Type {
    static get PLAIN() {
        return 1;
    }

    static get INFO() {
        return 2;
    }

    static get ACTION() {
        return 4;
    }

    static get TEST() {
        return 8;
    }

    static get CHECK() {
        return 16;
    }

    static get PASSED() {
        return 32;
    }

    static get SUCCESS() {
        return 64;
    }

    static get INIT() {
        return 128;
    }

    static get ENVIRONMENT() {
        return 256;
    }

    static get INPUT() {
        return 512;
    }

    static get STATE() {
        return 1024;
    }

    static get EXCEPTION() {
        return 2048;
    }
}

class Message
{
    static get Type() {
        return Type;
    }

    get source() {
        return this._source;
    }
    set source(value) {
        this._source = value;
    }

    get type() {
        return this._type;
    }
    set type(value) {
        this._type = value;
    }

    get text() {
        return this._text;
    }
    set text(value) {
        this._text = value;
    }

    constructor(source, type, text) {
        this._source = source;
        this._type = type;
        this._text = text;
    }
}

module.exports = Message;