'use strict';

class Severity {
    static get NOTICE() {
        return 1;
    }

    static get WARNING() {
        return 2;
    }

    static get RECOVERABLE() {
        return 4;
    }

    static get FATAL() {
        return 8;
    }
}

class Type {
    static get ENGINE() {
        return 1;
    }

    static get RUNTIME() {
        return 2;
    }

    static get DRIVER() {
        return 4;
    }

    static get BROWSER() {
        return 8;
    }

    static get BROWSER_ADAPTER() {
        return 16;
    }

    static get TEST() {
        return 32;
    }
}

class Exception extends Error
{
    static get Severity() {
        return Severity;
    }

    static get Type() {
        return Type;
    }

    get previous() {
        return this._previous;
    }
    set previous(value) {
        this._previous = value;
    }

    get severity() {
        return this._severity;
    }

    get type() {
        return this._type;
    }

    get message() {
        return this._message;
    }

    get code() {
        return this._code;
    }

    get callback() {
        return this._callback;
    }

    get stack() {
        return this._stack;
    }

    constructor(previous, severity, type, message, code, callback, stack) {
        super();

        this._previous = previous;
        this._severity = severity;
        this._type = type;
        this._message = message;
        this._code = code;
        this._callback = callback;
        this._stack = stack;
    }
}

module.exports = Exception;