'use strict';

const Selenium = require('selenium-webdriver');
const Capabilities = require('selenium-webdriver/lib/capabilities');
const Chrome = require('selenium-webdriver/chrome');
const Logging = require('selenium-webdriver/lib/logging');
const Input = require('selenium-webdriver/lib/input');
const Until = require('selenium-webdriver/lib/until');
const WebDriver = require('selenium-webdriver/lib/webdriver');

const ExceptionHandler = require('../../System/ExceptionHandler');
const Exception = require('../../System/Exception');

const Element = require('../../Web/Element');
const Mouse = require('../../Web/Browser/Input/Mouse');

class AdapterSelenium {
    static get Input() {
        return Input;
    }

    constructor(configBrowser) {
        this.environment = {
            isWindows: process.platform === 'win32',
        };

        this.builder = new Selenium.Builder();
        this.chromeOptions = new Chrome.Options()
            .addArguments('no-sandbox', 'disable-plugins')
            .windowSize({width: configBrowser.outerWidth, height: configBrowser.outerHeight})
        ;
        if(process.env.GHOST_IN_A_SHELL_BROWSER_HEADLESS && process.env.GHOST_IN_A_SHELL_BROWSER_HEADLESS === '1') {
            this.chromeOptions.headless();
        }
        this.environment.isWindows || this.chromeOptions.setChromeBinaryPath('/usr/bin/chromium');
        this.loggingPreferences = new Logging.Preferences();
        this.loggingPreferences.setLevel(Logging.Type.DRIVER, Logging.Level.DEBUG);
        this.loggingPreferences.setLevel(Logging.Type.BROWSER, Logging.Level.DEBUG);
        this.chromeCapabilities = Capabilities.Capabilities.chrome();
        //this.chromeCapabilities.setLoggingPrefs(this.loggingPreferences);
        this.builder.forBrowser(Capabilities.Browser.CHROME)
            .setChromeOptions(
                this.chromeOptions,
            )
            .withCapabilities(this.chromeCapabilities)
        ;

        return (async () => {
            this.driver = await this.builder.build();
            //Logging.installConsoleHandler();
            //this.logs = this.driver.manage().logs();
            //this.availableLogTypes = await this.logs.getAvailableLogTypes();
            //this.httpAgent = this.builder.getHttpAgent();
            //this.navigation = this.driver.navigate();
            //this.executor = this.driver.getExecutor();
            //this.actions = new StaticClassSeleniumX.Input.Actions(this.executor, {async: true});
            //this.actions = this.driver.actions();

            return this;
        })();
    }

    async loadPage(page) {
        await this.driver.get(page.route.url);

        return this;
    }

    async getPageSource() {
        return await this.driver.getPageSource();
    }

    /*
    async existsElement(element) {
        return this.findElement(element);
    }
    */

    async findElement(element) {
        try {
            /*
            switch(element.selector.type) {
                case Element.Selector.Type.CSS:
                    await this.driver.findElement(Selenium.By.css(element.selector.value));
                    break;
                case Element.Selector.Type.XPATH:
                    await this.driver.findElement(Selenium.By.xpath(element.selector.value));
                    break;
                case Element.Selector.Type.LINK_TEXT:
                    await this.driver.findElement(Selenium.By.linkText(element.selector.value));
                    break;
            }
            */
            element.webElement = await this.driver.findElement(Selenium.By[element.selectors[0].type](element.selectors[0].value));
            //console.log(await this.driver.executeScript('return arguments[0].outerHtml', element.webElement));
            return element;
        } catch(e) {
            return ExceptionHandler.createException(e, Exception.Severity.RECOVERABLE, Exception.Type.BROWSER_ADAPTER, 'Cannot find element ' + element.name + '.');
        }
    }

    async clear(element) {
        element.webElement = await this.driver.findElement(Selenium.By[element.selectors[0].type](element.selectors[0].value));
        await element.webElement.sendKeys(Input.Key.CONTROL, 'a', Input.Key.NULL, Input.Key.DELETE);
        return element;
    }

    async sendKeys(element, ...keys) {
        element.webElement = await this.driver.findElement(Selenium.By[element.selectors[0].type](element.selectors[0].value));
        await element.webElement.sendKeys(...keys);
        return element;
    }

    async click(element) {
        element.webElement = await this.driver.findElement(Selenium.By[element.selectors[0].type](element.selectors[0].value));
        await element.webElement.click();
        return element;
    }

    async mouseMove(coordinates) {
        // more on bridge mode here: https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/input_exports_Actions.html
        let actions = this.driver.actions({bridge: true,});

        for(let i in coordinates) {
            if (coordinates.hasOwnProperty(i)) {
                if(!coordinates[i].origin || coordinates[i].origin === Mouse.Origin.VIEWPORT) {
                    actions
                        .move({
                            x: coordinates[i].x || 0,
                            y: coordinates[i].y || 0,
                            duration: coordinates[i].duration || 0,
                            origin: AdapterSelenium.Input.Origin.VIEWPORT,
                        });
                } else {
                    if (coordinates[i].origin instanceof Element) {
                        let webElement = await this.driver.findElement(Selenium.By[coordinates[i].origin.selectors[0].type](coordinates[i].origin.selectors[0].value));
                        actions
                            .move({
                                x: coordinates[i].x || 0,
                                y: coordinates[i].y || 0,
                                duration: coordinates[i].duration || 0,
                                origin: webElement,
                            });
                    } else {
                        if (coordinates[i].origin === Mouse.Origin.POINTER) {
                            actions
                                .move({
                                    x: coordinates[i].x || 0,
                                    y: coordinates[i].y || 0,
                                    duration: coordinates[i].duration || 0,
                                    origin: AdapterSelenium.Input.Origin.POINTER,
                                });
                        }
                    }
                }
            }
        }
        /*
        targetCoordinates.forEach(
            async(target) => {
                if(target.origin instanceof Element) {
                    let webElement = await this.driver.findElement(Selenium.By[target.origin.selectors[0].type](target.origin.selectors[0].value));
                    return await actions
                        .move({
                            x: 0,
                            y: 0,
                            duration: 0,
                            origin: webElement,
                        })
                        .perform();
                } else {
                    if(target.origin === ) {

                    }
                }
            }
        );
        */
        await actions
            .perform();

        return coordinates;
    }

    async dragAndDrop(coordinates) {
        // more on bridge mode here: https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/input_exports_Actions.html
        let actions = this.driver.actions({bridge: true,});

        actions
            .press();
        for(let i in coordinates) {
            if (coordinates.hasOwnProperty(i)) {
                if(!coordinates[i].origin || coordinates[i].origin === Mouse.Origin.VIEWPORT) {
                    await actions
                        .move({
                            x: coordinates[i].x || 0,
                            y: coordinates[i].y || 0,
                            duration: coordinates[i].duration || 0,
                            origin: AdapterSelenium.Input.Origin.VIEWPORT,
                        });
                } else {
                    if (coordinates[i].origin instanceof Element) {
                        let webElement = await this.driver.findElement(Selenium.By[coordinates[i].origin.selectors[0].type](coordinates[i].origin.selectors[0].value));
                        actions
                            .move({
                                x: coordinates[i].x || 0,
                                y: coordinates[i].y || 0,
                                duration: coordinates[i].duration || 0,
                                origin: webElement,
                            });
                    } else {
                        if (coordinates[i].origin === Mouse.Origin.POINTER) {
                            actions
                                .move({
                                    x: coordinates[i].x || 0,
                                    y: coordinates[i].y || 0,
                                    duration: coordinates[i].duration || 0,
                                    origin: AdapterSelenium.Input.Origin.POINTER,
                                });
                        }
                    }
                }
            }
        }
        actions
            .release();
        await actions
            .perform();
        return coordinates;
    }

    async addEventListenerMousePosition() {
        await this.driver.executeScript(
            'window.mouseposx = 0; window.mouseposy = 0;' +
            '$(document).mousemove(' +
            '   (event) => {' +
            '       window.mouseposx = event.pageX;' +
            '       window.mouseposy = event.pageY;' +
            '   }' +
            ');'
        );
    }

    async getCurrentMousePosition() {
        let mousePos =
            await this.driver.executeScript(
                'return {' +
                '   x: window.mouseposx,' +
                '   y: window.mouseposy,' +
                '};'
            );
        //console.log('mouse.x: ' + mousePos.x + ', mouse.y: ' + mousePos.y);

        return mousePos;
    }

    async waitUntilElementInDom(element, milliseconds) {
        let returnValue = false;
        try {
            /*
            switch(element.selector.type) {
                case Element.Selector.Type.CSS:
                    await this.driver.findElement(Selenium.By.css(element.selector.value));
                    break;
                case Element.Selector.Type.XPATH:
                    await this.driver.findElement(Selenium.By.xpath(element.selector.value));
                    break;
                case Element.Selector.Type.LINK_TEXT:
                    await this.driver.findElement(Selenium.By.linkText(element.selector.value));
                    break;
            }
            */
            element.webElement = await this.driver.wait(Until.elementLocated(Selenium.By[element.selectors[0].type](element.selectors[0].value)), milliseconds);
            //console.log(await this.driver.executeScript('return arguments[0].outerHtml', element.webElement));
            return element;
        } catch(e) {
            return ExceptionHandler.createException(e, Exception.Severity.RECOVERABLE, Exception.Type.BROWSER_ADAPTER, 'Element ' + element.name + ' could not be found after ' + milliseconds + 'ms.');
        }
    }

    async waitUntilUrlIs(page, milliseconds) {
        let returnValue = false;
        try {
            /*
            switch(element.selector.type) {
                case Element.Selector.Type.CSS:
                    await this.driver.findElement(Selenium.By.css(element.selector.value));
                    break;
                case Element.Selector.Type.XPATH:
                    await this.driver.findElement(Selenium.By.xpath(element.selector.value));
                    break;
                case Element.Selector.Type.LINK_TEXT:
                    await this.driver.findElement(Selenium.By.linkText(element.selector.value));
                    break;
            }
            */
            await this.driver.wait(Until.urlIs(page.route.url), milliseconds);
            //console.log(await this.driver.getCurrentUrl());
            return page;
        } catch(e) {
            return ExceptionHandler.createException(e, Exception.Severity.RECOVERABLE, Exception.Type.BROWSER_ADAPTER, 'The URL did not change to ' + page.route.url + ' after ' + milliseconds + 'ms.');
        }
    }

    async executeScript(code, ...args) {
        args = args.map((arg) => {
            if(arg instanceof Element) {
                return arg.webElement;
            }

            return arg;
        });
        return await this.driver.executeScript(code, ...args);
    }

    async quit() {
        return await this.driver.quit();
    }
}

module.exports = AdapterSelenium;