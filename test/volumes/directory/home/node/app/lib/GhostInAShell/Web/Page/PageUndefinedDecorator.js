'use strict';

const Page = require('.');

class PageUndefinedDecorator extends Page
{
    constructor(configFilename, name, route, elements) {
        super(configFilename, name, route, elements);
        this._configFilename = '- Undefined Page -';
        this._name = [];
        this._route = null;
        this._elements = null;
    }
}

module.exports = PageUndefinedDecorator;