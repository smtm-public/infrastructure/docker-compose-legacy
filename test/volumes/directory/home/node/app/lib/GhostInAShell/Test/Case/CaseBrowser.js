'use strict';

const ExceptionHandler = require('../../System/ExceptionHandler');
const Exception = require('../../System/Exception');

const Case = require('.');
const CaseBrowserState = require('./CaseBrowserState');

const Page = require('../../Web/Page');
const Element = require('../../Web/Element');

class CaseBrowser extends Case
{
    get state() {
        return this._state;
    }
    set state(value) {
        this._state = value;
    }

    constructor(gias, name, validationCallable, callables) {
        validationCallable = validationCallable || CaseBrowser.validateDefault;
        super(gias, name, validationCallable, callables);
        this._state = new CaseBrowserState();
    }

    getCurrentPage() {
        return this.gias.browser.pages[this.state.currentPage];
    }

    getCurrentElement() {
        return this.gias.browser.getPageElement(this.state.currentPage, this.state.currentElement);
    }

    onPage(name) {
        this.callables.push(async () => {
            this.state.currentPage = name;
            return this.getCurrentPage();
        });

        return this;
    }

    element(name) {
        this.callables.push(async () => {
            return await this.elementSync(name);
        });
        return this;
    }

    elementSync(name) {
        this.state.currentElement = name;
        return this
            .gias
            .browser
            .findElementInstance(this.getCurrentElement());
    }

    clear() {
        this.callables.push(async () => {
            //console.log('CaseBrowser: clear: ' + this.getCurrentElement().name);
            return await this.getCurrentElement().clear(this.gias.browser);
        });
        return this;
    }

    sendKeys(...keys) {
        this.callables.push(async () => {
            //console.log('CaseBrowser: sendKeys: ' + this.getCurrentElement().name);
            return await this.getCurrentElement().sendKeys(this.gias.browser, ...keys);
        });
        return this;
    }

    click() {
        this.callables.push(async () => {
            return await this.clickSync();
        });
        return this;
    }

    clickSync() {
        //console.log('CaseBrowser: click: ' + this.getCurrentElement().name);
        return this.getCurrentElement().click(this.gias.browser);
    }

    check() {
        this.callables.push(async () => {
            //console.log('CaseBrowser: check: ' + this.getCurrentElement().name);
            return await this.getCurrentElement().check(this.gias.browser);
        });
        return this;
    }

    uncheck() {
        this.callables.push(async () => {
            //console.log('CaseBrowser: uncheck: ' + this.getCurrentElement().name);
            return await this.getCurrentElement().uncheck(this.gias.browser);
        });
        return this;
    }

    mouseMoveToCenterOfCurrentElement() {
        this.callables.push(async () => {
            return await this.mouseMoveToCenterOfCurrentElementSync();
        });
        return this;
    }

    mouseMoveToCenterOfCurrentElementSync() {
        //console.log('CaseBrowser: click: ' + this.getCurrentElement().name);
        return this.getCurrentElement().mouseMoveToCenter(this.gias.browser);
    }

    dragAndDrop(coordinates) {
        this.callables.push(async () => {
            return await this.dragAndDropSync(coordinates);
        });
        return this;
    }

    dragAndDropSync(coordinates) {
        //console.log('CaseBrowser: click: ' + this.getCurrentElement().name);
        return this.gias.browser.dragAndDrop(coordinates);
    }

    waitUntilElementInDom(name, milliseconds) {
        this.callables.push(async () => {
            let now = new Date();
            //console.log('CaseBrowser: waitUntilElementInDom: ' + name);
            const result = await this.waitUntilElementInDomSync(name, milliseconds);
            //console.log('CaseBrowser: waitUntilElementInDom: ' + name + ' resulted in a ' + (typeof result) + '. Took ' + (((new Date()).getTime() - now.getTime()) / 1000) + ' seconds.');
            return result;
        });
        return this;
    }

    waitUntilElementInDomSync(name, milliseconds) {
        this.state.currentElement = name;
        return this.gias.browser.waitUntilElementInDom(
            name,
            milliseconds
        );
    }

    waitUntilElementInDomInstance(element, milliseconds) {
        this.callables.push(async () => {
            let now = new Date();
            const result = await this.waitUntilElementInDomInstanceSync(element, milliseconds);
            //console.log('CaseBrowser: waitUntilElementInDomInstance: ' + element.name + ' resulted in a ' + (typeof result) + '. Took ' + (((new Date()).getTime() - now.getTime()) / 1000) + ' seconds.');
            return result;
        });
        return this;
    }

    waitUntilElementInDomInstanceSync(element, milliseconds) {
        this.state.currentElement = element.name;
        return this.gias.browser.waitUntilElementInDomInstance(
            element,
            milliseconds
        );
    }

    enumeratePageElements(pageName) {
        this.callables.push(
            ...this.gias.browser.pages[pageName].elements.map(
                (element) => {
                    return this.gias.browser.findElementInstance.bind(this.gias.browser, this.gias.browser.getPageElement(pageName, element.name));
                }
            )
        );
        return this;
    }

    waitUntilPageLoaded(name, milliseconds) {
        this.callables.push(async () => {
            //console.log('CaseBrowser: waitUntilPageLoaded: ' + name);
            return  await this.waitUntilPageLoadedSync(name, milliseconds);
        });
        return this;
    }

    waitUntilPageLoadedSync(name, milliseconds) {
        this.state.currentPage = name;
        return this.gias.browser.waitUntilPageLoaded(
            name,
            milliseconds
        );
    }

    runAsync(name, asyncFunction) {
        this.callables.push(asyncFunction.bind(this));

        return this;
    }

    if(name, conditionAsyncCallable, thenAsyncCallable, elseAsyncCallable) {
        this.callables.push(
            async() => {
                let conditionResult = await conditionAsyncCallable.bind(this);
                return (conditionResult ? await thenAsyncCallable.bind(this) : (elseAsyncCallable ? await elseAsyncCallable.bind(this) : conditionResult));
            }
        );

        return this;
    }

    mapCallable(elements, callable) {
        elements.map(
            (element) => {
                return callable.apply(this, [element]);
            }
        );

        return this;
    }

    addTestAssembly(name, collections) {
        return this.gias.addTestAssembly(name, collections);
    }

    addTestCollection(name) {
        return this.gias.addTestCollection(name);
    }

    addTestCollectionLoadPage(name) {
        return this.gias.addTestCollectionLoadPage(name);
    }

    addTestCaseBrowser(name, callables) {
        return this.gias.addTestCaseBrowser(name, callables);
    }

    static validateDefault(results) {
        return 'browser true';
    }
}

module.exports = CaseBrowser;