'use strict';

const Exception = require('./Exception');

class ExceptionHandler
{
    static createException(previous, severity, type, message, code, callback, stack) {
        const exc = new Exception(previous, severity, type, message, code, callback, stack);
        //console.log(message);
        return exc;
    }

    static throwException(previous, severity, type, message, code, callback, stack) {
        throw new Exception(previous, severity, type, message, code, callback, stack);
    }
}

module.exports = ExceptionHandler;