'use strict';

const ExceptionHandler = require('../../System/ExceptionHandler');
const Exception = require('../../System/Exception');

//const State = require('./State');
const Element = require('.');

class ElementStyledCheckbox extends Element
{
    constructor(name, selectors, behavior, webElement) {
        super(name, selectors, behavior, webElement);
    }

    async uncheck(browser) {
        let returnValue;

        let as = await this.isChecked(browser);
        if(await this.isChecked(browser)) {
            returnValue = await this.click(browser);
        }

        let bs = await this.isChecked(browser);
        if(await this.isChecked(browser)) {
            returnValue = ExceptionHandler.createException(null, Exception.Severity.RECOVERABLE, Exception.Type.TEST, 'Could not uncheck checkbox ' + this.name);
        }

        return returnValue;
    }

    async check(browser) {
        let returnValue;

        if(!await this.isChecked(browser)) {
            returnValue = await this.click(browser);
        }

        if(!await this.isChecked(browser)) {
            returnValue = ExceptionHandler.createException(null, Exception.Severity.RECOVERABLE, Exception.Type.TEST, 'Could not check checkbox ' + this.name);
        }

        return returnValue;
    }

    async isChecked(browser) {
        let checkMarkComputedCss =
            await browser
                .executeScript(
                    'return {' +
                    '   opacity: window.getComputedStyle(arguments[0], ":after").getPropertyValue("opacity"),' +
                    '   visibility: window.getComputedStyle(arguments[0], ":after").getPropertyValue("visibility"),' +
                    '};',
                    this
                );

        return (checkMarkComputedCss.opacity === '1' || checkMarkComputedCss.visibility === 'visible');
    }
}

module.exports = ElementStyledCheckbox;