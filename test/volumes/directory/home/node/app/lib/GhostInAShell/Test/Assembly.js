'use strict';

const Collection = require('./Collection');
const Case = require('./Case');

class Assembly
{
    get gias() {
        return this._gias;
    }
    set gias(value) {
        this._gias = value;
    }

    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }

    get validationCallable() {
        return this._validationCallable;
    }
    set validationCallable(value) {
        this._validationCallable = value;
    }

    get collections() {
        return this._collections;
    }
    set collections(value) {
        this._collections = value;
    }

    constructor(gias, name, validationCallable, collections) {
        this._gias = gias;
        this._name = name;
        this._collections = collections || [];
        this._validationCallable = validationCallable || Assembly.validateDefault;
    }

    addCollection(name, cases) {
        this.collections.push(new Collection(this.gias, name, cases));

        return this;
    }

    async run() {
        const now = new Date();
        let results = {
            name: this.name,
            'run-date': now.getUTCFullYear() + '-' + (now.getUTCMonth() + 1 + '').padStart(2, '0') + '-' + (now.getUTCDate() + '').padStart(2, '0'),
            'run-time': (now.getUTCHours() + '').padStart(2, '0') + ':' + (now.getUTCMinutes() + '').padStart(2, '0') + ':' + (now.getUTCSeconds() + '').padStart(2, '0'),
            items: [],
        };
        for(let collectionIndex in this.collections) {
            if(this.collections.hasOwnProperty(collectionIndex)) {
                let result = await this.collections[collectionIndex].run();
                results.items[collectionIndex] = {
                    collection: this.collections[collectionIndex],
                    result: result,
                };
            }
        }
        results.time = ((new Date()).getTime() - now.getTime()) / 1000;
        results.validation = this.validationCallable(results);
        results.stats = Assembly.produceTestCasesStatistics(results);

        return results;
    }

    static validateDefault(results) {
        return true;
    }

    static produceTestCasesStatistics(assemblyResult) {
        return {
            total: assemblyResult.items.map((collection) => {
                return collection.result.items;
            }).reduce(
                (carry, collectionTestCases) => {
                    return carry + collectionTestCases.length;
                },
                0
            ),
            passed: assemblyResult.items.map((collection) => {
                return collection.result.items.filter(
                    (caseItem) => {
                        return caseItem.result.validation;
                    }
                );
            }).reduce(
                (carry, collectionTestCases) => {
                    return carry + collectionTestCases.length;
                },
                0
            ),
            failed: assemblyResult.items.map((collection) => {
                return collection.result.items.filter(
                    (caseItem) => {
                        return !caseItem.result.validation;
                    }
                );
            }).reduce(
                (carry, collectionTestCases) => {
                    return carry + collectionTestCases.length;
                },
                0
            ),
            skipped: 0,
        };
    }
}

module.exports = Assembly;