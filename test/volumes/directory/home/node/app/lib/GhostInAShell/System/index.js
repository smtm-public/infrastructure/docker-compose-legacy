'use strict';

const EventEmitter = require('./EventEmitter');
const Message = require('./Message');
const Output = require('./Output');

class System
{
    static get EventEmitter() {
        return EventEmitter;
    }

    static get Message() {
        return Message;
    }

    static get Output() {
        return Output;
    }

    get eventEmitter() {
        return this._eventEmitter;
    }
    set eventEmitter(value) {
        this._eventEmitter = value;
    }

    get output() {
        return this._output;
    }
    set output(value) {
        this._output = value;
    }

    get environment() {
        return this._environment;
    }
    set environment(value) {
        this._environment = value;
    }

    constructor() {
        this._eventEmitter = new EventEmitter();
        this._output = new Output();

        this._environment = {
            isWindows: process.platform === 'win32',
        };
    }
}

module.exports = System;