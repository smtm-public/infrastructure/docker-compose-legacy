'use strict';

const Assembly = require('./Assembly');
const Collection = require('./Collection');
const Case = require('./Case');
const CaseBrowser = require('./Case/CaseBrowser');

class Test
{
    static get Assembly() {
        return Assembly;
    }

    static get Collection() {
        return Collection;
    }

    static get Case() {
        return Case;
    }

    static get CaseBrowser() {
        return CaseBrowser;
    }
}

module.exports = Test;