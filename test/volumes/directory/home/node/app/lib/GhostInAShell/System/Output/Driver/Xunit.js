'use strict';

const xmlBuilder = require('xmlbuilder');

class Xunit {
    constructor(config) {
        this.config = config;
    }

    out(results) {
        let doc = xmlBuilder.create('assemblies');
        let currentAssembly;
        let currentCollection;
        let currentTest;
        results.items.forEach(
            (assembly) => {
                currentAssembly =
                    doc.ele(
                        'assembly',
                        {
                            'name': assembly.assembly['name'],
                            'test-framework': 'GhostInAShell',
                            'environment': [
                                'lang=node.js',
                                'platform=' + encodeURIComponent(process.platform),
                                'arch=' + process.arch,
                                'execPath=' + process.execPath,
                                'version=' + process.version,
                                'argv0=' + process.argv0,
                                process.argv.map((arg, index) => 'arg[' + index + ']=' + encodeURIComponent(arg)).join('&')
                            ].join('&'),
                            'run-date': assembly.result['run-date'],
                            'run-time': assembly.result['run-time'],
                            'time': assembly.result['time'],
                            'total': assembly.result.stats['total'],
                            'passed': assembly.result.stats['passed'],
                            'failed': assembly.result.stats['failed'],
                            'skipped': assembly.result.stats['skipped'],
                        },
                    );
                assembly.result.items.forEach(
                    (collection) => {
                        currentCollection =
                            currentAssembly.ele(
                                'collection',
                                {
                                    'name': collection.collection['name'],
                                    'time': collection.result['time'],
                                    'total': collection.result.stats['total'],
                                    'passed': collection.result.stats['passed'],
                                    'failed': collection.result.stats['failed'],
                                    'skipped': collection.result.stats['skipped'],
                                }
                            );
                        collection.result.items.forEach(
                            (test) => {
                                currentTest =
                                    currentCollection.ele(
                                        'test',
                                        {
                                            'name': test.case['name'],
                                            'time': test.result['time'],
                                            'status': test.result['status'],
                                        }
                                    );
                            }
                        );
                        currentTest.up();
                    }
                );
                currentCollection.up();
            }
        );
        doc.end(
            {
                pretty: true,
            }
        );
        console.log(doc.toString());
    }
}

module.exports = Xunit;