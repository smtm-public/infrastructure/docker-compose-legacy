'use strict';

const ClassSeleniumXElement = require('../SeleniumXElement');

class ClassSeleniumXElementFactory {
    static async create(seleniumX, instance) {
        let properties = {
            outerHtml: await seleniumX.driver.executeScript('return arguments[0].outerHTML;', instance),
            outerText: await seleniumX.driver.executeScript('return arguments[0].outerText;', instance),
            innerHtml: await seleniumX.driver.executeScript('return arguments[0].innerHTML;', instance),
            innerText: await seleniumX.driver.executeScript('return arguments[0].innerText;', instance),
            tagName: await instance.getTagName(),
        };
        let attributes = {};
        if (properties.tagName === 'input') {
            attributes.type = await instance.getAttribute('type');
        }
        let webDriverProperties = {
            id: await instance.getId(),
            text: await instance.getText(),
            rect: await instance.getRect(),
            //isDisabled: await element.isDisabled(),
            isDisplayed: await instance.isDisplayed(),
        };

        let state = {
            stale: false,
        };

        return new ClassSeleniumXElement(instance, properties, attributes, webDriverProperties, state);
    }
}

module.exports = ClassSeleniumXElementFactory;