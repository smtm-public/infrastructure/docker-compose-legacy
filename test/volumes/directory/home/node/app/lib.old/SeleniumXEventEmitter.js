'use strict';

const EventEmitter = require('events');

const CONTAINS = 1;
const EQUALS = 2;
const SUBSET = 4;
class ClassCondition {
    static get CONTAINS() {
        return CONTAINS;
    }

    static get EQUALS() {
        return EQUALS;
    }

    static get SUBSET() {
        return SUBSET;
    }
}

class ClassSeleniumXEventEmitter extends EventEmitter
{
    constructor() {
        super();

        this.multiEvents = [];
    }

    static get ClassCondition() {
        return ClassCondition;
    }

    emitEvents(events, args) {
        if(typeof events === 'string') {
            events = events.split(/[ ,]/);
        }

        for(let i=0; i<this.multiEvents.length; i++) {
            let intersect = this.multiEvents[i].events.filter(value => -1 !== events.indexOf(value));
            if(intersect.length && (this.multiEvents[i].condition === ClassSeleniumXEventEmitter.ClassCondition.EQUALS) && (intersect.length === this.multiEvents[i].events.length) && (intersect.length === events.length)) {
                this.multiEvents[i].handler(...args);
            } else {
                if(intersect.length && (this.multiEvents[i].condition === ClassSeleniumXEventEmitter.ClassCondition.SUBSET) && (intersect.length === this.multiEvents[i].events.length)) {
                    this.multiEvents[i].handler(...args);
                } else {
                    if(intersect.length && (this.multiEvents[i].condition === ClassSeleniumXEventEmitter.ClassCondition.CONTAINS) && (intersect.length === events.length)) {
                        this.multiEvents[i].handler(...args);
                    }
                }
            }
        }

        /*
        return events.map(
            (event) => {
                return this.emit(event, ...args);
            }
        );
        */
    }

    onEvents(events, handler, condition) {
        condition = condition || ClassSeleniumXEventEmitter.ClassCondition.EQUALS;

        this.multiEvents.push(
            {
                events: events,
                handler: handler,
                condition: condition
            },
        );
    }
}

module.exports = ClassSeleniumXEventEmitter;