'use strict';

const ClassSeleniumXBody = require('./SeleniumXBody');

class ClassSeleniumXDocument {
    constructor(documentInstance, bodyInstance) {
        this.instance = documentInstance;
        this.body = new ClassSeleniumXBody(bodyInstance);
    }
}

module.exports = ClassSeleniumXDocument;