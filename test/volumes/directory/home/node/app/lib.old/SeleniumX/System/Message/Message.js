'use strict';

const Type = require('./Type');

class Message
{
    get source() {
        return this._source;
    }

    set source(value) {
        this._source = value;
    }

    static get Type() {
        return Type;
    }

    get type() {
        return this._type;
    }

    set type(value) {
        this._type = value;
    }

    get text() {
        return this._text;
    }

    set text(value) {
        this._text = value;
    }

    constructor(source, type, text) {
        this._source = source;
        this._type = type;
        this._text = text;
    }
}

exports = Message;