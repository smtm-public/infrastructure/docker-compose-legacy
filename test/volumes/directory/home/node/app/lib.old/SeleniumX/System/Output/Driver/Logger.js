'use strict';

const ClassSeleniumXOutput = require('./SeleniumXOutput');
const ClassSeleniumXOutputType = require('../SeleniumXOutputType');

let BASIC = 1;
let NORMAL = 2;
let VERBOSE = 4;
let DEBUG = 8;

class ClassLogLevel {
    static get BASIC() {
        return BASIC;
    }

    static get NORMAL() {
        return NORMAL;
    }

    static get VERBOSE() {
        return VERBOSE;
    }

    static get DEBUG() {
        return DEBUG;
    }
}

class ClassSeleniumXOutputLogger {
    constructor(config) {
        this.config = config;
        this.config.logLevel = this.config.logLevel || ClassSeleniumXOutputLogger.ClassLogLevel.NORMAL;
    }

    static get ClassLogLevel() {
        return ClassLogLevel;
    }

    out(type, messages) {
        messages.forEach(
            (message) => {
                let preText = '';
                let postText = '';
                switch (type) {
                    case ClassSeleniumXOutputType.INFO:
                        preText = 'Info: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.ACTION:
                        preText = 'Action: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.TEST:
                        preText = 'TEST: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.CHECK:
                        preText = 'Check: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.PASSED:
                        preText = 'Passed: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.SUCCESS:
                        preText = 'SUCCESS: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.INIT:
                        preText = 'Init: ';
                        postText = '';
                        break;
                    case ClassSeleniumXOutputType.ENVIRONMENT:
                        preText = '<<<===>>> ';
                        postText = ' <<<===>>>';
                        break;
                    case ClassSeleniumXOutputType.PLAIN:
                    default:
                        preText = '';
                        postText = '';
                        break;
                }
                console.log(preText + message + postText);
            }
        )
    }
}

module.exports = ClassSeleniumXOutputLogger;