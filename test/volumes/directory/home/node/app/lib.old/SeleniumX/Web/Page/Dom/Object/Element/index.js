'use strict';

//const { WebElement } = require('selenium-webdriver/lib/webdriver');
class Factory {
    static async create(seleniumX, instance) {
        let properties = {
            outerHtml: await seleniumX.driver.executeScript('return arguments[0].outerHTML;', instance),
            outerText: await seleniumX.driver.executeScript('return arguments[0].outerText;', instance),
            innerHtml: await seleniumX.driver.executeScript('return arguments[0].innerHTML;', instance),
            innerText: await seleniumX.driver.executeScript('return arguments[0].innerText;', instance),
            tagName: await instance.getTagName(),
        };
        let attributes = {};
        if (properties.tagName === 'input') {
            attributes.type = await instance.getAttribute('type');
        }
        let webDriverProperties = {
            id: await instance.getId(),
            text: await instance.getText(),
            rect: await instance.getRect(),
            //isDisabled: await element.isDisabled(),
            isDisplayed: await instance.isDisplayed(),
        };

        let state = {
            stale: false,
        };

        return new Element(instance, properties, attributes, webDriverProperties, state);
    }
}

class Element {
    static get Factory() {
        return Factory;
    }

    get instance() {
        return this._instance;
    }

    set instance(value) {
        this._instance = value;
    }

    get properties() {
        return this._properties;
    }

    set properties(value) {
        this._properties = value;
    }

    get attributes() {
        return this._attributes;
    }

    set attributes(value) {
        this._attributes = value;
    }

    get webDriverProperties() {
        return this._webDriverProperties;
    }

    set webDriverProperties(value) {
        this._webDriverProperties = value;
    }

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = value;
    }

    constructor(instance, properties, attributes, webDriverProperties, state) {
        this._instance = instance;
        this._properties = properties;
        this._attributes = attributes;
        this._webDriverProperties = webDriverProperties;
        this._state = state;
    }
}

exports = Element;