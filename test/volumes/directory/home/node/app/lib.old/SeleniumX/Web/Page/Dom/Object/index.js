exports = {
    Window: require('../../Window'),
    Document: require('./Document'),
    Element: require('./Element'),
};