'use strict';

const Assembly = require('./Assembly');
const Collection = require('./Collection');

class Test
{
    static get Assembly() {
        return Assembly;
    }

    static get Collection() {
        return Collection;
    }
}

module.exports = Test;