'use strict';

const Logger = require('./Driver/Logger');
const Xunit = require('./Driver/Xunit');

class Output
{
    get config() {
        return this._config;
    }

    set config(value) {
        this._config = value;
    }

    get driver() {
        return this._driver;
    }

    set driver(value) {
        this._driver = value;
    }

    constructor(config, driver) {
        this._config = config;
        this._driver = driver || new ClassSeleniumXOutputLogger({});
    }

    out(seleniumX, type, ...args) {
        switch(typeof this.config.driver) {
            case ClassSeleniumXOutputXunit:
                break;
            case ClassSeleniumXOutputLogger:

            default:
                break;
        }
    }
}

module.exports = ClassSeleniumXOutput;