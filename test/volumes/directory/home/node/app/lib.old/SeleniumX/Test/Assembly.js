'use strict';

class Assembly
{
    get seleniumX() {
        return this._seleniumX;
    }

    set seleniumX(value) {
        this._seleniumX = value;
    }

    constructor(seleniumX) {
        this._seleniumX = seleniumX;
    }

    async runTest(file) {
        this.seleniumX.system.eventEmitter.emitEvents(
            ['test', 'suite-onboarding', 'main', 'before', 'test-no-authentication-login'],
            [seleniumX, process.env.LOGIN_USER_EMAIL]);
        const TestClass = require(file);
        const test = new TestClass();
        await test.boot();
        await test.main();
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-no-authentication-login'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
    }
}

module.exports = Assembly;