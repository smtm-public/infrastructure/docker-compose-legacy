'use strict';

let PLAIN = 1;
let INFO = 2;
let ACTION = 4;
let TEST = 8;
let CHECK = 16;
let PASSED = 32;
let SUCCESS = 64;
let INIT = 128;
let ENVIRONMENT = 256;
let INPUT = 512;
let STATE = 1024;

class Type {
    static get PLAIN() {
        return PLAIN;
    }

    static get INFO() {
        return INFO;
    }

    static get ACTION() {
        return ACTION;
    }

    static get TEST() {
        return TEST;
    }

    static get CHECK() {
        return CHECK;
    }

    static get PASSED() {
        return PASSED;
    }

    static get SUCCESS() {
        return SUCCESS;
    }

    static get INIT() {
        return INIT;
    }

    static get ENVIRONMENT() {
        return ENVIRONMENT;
    }

    static get INPUT() {
        return INPUT;
    }

    static get STATE() {
        return STATE;
    }
}

exports = Type;