'use strict';

//const System = require('./System');
//const Test = require('./Test');
const Web = require('./Web');

class GhostInAShell {
    static get System() {
        return System;
    }

    static get Test() {
        return Test;
    }

    static get Web() {
        return Web;
    }

    get config() {
        return this._config;
    }

    set config(value) {
        this._config = value;
    }

    get routes() {
        return this._routes;
    }

    set routes(value) {
        this._routes = value;
    }

    constructor(config, routes) {
        this._config = config;
        this._routes = routes;
    }
}

exports = GhostInAShell;