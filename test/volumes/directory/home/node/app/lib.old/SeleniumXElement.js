'use strict';

const { WebElement } = require('selenium-webdriver/lib/webdriver');

class ClassSeleniumXElement {
    get instance() {
        return this._instance;
    }

    set instance(value) {
        this._instance = value;
    }

    get properties() {
        return this._properties;
    }

    set properties(value) {
        this._properties = value;
    }

    get attributes() {
        return this._attributes;
    }

    set attributes(value) {
        this._attributes = value;
    }

    get webDriverProperties() {
        return this._webDriverProperties;
    }

    set webDriverProperties(value) {
        this._webDriverProperties = value;
    }

    get state() {
        return this._state;
    }

    set state(value) {
        this._state = value;
    }

    constructor(instance, properties, attributes, webDriverProperties, state) {
        this._instance = instance;
        this._properties = properties;
        this._attributes = attributes;
        this._webDriverProperties = webDriverProperties;
        this._state = state;
    }
}

module.exports = ClassSeleniumXElement;