const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingStep1Page {
    static async main(seleniumX) {
        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1,
            'onboarding step1',
            true,
        );

        await seleniumX.doBasicTests('onboarding step1');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1-page', 'begin'], [seleniumX]);

        let onboardingStep1PageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h2[contains(string(), 'Wann soll der Vertrag')]"),
                    'Check: Looking for the heading on the onboarding step1 page...',
                ],
                'Found the heading on the onboarding step1 page.',
                '>>>>>> Error: No heading found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//span[contains(string(), 'Ab Sofort')]"),
                    'Check: Looking for the start contract immediately radio button on the onboarding step1 page...',
                ],
                'Found the start contract immediately radio button on the onboarding step1 page.',
                '>>>>>> Error: No start contract immediately radio button found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//span[contains(string(), 'Datum')]"),
                    'Check: Looking for the specify contract starting date radio button on the onboarding step1 page...',
                ],
                'Found the specify contract starting date radio button on the onboarding step1 page.',
                '>>>>>> Error: No specify contract starting date radio button found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//span[contains(string(), 'Vertragswechsel')]"),
                    'Check: Looking for the terminate existing contract radio button on the onboarding step1 page...',
                ],
                'Found the terminate existing contract radio button on the onboarding step1 page.',
                '>>>>>> Error: No terminate existing contract radio button found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.name('selection2'),
                    'Check: Looking for the user record verification hidden checkbox on the onboarding step1 page...',
                ],
                'Found the user record verification hidden checkbox on the onboarding step1 page.',
                '>>>>>> Error: No user record verification hidden checkbox found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('input[name="selection2"]+span.checkbox'),
                    'Check: Looking for the user record verification styled checkbox span on the onboarding step1 page...',
                ],
                'Found the user record verification styled checkbox span on the onboarding step1 page.',
                '>>>>>> Error: No user record verification styled checkbox span found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//button[contains(string(), 'WEITER')]"),
                    'Check: Looking for the submit button on the onboarding step1 page...',
                ],
                'Found the submit button on the onboarding step1 page.',
                '>>>>>> Error: No submit button found on the onboarding step1 page.',
            ),
        ];
        let onboardingStep1PageElements = await seleniumX.runAsync(
            onboardingStep1PageElementsTasks
        );
        seleniumX.validateAllResultsSimple(onboardingStep1PageElementsTasks, onboardingStep1PageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding step1 page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding step1 page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingStep1Page;