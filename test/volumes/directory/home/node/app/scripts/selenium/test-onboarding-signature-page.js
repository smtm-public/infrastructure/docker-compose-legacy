const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingSignaturePage {
    static async main(seleniumX) {
        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_SIGNATURE,
            'onboarding signature',
            true,
        );

        await seleniumX.doBasicTests('onboarding signature');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-signature-page', 'begin'], [seleniumX]);

        let onboardingSignaturePageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h1[contains(string(), 'Und zum Abschluss: Deine Unterschrift.')]"),
                    'Check: Looking for the heading on the onboarding signature page...',
                ],
                'Found the heading on the onboarding signature page.',
                '>>>>>> Error: No heading found on the onboarding signature page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('div.box-signature'),
                    'Check: Looking for the signature canvas container on the onboarding signature page...',
                ],
                'Found the signature canvas container on the onboarding signature page.',
                '>>>>>> Error: No signature canvas container found on the onboarding signature page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('div.box-signature>canvas'),
                    'Check: Looking for the signature canvas on the onboarding signature page...',
                ],
                'Found the signature canvas on the onboarding signature page.',
                '>>>>>> Error: No signature canvas found on the onboarding signature page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('button[type="submit"]'),
                    'Check: Looking for the submit button on the onboarding signature page...',
                ],
                'Found the submit button on the onboarding signature page.',
                '>>>>>> Error: No submit button found on the onboarding signature page.',
            ),
        ];
        let onboardingSignaturePageElements = await seleniumX.runAsync(onboardingSignaturePageElementsTasks);
        seleniumX.validateAllResultsSimple(onboardingSignaturePageElementsTasks, onboardingSignaturePageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-signature-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-signature-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding signature page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-signature-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding signature page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingSignaturePage;