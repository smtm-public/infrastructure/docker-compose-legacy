const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const Until = require('selenium-webdriver/lib/until');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardLandingPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.LOGIN,
            'dashboard contract select',
            true,
        );

        // await seleniumX.navigate(StaticClassSeleniumX.ClassSeleniumXRoutes.LOGIN);
         // to be replace with Milens login implementation
        // await this.login(seleniumX, 'a@a.a', '12stilko12')


        let privateIcon = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.xpath('//form/div/div/div[1]/ul/li[1]/a/i'));

        await seleniumX.click(privateIcon);

        await seleniumX.doBasicTests('dashboard');

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-page', 'begin'], [seleniumX]);

        let dashboardPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('ANZEIGEN'),
                    'Check: Looking for "ANZEIGEN" button on the dashboard page...',
                ],
                'Found "ANZEIGEN" button on the dashboard page.',
                '>>>>>> Error: The "ANZEIGEN" button is missing on the dashboard page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('ANTWORTEN FINDEN'),
                    'Check: Looking for "ANTWORTEN FINDEN" button on the dashboard page...',
                ],
                'Found "ANTWORTEN FINDEN" button on the dashboard page.',
                '>>>>>> Error: The "ANTWORTEN FINDEN" button is missing on the dashboard page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('HELDEN STATUS ANZEIGEN'),
                    'Check: Looking for "HELDEN STATUS ANZEIGEN" button on the dashboard page...',
                ],
                'Found "HELDEN STATUS ANZEIGEN" button on the dashboard page.',
                '>>>>>> Error: The "HELDEN STATUS ANZEIGEN" button is missing on the dashboard page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('LADE DEINE FREUNDE EIN'),
                    'Check: Looking for "LADE DEINE FREUNDE EIN" button on the dashboard page...',
                ],
                'Found "LADE DEINE FREUNDE EIN" button on the dashboard page.',
                '>>>>>> Error: The "LADE DEINE FREUNDE EIN" button is missing on the dashboard page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('WIE FUNKTIONIERT´S?'),
                    'Check: Looking for "WIE FUNKTIONIERT´S?" button on the dashboard page...',
                ],
                'Found "WIE FUNKTIONIERT´S?" button on the dashboard page.',
                '>>>>>> Error: The WIE FUNKTIONIERT´S?" button is missing on the dashboard page.'
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('LADE DEINE FREUNDE EIN'),
                    'Check: Looking for "LADE DEINE FREUNDE EIN" button on the dashboard page...',
                ],
                'Found "LADE DEINE FREUNDE EIN" button on the dashboard page.',
                '>>>>>> Error: The "LADE DEINE FREUNDE EIN" button is missing on the dashboard page.'
            ),
        ];

        let dashboardPageElements = await seleniumX.runAsync(dashboardPageElementsTasks);
        seleniumX.validateAllResultsSimple(dashboardPageElementsTasks, dashboardPageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-page', 'content', 'success'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-page', 'buttons-correct-redirect'], [seleniumX]);

        let pagesConstants = [
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBAORD_SETTINGS,
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INFO_QUESTIONS,
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_STATUS,
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INVITE,
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INFO_HOW,
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INVITE,
        ];

        let buttonNames = [];
        for (let b = 0; b < dashboardPageElements.length; b++) {
            await buttonNames.push(await dashboardPageElements[b].webDriverProperties.text);
        }

        for (let i = 0; i < dashboardPageElements.length; i++) {

            let button = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText(buttonNames[i]));

            await seleniumX.click(button);

            if (await seleniumX.getCurrentUrl() === pagesConstants[i]) {
                seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-page', 'buttons-correct-redirect', buttonNames[i], 'passed'], [seleniumX]);
            } else {
                seleniumX.exceptionHandler.throwException(
                    ClassSeleniumXException.ClassSeverity.FATAL,
                    ClassSeleniumXException.ClassType.TEST,
                    'Button ' + buttonNames[i] + ' doesn\'t redirect to ' + pagesConstants[i] + ' page!',
                );
            }
            let turnBackLink = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Übersicht'));
            await seleniumX.click(turnBackLink);
        }

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-page', 'buttons-correct-redirect', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'content', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard page buttons redirect to the correct links.', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect', 'ANZEIGEN', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Button ANZEIGEN redirect to the correct link.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect', 'ANTWORTEN FINDEN', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Button ANTWORTEN FINDEN redirect to the correct link.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect', 'HELDEN STATUS ANZEIGEN', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Button HELDEN STATUS ANZEIGEN redirect to the correct link.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect', 'LADE DEINE FREUNDE EIN', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Button LADE DEINE FREUNDE EIN redirect to the correct link.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect', 'WIE FUNKTIONIERT´S?', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Button WIE FUNKTIONIERT´S? redirect to the correct link.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect', 'LADE DEINE FREUNDE EIN', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Button LADE DEINE FREUNDE EIN redirect to the correct link.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-page', 'buttons-correct-redirect', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully All buttons redirect to the correct link.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }

    // to be removed after Milens Login implementation
    static async login(seleniumX, email, passwd) {

        seleniumX = seleniumX || await new ClassSeleniumX();

        let emailField = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="email"]'));
        let passwordField = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="password"]'));
        let loginButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.id('login-button'));

        await emailField.instance.sendKeys(email);
        await passwordField.instance.sendKeys(passwd);
        await seleniumX.click(loginButton);
    }
}

module.exports = TestDashboardLandingPage;

