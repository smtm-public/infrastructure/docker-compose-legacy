const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');
const path = require('path');

class NoAuthenticationLogin extends Test {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.LOGIN,
            'login',
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login', 'begin'], [seleniumX]);

        let submitButton = await seleniumX.findElementsAsync(StaticClassSeleniumX.Selenium.By.css('[type="submit"]'));

        let email = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="email"]'));
        let password = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="password"]'));

        seleniumX.eventEmitter.emitEvents(['test-no-authentication-login', 'check', 'empty', 'values', 'validation'], [seleniumX]);
        let emptyValuesFormTest =
            await seleniumX.formTest(
                [
                    [email, StaticClassSeleniumX.Selenium.Key.CONTROL, 'a', StaticClassSeleniumX.Selenium.Key.NULL, StaticClassSeleniumX.Selenium.Key.DELETE],
                    [password, StaticClassSeleniumX.Selenium.Key.CONTROL, 'a', StaticClassSeleniumX.Selenium.Key.NULL, StaticClassSeleniumX.Selenium.Key.DELETE],
                ],
                submitButton[0],
                () => {
                    return (async () => {
                        let emptyEmailValidationErrorElements = await seleniumX.findElementsAsync(
                            StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen deine E-Mail Adresse.')]"),
                            'Check: Looking for empty email validation error message HTML elements...',
                        );
                        let emptyPasswordValidationErrorElements = await seleniumX.findElementsAsync(
                            StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen dein Passwort zum einloggen.')]"),
                            'Check: Looking for empty password validation error message HTML elements...',
                        );

                        return (emptyEmailValidationErrorElements.length && emptyPasswordValidationErrorElements.length);
                    })()
                        .then(
                            (outcome) => {
                                if (outcome) {
                                    seleniumX.eventEmitter.emitEvents(['test-no-authentication-login', 'check', 'empty', 'values', 'validation', 'passed'], [seleniumX]);

                                    return true;
                                } else {
                                    seleniumX.exceptionHandler.throwException(
                                        ClassSeleniumXException.ClassSeverity.FATAL,
                                        ClassSeleniumXException.ClassType.TEST,
                                        'Validation is not functioning properly.',
                                    );
                                }
                            }
                        );
                }
            );

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login', 'before', 'email', 'field', 'validation'], [seleniumX]);
        let emailTestCases = [
            'test',
            'test@asas',
            'tesqw    et@abv.БГ',
        ];
        let emailTestCasesResults = [];
        while (emailTestCases && emailTestCases.length) {
            let text = emailTestCases.shift();
            emailTestCasesResults.push(
                await seleniumX.formTest(
                    [[email, text]],
                    submitButton[0],
                    () => {
                        return seleniumX.findElementsAsync(
                            StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Deine E-Mail Adresse sieht nicht richtig aus.')]"),
                            'Check: Testing the email field with value: ' + text + '. Looking for email validation error message HTML elements...',
                        )
                            .then(
                                (fieldErrorText) => {
                                    if (fieldErrorText.length) {
                                        seleniumX.eventEmitter.emitEvents(['test-no-authentication-login', 'check', 'email', 'field', 'validation', 'passed'], [seleniumX, text]);

                                        return seleniumX.sendKeys(email, StaticClassSeleniumX.Selenium.Key.CONTROL, 'a', StaticClassSeleniumX.Selenium.Key.NULL, StaticClassSeleniumX.Selenium.Key.DELETE)
                                            .then(
                                                () => {
                                                    return true;
                                                }
                                            )
                                    } else {
                                        seleniumX.exceptionHandler.throwException(
                                            ClassSeleniumXException.ClassSeverity.FATAL,
                                            ClassSeleniumXException.ClassType.TEST,
                                            'Validation is not functioning properly.',
                                        );
                                    }
                                }
                            )
                    }
                )
            )
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login', 'after', 'email', 'field', 'validation', 'success'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login', 'before', 'valid', 'login'], [seleniumX]);
        let loginUserEmail = process.env.LOGIN_USER_EMAIL;
        let testValidLogin = await seleniumX.formTest(
                [
                    [email, loginUserEmail],
                    [password, '1]2[3p4o'],
                ],
                submitButton[0],
                () => {
                    // DEBUUUUUUUUUUUUUUUUUUUUUUUUUUUGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG
                    return (
                        !seleniumX.existsElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen eine E-Mail Adresse von Dir.')]"))
                        &&
                        !seleniumX.existsElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen ein Passwort um dich anzumelden.')]"))
                        &&
                        !seleniumX.existsElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Dein Passwort muss mindestens 8 Zeichen lang sein.')]"))
                    );
                },
            );
        let outcome = await Promise.race([
            seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT, 40000),
            seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.CONTRACT_SELECT, 40000),
            seleniumX.driver.wait(new StaticClassSeleniumX.WebDriver.Condition('to see if user does not exists', async() => {
                let existsElement = await seleniumX.existsElementAsync(
                    StaticClassSeleniumX.Selenium.By.xpath(
                        "//div[contains(string(), 'Deine Logindaten sind leider nicht korrekt.')]",
                    ),
                );
                if(existsElement) {
                    return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//div[contains(string(), 'Deine Logindaten sind leider nicht korrekt.')]"));
                }

                return false;
            }), 40000),
        ]);
        if(outcome instanceof StaticClassSeleniumX.WebDriver.WebElement) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'Looks like the email ' + loginUserEmail + ' has not been registered. Cannot continue the login functionality test. Aborting.',
            );
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login', 'after', 'valid', 'login', 'passed'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login', 'success'], [seleniumX]);
        //await seleniumX.takeScreenshot(true, true);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-login', 'check', 'empty', 'values', 'validation'],
            (seleniumX) => {
                seleniumX.logger.log('Trying to submit the form with empty values for email and password...', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-login', 'check', 'empty', 'values', 'validation', 'passed'],
            (seleniumX, text) => {
                seleniumX.logger.log('Field validation is working with empty values for email and password.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-login', 'before', 'email', 'field', 'validation'],
            (seleniumX) => {
                seleniumX.logger.log('Trying to submit the form with invalid values for email...', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-login', 'check', 'email', 'field', 'validation', 'passed'],
            (seleniumX, text) => {
                seleniumX.logger.log('Field validation is working. Tested with: ' + text, ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-login', 'after', 'email', 'field', 'validation', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Email validation is working properly.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-login', 'check', 'password', 'field', 'validation', 'passed'],
            (seleniumX, text) => {
                seleniumX.logger.log('Field validation is working. Tested with: ' + text, ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-login', 'before', 'valid', 'login'],
            (seleniumX) => {
                seleniumX.logger.log('Trying to login with valid credentials...', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-login', 'after', 'valid', 'login', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Logged in successfully.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-login', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The login page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestNoAuthenticationLogin;