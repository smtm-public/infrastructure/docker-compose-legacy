const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');

class TestNoAuthenticationLoginPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.LOGIN,
            'login',
        );

        await seleniumX.doBasicTests('login');

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login-page', 'begin'], [seleniumX]);

        let loginPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Jetzt registrieren.'),
                    'Check: Looking for the registration page link on the login page...',
                ],
                'Found the registration page link on the login page.',
                '>>>>>> Error: The registration page link is missing on the login page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Passwort vergessen?'),
                    'Check: Looking for the forgot password page link on the login page...',
                ],
                'Found the forgot password page link on the login page.',
                '>>>>>> Error: The forgot password page link is missing on the login page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="email"]'),
                    'Check: Looking for email input form fields on the login page...',
                ],
                'Found email input form fields on the login page.',
                '>>>>>> Error: No email input form fields found on the login page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="password"]'),
                    'Check: Looking for password input form fields on the login page...',
                ],
                'Found password input forms field on the login page.',
                '>>>>>> Error: No password input form fields found on the login page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.id('login-button'),
                    'Check: Looking for the submit button on the login page...',
                ],
                'Found the submit button on the login page.',
                '>>>>>> Error: The submit button is missing on the login page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.id('fb-login-button'),
                    'Check: Looking for Facebook submit button on the login page...',
                ],
                'Found the Facebook submit button on the login page.',
                '>>>>>> Error: The Facebook submit button is missing on the login page.',
            ),
        ];
        let loginPageElements = await seleniumX.runAsync(loginPageElementsTasks);
        seleniumX.validateAllResultsSimple(loginPageElementsTasks, loginPageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-login-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-login-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the login page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-login-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the login page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestNoAuthenticationLoginPage;