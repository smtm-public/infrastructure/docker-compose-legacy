const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const Until = require('selenium-webdriver/lib/until');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardInfoHowPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INFO_HOW,
            'dashboard info how page',
            true,
        );

        await seleniumX.doBasicTests('dashboard info how');


        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-info-how-page', 'content', 'begin'], [seleniumX]);

        let infoHowPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('JETZT HELDEN CODE TEILEN'),
                    'Check: Looking for "JETZT HELDEN CODE TEILEN" button on the info how page...',
                ],
                'Found "JETZT HELDEN CODE TEILEN" button on the info how page.',
                '>>>>>> Error: The "JETZT HELDEN CODE TEILEN" button is missing on the info how page.',
            ),
        ];
        let infoHowPageElements = await seleniumX.runAsync(infoHowPageElementsTasks);
        seleniumX.validateAllResultsSimple(infoHowPageElementsTasks, infoHowPageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-info-how-page', 'content', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-info-how-page', 'content', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Info How page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-info-how-page', 'content', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Info How page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardInfoHowPage;

