const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');

class TestOnboardingStep2Page {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP2,
            'onboarding step2',
            true,
        );

        await seleniumX.doBasicTests('onboarding step2');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step2-page', 'begin'], [seleniumX]);

        let onboardingStep2PageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h2[contains(string(), 'Wer bist du?')]"),
                    'Check: Looking for the heading on the onboarding step2 page...',
                ],
                'Found the heading on the onboarding step2 page.',
                '>>>>>> Error: The heading is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="submit"]'),
                    'Check: Looking for submit buttons on the onboarding step2 page...',
                ],
                'Found submit buttons on the onboarding step2 page.',
                '>>>>>> Error: No submit buttons found on the onboarding step2 page.',
            ),
        ];
        let onboardingStep2PageElements = await seleniumX.runAsync(
            onboardingStep2PageElementsTasks
        );
        seleniumX.validateAllResultsSimple(onboardingStep2PageElementsTasks, onboardingStep2PageElements);

        let onboardingStep2PageFormElementsLabelParentsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Vorname')]/.."),
                    'Check: Looking for the parent container of the first name input label on the onboarding step2 page...',
                ],
                'Found the parent container of the first name input label on the onboarding step2 page.',
                '>>>>>> Error: The parent container of the first name input label is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Nachname')]/.."),
                    'Check: Looking for the parent container of the last name input label on the onboarding step2 page...',
                ],
                'Found the parent container of the last name input label on the onboarding step2 page.',
                '>>>>>> Error: The parent container of the last name input label is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Straße')]/.."),
                    'Check: Looking for the parent container of the street input label on the onboarding step2 page...',
                ],
                'Found the parent container of the street input label on the onboarding step2 page.',
                '>>>>>> Error: The parent container of the street input label is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Hausnummer')]/.."),
                    'Check: Looking for the parent container of the address number input label on the onboarding step2 page...',
                ],
                'Found the parent container of the address number input label on the onboarding step2 page.',
                '>>>>>> Error: The parent container of the address number input label is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Postleitzahl')]/.."),
                    'Check: Looking for the parent container of the postal code input label on the onboarding step2 page...',
                ],
                'Found the parent container of the postal code input label on the onboarding step2 page.',
                '>>>>>> Error: The parent container of the postal code input label is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Ort')]/.."),
                    'Check: Looking for the parent container of the city input label on the onboarding step2 page...',
                ],
                'Found the parent container of the city input label on the onboarding step2 page.',
                '>>>>>> Error: The parent container of the city input label is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Mobilnummer')]/.."),
                    'Check: Looking for the parent container of the phone number input label on the onboarding step2 page...',
                ],
                'Found the parent container of the phone number input label on the onboarding step2 page.',
                '>>>>>> Error: The parent container of the phone number input label is missing from the onboarding step2 page.',
            ),
        ];
        let onboardingStep2PageFormElementsLabelParents = await seleniumX.runAsync(
            onboardingStep2PageFormElementsLabelParentsTasks
        );
        seleniumX.validateAllResultsSimple(onboardingStep2PageFormElementsLabelParentsTasks, onboardingStep2PageFormElementsLabelParents);

        let onboardingStep2PageFormElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findChildrenOfElementAsync,
                seleniumX,
                [
                    onboardingStep2PageFormElementsLabelParents[0],
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the first name text input on the onboarding step2 page...',
                ],
                'Found the first name text input on the onboarding step2 page.',
                '>>>>>> Error: The first name text input is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findChildrenOfElementAsync,
                seleniumX,
                [
                    onboardingStep2PageFormElementsLabelParents[1],
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the last name text input on the onboarding step2 page...',
                ],
                'Found the last name text input on the onboarding step2 page.',
                '>>>>>> Error: The last name text input is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findChildrenOfElementAsync,
                seleniumX,
                [
                    onboardingStep2PageFormElementsLabelParents[2],
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the street text input on the onboarding step2 page...',
                ],
                'Found the street text input on the onboarding step2 page.',
                '>>>>>> Error: The street text input is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findChildrenOfElementAsync,
                seleniumX,
                [
                    onboardingStep2PageFormElementsLabelParents[3],
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the address number text input on the onboarding step2 page...',
                ],
                'Found the address number text input on the onboarding step2 page.',
                '>>>>>> Error: The address number text input is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findChildrenOfElementAsync,
                seleniumX,
                [
                    onboardingStep2PageFormElementsLabelParents[4],
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the postal code text input on the onboarding step2 page...',
                ],
                'Found the postal code text input on the onboarding step2 page.',
                '>>>>>> Error: The postal code text input is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findChildrenOfElementAsync,
                seleniumX,
                [
                    onboardingStep2PageFormElementsLabelParents[5],
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the city text input on the onboarding step2 page...',
                ],
                'Found the city text input on the onboarding step2 page.',
                '>>>>>> Error: The city text input is missing from the onboarding step2 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findChildrenOfElementAsync,
                seleniumX,
                [
                    onboardingStep2PageFormElementsLabelParents[6],
                    StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                    'Check: Looking for the phone number text input on the onboarding step2 page...',
                ],
                'Found the phone number text input on the onboarding step2 page.',
                '>>>>>> Error: The phone number text input is missing from the onboarding step2 page.',
            ),
        ];
        let onboardingStep2PageFormElements = await seleniumX.runAsync(
            onboardingStep2PageFormElementsTasks
        );
        seleniumX.validateAllResults(
            onboardingStep2PageFormElementsTasks,
            onboardingStep2PageFormElements,
            (result) => {
                return (typeof result === 'object' && result.length === 1);
            }
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step2-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step2-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding step2 page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step2-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding step2 page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestOnboardingStep2Page;