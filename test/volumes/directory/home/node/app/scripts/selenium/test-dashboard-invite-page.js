const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const Until = require('selenium-webdriver/lib/until');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardInvitePage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INVITE,
            'dashboard invite',
            true,
        );

        await seleniumX.doBasicTests('dashboard invite');

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-invite-page', 'begin'], [seleniumX]);

        let invitePageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="submit"]'),
                    'Check: Looking for the "FREUNDE EINLADEN" button on the Dashboard Invite page...',
                ],
                'Found the "FREUNDE EINLADEN" button on the Dashboard Invite page.',
                '>>>>>> Error: The "FREUNDE EINLADEN" button is missing on the Dashboard Invite page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('textarea'),
                    'Check: Looking for textarea element on the Dashboard Invite page...',
                ],
                'Found the textarea element on the Dashboard Invite page.',
                '>>>>>> Error: The textarea element is missing on the Dashboard Invite page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Facebook'),
                    'Check: Looking for the Facebook icon on the Dashboard Invite page...',
                ],
                'Found the Facebook icon on the Dashboard Invite page.',
                '>>>>>> Error: The Facebook icon is missing on the Dashboard Invite page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Twitter'),
                    'Check: Looking for the Twitter icon on the Dashboard Invite page...',
                ],
                'Found the Twitter icon on the Dashboard Invite page.',
                '>>>>>> Error: The Twitter icon is missing on the Dashboard Invite page.',
            ),
        ];
        let invitePageElements = await seleniumX.runAsync(invitePageElementsTasks);
        seleniumX.validateAllResultsSimple(invitePageElementsTasks, invitePageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-invite-page', 'content', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-invite-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Invite page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-invite-page', 'content', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Invite page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardInvitePage;

