const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardAccountSettings {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_ACCOUNT_SETTINGS,
            'dashboard account settings',
            true,
        );

        let password = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.css('[type="password"]'), 10000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-account-settings', 'change-password'], [seleniumX]);

        await password.instance.sendKeys('12ivan12');

        let submitButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="submit"]'));
        await seleniumX.click(submitButton);

        let successMessage = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.xpath("//div[contains(string(), 'Wir haben deine Daten gespeichert.')]"), 15000);

        if (!successMessage) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                '"change password" does not work properly on the Dashboard Account Settings page',
            );
        }

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-account-settings', 'change-password', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-account-settings', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Account Settings page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );
        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-account-settings', 'change-password'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the change password functionality on the Dashboard Account Settings page works.', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-account-settings', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the Dashboard Account Settings page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-account-settings', 'change-password', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully changed the user password from the Dashboard Account Settings page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardAccountSettings;

