const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');
const path = require('path');

class TestNoAuthenticationHomeSourceBasic {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.navigateSource(StaticClassSeleniumX.ClassSeleniumXRoutes.HOME);

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'test-no-authentication-home-source-basic', 'begin'], [seleniumX]);

        if (!seleniumX.validateHtmlSourceBasic(seleniumX.state.currentPageSource)) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'The current page source code does not contain proper html opening/closing tag(s).',
            );
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-home-source-basic', 'validate', 'html', 'source', 'basic', 'passed'], [seleniumX]);

        if (seleniumX.state.currentPageSource.length < 20) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'The current page source code is suspiciously short. Number of characters: ' + seleniumX.state.currentPageSource.length,
            );
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-home-source-basic', 'validate', 'html', 'source', 'length', 'passed'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-home-source-basic', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['pre', 'test', 'test-no-authentication-home-source-basic', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the home page (' + seleniumX.state.currentUrl + ') source code...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-home-source-basic', 'validate', 'html', 'source', 'basic', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('The current page contains an opening and closing html tag.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-home-source-basic', 'validate', 'html', 'source', 'length', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('The current page source code is ' + seleniumX.state.currentPageSource.length + ' characters long.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        return seleniumX;
    }
}

module.exports = TestNoAuthenticationHomeSourceBasic;
