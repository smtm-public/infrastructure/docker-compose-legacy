let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
let ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingStep1ContractStartImmediately {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP1,
            'onboarding step1',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1-contract-start-immediately', 'begin'], [seleniumX]);

        let onboardingStep1FormElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//span[contains(string(), 'Ab Sofort')]/../span[contains(@class,'radio')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No start contract immediately radio button found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.name('selection2'),
                    null,
                ],
                null,
                '>>>>>> Error: No user record verification hidden checkbox found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('input[name="selection2"]+span.checkbox'),
                    null,
                ],
                null,
                '>>>>>> Error: No user record verification styled checkbox span found on the onboarding step1 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//button[contains(string(), 'WEITER')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No submit button found on the onboarding step1 page.',
            ),
        ];
        let onboardingStep1FormElements = await seleniumX.runAsync(
            onboardingStep1FormElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingStep1FormElementsTasks,
            onboardingStep1FormElementsTasks
        );

        seleniumX.eventEmitter.emitEvents(['test-onboarding-step1-contract-start-immediately', 'action', 'click', 'radio', 'button', 'start', 'contract', 'immediately', 'onboarding step1', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep1FormElements[0]);
        if(!await seleniumX.getStyledCheckboxCheckedState(onboardingStep1FormElements[2])) {
            seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1-contract-start-immediately', 'action', 'click', 'check', 'styled', 'checkbox', 'user', 'record', 'verification', 'onboarding step1', 'page'], [seleniumX]);
            await seleniumX.click(onboardingStep1FormElements[2]);
        }
        seleniumX.eventEmitter.emitEvents(['test-onboarding-step1-contract-start-immediately', 'action', 'click', 'submit', 'button', 'onboarding step1', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep1FormElements[3]);
        await seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP2), 2000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step1-contract-start-immediately', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1-contract-start-immediately', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding step1 page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-step1-contract-start-immediately', 'action', 'click', 'radio', 'button', 'start', 'contract', 'immediately', 'onboarding step1', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the start contract immediately radio button on the onboarding step1 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1-contract-start-immediately', 'action', 'click', 'check', 'styled', 'checkbox', 'user', 'record', 'verification', 'onboarding step1', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the user record verification checkbox in order to check it on the onboarding step1 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-step1-contract-start-immediately', 'action', 'click', 'submit', 'button', 'onboarding step1', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked the submit button on the onboarding step1 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step1-contract-start-immediately', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding step1 page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingStep1ContractStartImmediately;