const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXEventEmitter = require('../../lib/SeleniumXEventEmitter');
const ClassSeleniumXException = require('../../lib/SeleniumXException');
const path = require('path');

let seleniumX;

class SuiteOnboarding {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        if(process.env.LOGIN_USER_EMAIL) {
            seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'before', 'test-no-authentication-login'], [seleniumX, process.env.LOGIN_USER_EMAIL]);
            let TestNoAuthenticationLogin = require('./test-no-authentication-login');
            seleniumX = await TestNoAuthenticationLogin.boot(seleniumX);
            seleniumX = await TestNoAuthenticationLogin.main(seleniumX);
            seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-no-authentication-login'], [seleniumX]);
            seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
        } else {
            if(process.env.NEW_USER_EMAIL) {
                seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'before', 'test-no-authentication-registration'], [seleniumX, process.env.NEW_USER_EMAIL]);
                let TestNoAuthenticationRegistration = require('./test-no-authentication-registration');
                seleniumX = await TestNoAuthenticationRegistration.boot(seleniumX);
                seleniumX = await TestNoAuthenticationRegistration.main(seleniumX);
                seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-no-authentication-registration'], [seleniumX]);
                seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
            } else {
                seleniumX.exceptionHandler.throwException(
                    ClassSeleniumXException.ClassSeverity.FATAL,
                    ClassSeleniumXException.ClassType.ENGINE,
                    'No login or signup user emails provided. The onboarding suite cannot continue. Aborting.',
                );
            }
        }

        let TestOnboardingProductSelectPage = require('./test-onboarding-product-select-page');
        seleniumX = await TestOnboardingProductSelectPage.boot(seleniumX);
        seleniumX = await TestOnboardingProductSelectPage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-product-select-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingProductSelect = require('./test-onboarding-product-select');
        seleniumX = await TestOnboardingProductSelect.boot(seleniumX);
        seleniumX = await TestOnboardingProductSelect.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-product-select'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let onboardingProductSelectPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-pair'),
                ],
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-fill8'),
                ],
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('i.icon-pferde'),
                ],
            ),
        ];
        let onboardingProductSelectPageElements = await seleniumX.runAsync(
            onboardingProductSelectPageElementsTasks
        );
        if(onboardingProductSelectPageElements[0]) {
            let TestOnboardingProductSelectPersonalLiability = require('./test-onboarding-product-select-personal-liability');
            seleniumX = await TestOnboardingProductSelectPersonalLiability.boot(seleniumX);
            seleniumX = await TestOnboardingProductSelectPersonalLiability.main(seleniumX);
            seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-product-select-personal-liability'], [seleniumX]);
            seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
        } else {
            if(false && onboardingProductSelectPageElements[1]) {
                let TestOnboardingProductSelectDogLiability = require('./test-onboarding-product-select-dog-liability');
                seleniumX = await TestOnboardingProductSelectDogLiability.boot(seleniumX);
                seleniumX = await TestOnboardingProductSelectDogLiability.main(seleniumX);
                seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-product-select-dog-liability'], [seleniumX]);
                seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
            } else {
                if(onboardingProductSelectPageElements[2]) {
                    let TestOnboardingProductSelectHorseLiability = require('./test-onboarding-product-select-horse-liability');
                    seleniumX = await TestOnboardingProductSelectHorseLiability.boot(seleniumX);
                    seleniumX = await TestOnboardingProductSelectHorseLiability.main(seleniumX);
                    seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-product-select-horse-liability'], [seleniumX]);
                    seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

                    let TestOnboardingProductSpecificPage = require('./test-onboarding-product-specific-page');
                    seleniumX = await TestOnboardingProductSpecificPage.boot(seleniumX);
                    seleniumX = await TestOnboardingProductSpecificPage.main(seleniumX);
                    seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-product-specific-page'], [seleniumX]);
                    seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

                    let TestOnboardingProductSpecific = require('./test-onboarding-product-specific');
                    seleniumX = await TestOnboardingProductSpecific.boot(seleniumX);
                    seleniumX = await TestOnboardingProductSpecific.main(seleniumX);
                    seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-product-specific'], [seleniumX]);
                    seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);
                }
            }
        }

        let TestOnboardingStep1Page = require('./test-onboarding-step1-page');
        seleniumX = await TestOnboardingStep1Page.boot(seleniumX);
        seleniumX = await TestOnboardingStep1Page.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step1-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep1 = require('./test-onboarding-step1');
        seleniumX = await TestOnboardingStep1.boot(seleniumX);
        seleniumX = await TestOnboardingStep1.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step1'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep1ContractStartImmediately = require('./test-onboarding-step1-contract-start-immediately');
        seleniumX = await TestOnboardingStep1ContractStartImmediately.boot(seleniumX);
        seleniumX = await TestOnboardingStep1ContractStartImmediately.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step1-contract-start-immediately'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep2Page = require('./test-onboarding-step2-page');
        seleniumX = await TestOnboardingStep2Page.boot(seleniumX);
        seleniumX = await TestOnboardingStep2Page.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step2-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep2 = require('./test-onboarding-step2');
        seleniumX = await TestOnboardingStep2.boot(seleniumX);
        seleniumX = await TestOnboardingStep2.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step2'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep3Page = require('./test-onboarding-step3-page');
        seleniumX = await TestOnboardingStep3Page.boot(seleniumX);
        seleniumX = await TestOnboardingStep3Page.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step3-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep3 = require('./test-onboarding-step3');
        seleniumX = await TestOnboardingStep3.boot(seleniumX);
        seleniumX = await TestOnboardingStep3.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step3'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep4Page = require('./test-onboarding-step4-page');
        seleniumX = await TestOnboardingStep4Page.boot(seleniumX);
        seleniumX = await TestOnboardingStep4Page.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step4-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep4 = require('./test-onboarding-step4');
        seleniumX = await TestOnboardingStep4.boot(seleniumX);
        seleniumX = await TestOnboardingStep4.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step4'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep4PaymentMethodSepa = require('./test-onboarding-step4-payment-method-sepa');
        seleniumX = await TestOnboardingStep4PaymentMethodSepa.boot(seleniumX);
        seleniumX = await TestOnboardingStep4PaymentMethodSepa.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step4-payment-method-sepa'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep5Page = require('./test-onboarding-step5-page');
        seleniumX = await TestOnboardingStep5Page.boot(seleniumX);
        seleniumX = await TestOnboardingStep5Page.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step5-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingStep5 = require('./test-onboarding-step5');
        seleniumX = await TestOnboardingStep5.boot(seleniumX);
        seleniumX = await TestOnboardingStep5.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-step5'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingSignaturePage = require('./test-onboarding-signature-page');
        seleniumX = await TestOnboardingSignaturePage.boot(seleniumX);
        seleniumX = await TestOnboardingSignaturePage.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-signature-page'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        let TestOnboardingSignature = require('./test-onboarding-signature');
        seleniumX = await TestOnboardingSignature.boot(seleniumX);
        seleniumX = await TestOnboardingSignature.main(seleniumX);
        seleniumX.eventEmitter.emitEvents(['test', 'suite-onboarding', 'main', 'after', 'test-onboarding-signature'], [seleniumX]);
        seleniumX.eventEmitter.emitEvents(['after', 'test', 'main', 'filename'], [seleniumX, __filename]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'main', 'filename'],
            (seleniumX, filename) => {
                seleniumX.logger.log('');
                seleniumX.logger.log(filename, ClassSeleniumXLogger.ClassLogType.ENVIRONMENT);
            },
            ClassSeleniumXEventEmitter.ClassCondition.SUBSET,
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'suite-onboarding', 'main', 'before', 'test-login'],
            (seleniumX, loginUserEmail) => {
                seleniumX.logger.log('Login user email provided:' + loginUserEmail, ClassSeleniumXLogger.ClassLogType.INFO);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'suite-onboarding', 'main', 'before', 'test-registration'],
            (seleniumX, newUserEmail) => {
                seleniumX.logger.log('No login user email provided. Registering with user:' + newUserEmail, ClassSeleniumXLogger.ClassLogType.INFO);
            },
        );

        return seleniumX;
    }
}

(async () => {
    seleniumX =
        seleniumX ||
        await new ClassSeleniumX();
    seleniumX = SuiteOnboarding.boot(seleniumX)
        .then(
            (seleniumX) => {
                return SuiteOnboarding.main(seleniumX)
                    .then(
                        (seleniumX) => {
                            !seleniumX || seleniumX.shutdownWithSuccess();
                        }
                    )
                    .catch(
                        async (error) => {
                            console.log(error);
                            !seleniumX || seleniumX.shutdownWithError(error);
                        }
                    );
            }
        );
})();