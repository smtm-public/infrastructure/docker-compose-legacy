let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
let ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingStep2 {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP2,
            'onboarding step2',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step2', 'begin'], [seleniumX]);

        let formElementsValues = [
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Vorname')]/.."), 'auto', 'first name', StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir brauchen deinen Vornamen')]")],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Nachname')]/.."), 'test', 'last name', StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir brauchen deinen Nachnamen')]")],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Straße')]/.."), 'Somestreet blvd.', 'street', StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir brauchen deine Straße.')]")],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Hausnummer')]/.."), '34', 'address number', StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir brauchen deine Hausnummer.')]")],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Postleitzahl')]/.."), '10000', 'postal code', StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir brauchen deine PLZ.')]")],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Ort')]/.."), 'Hamburg', 'city', StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir brauchen deine Stadt.')]")],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Mobilnummer')]/.."), '+49171123456', 'phone number', StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir brauchen deine Mobilfunknummer.')]")],
        ];

        let onboardingStep2PageFormElementsTasks = [];
        for(let i in formElementsValues) {
            if(formElementsValues[i]) {
                onboardingStep2PageFormElementsTasks.push(
                    new ClassSeleniumXTask(
                        seleniumX.findChildOfElementAsync,
                        seleniumX,
                        [
                            await seleniumX.findElementAsync(formElementsValues[i][0]),
                            StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                        ],
                        null,
                        '>>>>>> Error: The ' + formElementsValues[i][2] + ' text input is missing on the onboarding step2 page.'
                    )
                );
            }
        }
        let onboardingStep2PageFormElements = await seleniumX.runAsync(
            onboardingStep2PageFormElementsTasks
        );
        seleniumX.validateAllResultsSimple(onboardingStep2PageFormElementsTasks, onboardingStep2PageFormElements);

        let submitButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="submit"]'));

        seleniumX.eventEmitter.emitEvents(['test-onboarding-step2', 'check', 'empty', 'values', 'validation'], [seleniumX]);
        let emptyValuesFormTest =
            await seleniumX.formTest(
                formElementsValues.map(
                    (element, index) => {
                        return [onboardingStep2PageFormElements[index], StaticClassSeleniumX.Selenium.Key.chord(StaticClassSeleniumX.Selenium.Key.CONTROL, 'a'), StaticClassSeleniumX.Selenium.Key.DELETE];
                    }
                ),
                submitButton,
                () => {
                    return (async () => {
                        let emptyValidationErrorElementsTasks = [];
                        for(let i in formElementsValues) {
                            if(formElementsValues[i]) {
                                emptyValidationErrorElementsTasks.push(
                                    new ClassSeleniumXTask(
                                        seleniumX.findElementsAsync,
                                        seleniumX,
                                        [
                                            formElementsValues[i][3],
                                            'Check: Looking for empty ' + formElementsValues[i][2] + ' validation error message HTML elements on the onboarding step2 page...',
                                        ],
                                        'Empty value for ' + formElementsValues[i][2] + ' validation is working on the onboarding step2 page.',
                                        '>>>>>> Error: Empty value validation for ' + formElementsValues[i][2] + ' failed on the onboarding step2 page.',
                                    )
                                );
                            }
                        }
                        let emptyValidationErrorElements = await seleniumX.runAsync(
                            emptyValidationErrorElementsTasks
                        );
                        seleniumX.validateAllResults(
                            emptyValidationErrorElementsTasks,
                            emptyValidationErrorElements,
                            (result) => {
                                return (typeof result === 'object' && result.length === 1);
                            }
                        );
                    })()
                }
            );

        seleniumX.eventEmitter.emitEvents(['test-onboarding-step2', 'check', 'empty', 'values', 'validation'], [seleniumX]);
        let validFormTest =
            await seleniumX.formTest(
                formElementsValues.map(
                    (element, index) => {
                        return [onboardingStep2PageFormElements[index], element[1]];
                    }
                ),
                submitButton,
                () => {
                    return (async () => {
                        let emptyValidationErrorElementsTasks = [];
                        for(let i in formElementsValues) {
                            if(formElementsValues[i]) {
                                emptyValidationErrorElementsTasks.push(
                                    new ClassSeleniumXTask(
                                        seleniumX.findElementsAsync,
                                        seleniumX,
                                        [
                                            formElementsValues[i][3],
                                        ],
                                        null,
                                        null,
                                    )
                                );
                            }
                        }
                        let emptyValidationErrorElements = await seleniumX.runAsync(
                            emptyValidationErrorElementsTasks
                        );

                        seleniumX.validateAllResults(
                            emptyValidationErrorElementsTasks,
                            emptyValidationErrorElements,
                            (result) => {
                                return (typeof result !== 'object' || result.length === 0);
                            }
                        );
                    })()
                }
            );

        await seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP3), 2000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step2', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step2', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding step2 page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step2', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding step2 page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingStep2;