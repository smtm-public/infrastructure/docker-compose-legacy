const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');

class TestOnboardingProductSpecificPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SPECIFIC,
            'onboarding product specific',
            true,
        );

        await seleniumX.doBasicTests('onboarding product specific');

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific-page', 'begin'], [seleniumX]);

        let onboardingProductSpecificPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//h2[contains(string(), 'Wer soll versichert werden?')]"),
                    'Check: Looking for the heading on the onboarding product specific page...',
                ],
                'Found the heading on the onboarding product specific page.',
                '>>>>>> Error: The heading is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Wie heißt dein Pferd?')]"),
                    'Check: Looking for the horse name text field label on the onboarding product specific page...',
                ],
                'Found the horse name text field label on the onboarding product specific page.',
                '>>>>>> Error: The horse name text field label is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Wie heißt dein Pferd?')]/../input[contains(@type, 'text')]"),
                    'Check: Looking for the horse name text field on the onboarding product specific page...',
                ],
                'Found the horse name text field on the onboarding product specific page.',
                '>>>>>> Error: The horse name text field is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('Weiteres Pferd hinzufügen'),
                    'Check: Looking for the add another horse button on the onboarding product specific page...',
                ],
                'Found the add another horse button on the onboarding product specific page.',
                '>>>>>> Error: The add another horse button is missing from the onboarding product specific page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.linkText('WEITER'),
                    'Check: Looking for submit buttons on the onboarding product specific page...',
                ],
                'Found submit buttons on the onboarding product specific page.',
                '>>>>>> Error: No submit buttons found on the onboarding product specific page.',
            ),
        ];
        let onboardingProductSpecificPageElements = await seleniumX.runAsync(
            onboardingProductSpecificPageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingProductSpecificPageElementsTasks,
            onboardingProductSpecificPageElements
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-product-specific-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the onboarding product specific page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-product-specific-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the onboarding product specific page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestOnboardingProductSpecificPage;