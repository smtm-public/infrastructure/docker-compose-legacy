const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const path = require('path');
const Until = require('selenium-webdriver/lib/until');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardInvite {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_INVITE,
            'dashboard invite',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-invite', 'content', 'begin'], [seleniumX]);

        let submitButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="submit"]'));
        let textareaElement = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('textarea'));

        await textareaElement.instance.sendKeys('test_email0@a.a');
        await  seleniumX.click(submitButton);

        await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.xpath("//div[contains(string(), 'Wir haben deine Freunde eingeladen.')]"),10000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-dashboard-invite', 'content', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-invite', 'content', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Dashboard Invite page functionality is working.', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-dashboard-invite', 'content', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully passed the Dashboard Invite functionality test.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestDashboardInvite;

