let StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
let ClassSeleniumX = require('../../lib/SeleniumX');
let ClassSeleniumXTask = require('../../lib/SeleniumXTask');
let ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
let ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingStep5 {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_STEP5,
            'onboarding step5',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5', 'begin'], [seleniumX]);

        let onboardingStep5FormElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.name('selection2'),
                    null,
                ],
                null,
                '>>>>>> Error: No user record verification hidden checkbox found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('input[name="selection2"]+span.checkbox'),
                    null,
                ],
                null,
                '>>>>>> Error: No user record verification styled checkbox span found on the onboarding step5 page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.xpath("//button[contains(string(), 'ZUR UNTERSCHRIFT')]"),
                    null,
                ],
                null,
                '>>>>>> Error: No submit button found on the onboarding step5 page.',
            ),
        ];
        let onboardingStep5FormElements = await seleniumX.runAsync(onboardingStep5FormElementsTasks);
        seleniumX.validateAllResultsSimple(
            onboardingStep5FormElementsTasks,
            onboardingStep5FormElements
        );
        if(await seleniumX.getStyledCheckboxCheckedState(onboardingStep5FormElements[1][0])) {
            seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5', 'action', 'click', 'uncheck', 'styled', 'checkbox', 'agree', 'terms', 'conditions', 'privacy policy', 'commission', 'onboarding step5', 'page'], [seleniumX]);
            await seleniumX.click(onboardingStep5FormElements[1][0]);
        }
        if(await seleniumX.getStyledCheckboxCheckedState(onboardingStep5FormElements[1][1])) {
            seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5', 'action', 'click', 'uncheck', 'styled', 'checkbox', 'familiar', 'liability', 'concept', 'onboarding step5', 'page'], [seleniumX]);
            await seleniumX.click(onboardingStep5FormElements[1][1]);
        }
        let notAgreedValidationTest = await seleniumX.formTest(
            [],
            onboardingStep5FormElements[2],
            () => {
                seleniumX.eventEmitter.emitEvents(['test-onboarding-step5', 'action', 'click', 'submit', 'button', 'not agreed', 'onboarding step5', 'page'], [seleniumX]);
                return seleniumX.driver.wait(
                    new StaticClassSeleniumX.WebDriver.Condition(
                        'to see if the onboarding step5 form validation works with unchecked agreement styled checkboxes',
                        async() => {
                            let existsElementAgreeTermsConditionsPrivacyPolicyCommission = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath(
                                    "//span[contains(string(), 'Bitte bestätige die Beauftragung für uns.')]",
                                ),
                            );
                            let existsElementFamiliarLiabilityConcept = await seleniumX.existsElementAsync(
                                StaticClassSeleniumX.Selenium.By.xpath(
                                    "//span[contains(string(), 'Bitte bestätige, dass du dich mit der Versicherung beschäftigt hast.')]",
                                ),
                            );
                            return existsElementAgreeTermsConditionsPrivacyPolicyCommission && existsElementFamiliarLiabilityConcept;
                        }
                    ),
                    20000
                );
            }
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5', 'action', 'click', 'check', 'styled', 'checkbox', 'agree', 'terms', 'conditions', 'privacy policy', 'commission', 'onboarding step5', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep5FormElements[1][0]);
        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5', 'action', 'click', 'check', 'styled', 'checkbox', 'familiar', 'liability', 'concept', 'onboarding step5', 'page'], [seleniumX]);
        await seleniumX.click(onboardingStep5FormElements[1][1]);
        let validFormTest = await seleniumX.formTest(
            [],
            onboardingStep5FormElements[2],
            () => {
                seleniumX.eventEmitter.emitEvents(['test-onboarding-step5', 'action', 'click', 'submit', 'button', 'not agreed', 'onboarding step5', 'page'], [seleniumX]);
                return seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_SIGNATURE, 2000)
                    .then(
                        (result) => result
                    );
            }
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-step5', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding step5 page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5', 'action', 'click', 'uncheck', 'styled', 'checkbox', 'agree', 'terms', 'conditions', 'privacy policy', 'commission', 'onboarding step5', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('The agree to terms, conditions, privacy policy and commission styled checkbox appears checked on the onboarding step5 page.', ClassSeleniumXLogger.ClassLogType.INFO);
                seleniumX.logger.log('Clicked on the agree to terms, conditions, privacy policy and commission styled checkbox on the onboarding step5 page to uncheck it in order to test the form validation.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5', 'action', 'click', 'uncheck', 'styled', 'checkbox', 'familiar', 'liability', 'concept', 'onboarding step5', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('The familiar with liability concept styled checkbox appears checked on the onboarding step5 page.', ClassSeleniumXLogger.ClassLogType.INFO);
                seleniumX.logger.log('Clicked on the familiar with liability concept styled checkbox on the onboarding step5 page to uncheck it in order to test the form validation.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-onboarding-step5', 'action', 'click', 'submit', 'button', 'not agreed', 'onboarding step5', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked on the submit button with unchecked agreement checkboxes in order to test the form validation on the onboarding step5 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5', 'action', 'click', 'check', 'styled', 'checkbox', 'agree', 'terms', 'conditions', 'privacy policy', 'commission', 'onboarding step5', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked on the agree to terms, conditions, privacy policy and commission styled checkbox on the onboarding step5 page to check it.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5', 'action', 'click', 'check', 'styled', 'checkbox', 'familiar', 'liability', 'concept', 'onboarding step5', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked on the familiar with liability concept styled checkbox on the onboarding step5 page to check it.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-step5', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding step5 page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingStep5;