const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

const path = require('path');

class TestOnboardingSignature {
    static async main(seleniumX) {
        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_SIGNATURE,
            'onboarding signature',
            true,
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-signature', 'begin'], [seleniumX]);

        let onboardingSignaturePageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('div.box-signature>canvas'),
                    null,
                ],
                null,
                '>>>>>> Error: No signature canvas found on the onboarding signature page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('button[type="submit"]'),
                    null,
                ],
                null,
                '>>>>>> Error: No submit button found on the onboarding signature page.',
            ),
        ];
        let onboardingSignaturePageElements = await seleniumX.runAsync(
            onboardingSignaturePageElementsTasks
        );
        seleniumX.validateAllResultsSimple(
            onboardingSignaturePageElementsTasks,
            onboardingSignaturePageElements
        );

        // more on bridge mode here: https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/input_exports_Actions.html
        let actions = await seleniumX.driver.actions({bridge: true,});
        await seleniumX.driver.executeScript('window.mouseposx = 0; window.mouseposy = 0; $(document).mousemove((event) => {window.mouseposx = event.pageX; window.mouseposy = event.pageY;});');
        await actions
            .move({x: 0, y: 0, duration: 0, origin: onboardingSignaturePageElements[0].instance})
            .perform();
        let mousePos = await seleniumX.driver.executeScript('return {x: window.mouseposx, y: window.mouseposy};');
        console.log('mouse.x: ' + mousePos.x + ', mouse.y: ' + mousePos.y);
        await actions
            .pause(300)
            .press()
            .move({x: 1, y: 2, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 0, y: 3, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -1, y: 2, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -2, y: -1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -1, y: 0, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -2, y: 0, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -1, y: 1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 0, y: 2, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 1, y: 1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 2, y: 0, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 2, y: 0, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 3, y: -1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 3, y: -1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 2, y: -1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 1, y: -1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 1, y: 0, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 1, y: 0, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 1, y: 0, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: 0, y: 1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -1, y: 1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -1, y: 1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -1, y: 1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(300)
            .move({x: -1, y: 1, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
            .pause(3000)
            .release()
            .perform();
        /*
        await actions.press();
        let a = 20;
        let b = 1;
        let flick = (async() => {
            await actions
                .move({x: Math.round(a /= 3), y: b, duration: 0, origin: StaticClassSeleniumX.Input.Origin.POINTER})
                .perform();
            mousePos = await seleniumX.driver.executeScript('return {x: window.mouseposx, y: window.mouseposy};');
            console.log('mouse.x: ' + mousePos.x + ', mouse.y: ' + mousePos.y);
            return flick();
        });
        await flick();

        for(let he = 0; he < 30; he++) {
            for (let j = -1; j < 2; j++) {
                for (let i = -1; i < 1; i++) {
                    mousePos = await seleniumX.driver.executeScript('return {x: window.mouseposx, y: window.mouseposy};');
                    console.log('mouse.x: ' + mousePos.x + ', mouse.y: ' + mousePos.y);
                }
            }
        }
        await actions
            .release()
            .perform();
            */

        let validFormTest = await seleniumX.formTest(
            [],
            onboardingSignaturePageElements[1],
            () => {
                seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-signature', 'action', 'click', 'submit', 'button', 'valid', 'onboarding step5', 'page'], [seleniumX]);
                return seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_WELCOME, 40000);
            }
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-onboarding-signature', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-signature', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing the onboarding signature page (' + seleniumX.state.currentUrl + ') functionality...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-signature', 'action', 'click', 'submit', 'button', 'valid', 'onboarding step5', 'page'],
            (seleniumX) => {
                seleniumX.logger.log('Clicked on the submit button after drawing a signature on the canvas on the onboarding step5 page.', ClassSeleniumXLogger.ClassLogType.ACTION);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-onboarding-signature', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The onboarding signature page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}


module.exports = TestOnboardingSignature;