const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const path = require('path');
const ClassSeleniumXException = require('../../lib/SeleniumXException');

class TestDashboardSettings {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main'], [seleniumX, __filename]);

        await seleniumX.navigate(StaticClassSeleniumX.ClassSeleniumXRoutes.CONTRACT_SELECT);

        let privateIcon = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.xpath('//form/div/div/div[1]/ul/li[1]/a/i'));

        await seleniumX.click(privateIcon);

        let settingsButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('ANZEIGEN'));
        await seleniumX.click(settingsButton);

        await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.css('[type="text"]'), 10000);

        //Settings tab
        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-address-functionality', 'begin'], [seleniumX]);

        let registerPartner = await seleniumX.existsElementAByLinkText('Partner/in eintragen');
        if (registerPartner) {
            seleniumX.click(registerPartner);
        }

        let editableFormElementsLabels = [
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Nachname')]/.."), 'Nachname'],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Adresse')]/.."), 'Adresse'],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'PLZ')]/.."), 'PLZ'],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Ort')]/.."), 'Ort'],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Mobilnummer')]/.."), 'Mobilnummer'],
            [StaticClassSeleniumX.Selenium.By.xpath("//label[contains(string(), 'Partner/in')]/.."), 'Partner/in'],
        ];

        let settingsAdrressEditableFormElementsTasks = [];
        for (let i in editableFormElementsLabels) {
            if (editableFormElementsLabels[i]) {
                settingsAdrressEditableFormElementsTasks.push(
                    new ClassSeleniumXTask(
                        seleniumX.findChildOfElementAsync,
                        seleniumX,
                        [
                            await seleniumX.findElementAsync(editableFormElementsLabels[i][0]),
                            StaticClassSeleniumX.Selenium.By.css('input[type="text"]'),
                        ],
                        null,
                        '>>>>>> Error: The ' + editableFormElementsLabels[i][1] + ' text input is missing on the settings address page.'
                    )
                );
            }
        }

        let settingsAdrressEditableFormElements = await seleniumX.runAsync(settingsAdrressEditableFormElementsTasks);
        seleniumX.validateAllResultsSimple(settingsAdrressEditableFormElementsTasks, settingsAdrressEditableFormElements);

        let randUsername = Math.random().toString(36).substring(7);
        let params = [
            randUsername,
            'adresss 123',
            '12345',
            'Sofia',
            '+49171121111',
            'Ivan Ivanov'
        ];
        for (let n in settingsAdrressEditableFormElements) {
            await settingsAdrressEditableFormElements[n].instance.sendKeys(StaticClassSeleniumX.Selenium.Key.CONTROL, 'a', StaticClassSeleniumX.Selenium.Key.NULL, StaticClassSeleniumX.Selenium.Key.DELETE, params[n]);
        }

        let submitButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="submit"]'));
        await seleniumX.click(submitButton);

        let successMessage = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.xpath("//div[contains(string(), 'Wir haben deine Daten gespeichert.')]"), 20000);
        if (!successMessage) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'Account-settings "change password" doesn\'t work properly',
            );
        }

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-address-functionality', 'success'], [seleniumX]);

        //Payment Tab
        let paymentTab = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Zahlungsart'));
        await seleniumX.click(paymentTab);

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-payment-functionality', 'begin'], [seleniumX]);

        let changePaymnetLink = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Zahlmethode ändern'));
        await seleniumX.click(changePaymnetLink);

        await seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_CHANGE_PAYMENT), 10000);

        await seleniumX.navigate(StaticClassSeleniumX.ClassSeleniumXRoutes.CONTRACT_SELECT);

        let privateIconn = await seleniumX.waitUntilElementLocated(StaticClassSeleniumX.Selenium.By.xpath('//form/div/div/div[1]/ul/li[1]/a/i'), 20000);
        await seleniumX.click(privateIconn);

        let settingsButtonn = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('ANZEIGEN'));

        await seleniumX.click(settingsButtonn);

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-address-functionality', 'success'], [seleniumX]);

        //Contract Tab
        let contractTab = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Vertrag'));
        await seleniumX.click(contractTab);

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-contract-functionality', 'begin'], [seleniumX]);

        let viewConditionerLink = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('Bedingungswerk anschauen'));
        await seleniumX.click(viewConditionerLink);

        let browserTabs = await seleniumX.driver.getAllWindowHandles();

        await seleniumX.driver.switchTo().window(browserTabs[1]);

        if (await seleniumX.getCurrentUrl() !== 'https://haftpflichthelden.de/dokumente/bedingungswerk') {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'In Settings contract page link "View the conditioner" doesn\'t work',
            );
        }
        await seleniumX.driver.switchTo().window(browserTabs[0]);

        let terminateContractLink = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.linkText('VERTRAG KÜNDIGEN'));
        await seleniumX.click(terminateContractLink);

        await seleniumX.driver.wait(StaticClassSeleniumX.Until.urlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.DASHBOARD_CONFIRM_CANCEL), 10000);

        seleniumX.eventEmitter.emitEvents(['test', 'test-settings-contract-functionality', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-settings-address-functionality', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Settings page (Address tab) functionality is working properly...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-settings-address-functionality', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated that the functionality on the Settings page( Contract tab) is working properly.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-settings-payment-functionality', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Settings page (Payment tab) functionality is working properly...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-settings-payment-functionality', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated that the functionality on the Settings page(Payment tab) is working properly.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );
        seleniumX.eventEmitter.onEvents(
            ['test', 'test-settings-contract-functionality', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the Settings page (Contract tab) functionality is working properly...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-settings-contract-functionality', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated that the functionality on the Settings page(Contract tab) is working properly.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }

    // to be removed after Milens Login implementation
    static async login(seleniumX, email, passwd) {

        seleniumX = seleniumX || await new ClassSeleniumX();

        let emailField = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="email"]'));
        let passwordField = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="password"]'));
        let loginButton = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.id('login-button'));

        await emailField.instance.sendKeys(email);
        await passwordField.instance.sendKeys(passwd);
        await seleniumX.click(loginButton);
    }
}

module.exports = TestDashboardSettings;

