const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXTask = require('../../lib/SeleniumXTask');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');

const path = require('path');

class TestNoAuthenticationForgotPasswordPage {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.FORGOT,
            'forgot password',
        );

        await seleniumX.doBasicTests('forgot password');

        seleniumX.eventEmitter.emitEvents(['test', 'test-forgot-password-page', 'begin'], [seleniumX]);

        let forgotPasswordPageElementsTasks = [
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="email"]'),
                    'Check: Looking for email input form fields on the forgot password page...',
                ],
                'Found email input form fields on the forgot password page.',
                '>>>>>> Error: No email input form fields found on the forgot password page.',
            ),
            new ClassSeleniumXTask(
                seleniumX.findElementsAsync,
                seleniumX,
                [
                    StaticClassSeleniumX.Selenium.By.css('[type="submit"]'),
                    'Check: Looking for submit buttons on the forgot password page...',
                ],
                'Found submit buttons on the forgot password page.',
                '>>>>>> Error: No submit buttons found on the forgot password page.',
            ),
        ];
        let forgotPasswordPageElements = await seleniumX.runAsync(forgotPasswordPageElementsTasks);
        seleniumX.validateAllResultsSimple(forgotPasswordPageElementsTasks, forgotPasswordPageElements);

        seleniumX.eventEmitter.emitEvents(['test', 'test-forgot-password-page', 'success'], [seleniumX]);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-forgot-password-page', 'begin'],
            (seleniumX) => {
                seleniumX.logger.log('Testing whether the forgot password page (' + seleniumX.state.currentUrl + ') contains certain HTML elements...', ClassSeleniumXLogger.ClassLogType.TEST);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-forgot-password-page', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Successfully enumerated the essential elements on the forgot password page.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestNoAuthenticationForgotPasswordPage;