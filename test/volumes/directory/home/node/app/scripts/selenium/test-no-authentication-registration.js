const StaticClassSeleniumX = require('../../lib/StaticSeleniumX');
const ClassSeleniumX = require('../../lib/SeleniumX');
const ClassSeleniumXLogger = require('../../lib/SeleniumXLogger');
const ClassSeleniumXException = require('../../lib/SeleniumXException');
const path = require('path');

class TestNoAuthenticationRegistration {
    static async main(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.emitEvents(['pre', 'test', 'main', 'filename'], [seleniumX, __filename]);

        await seleniumX.loadRoute(
            StaticClassSeleniumX.ClassSeleniumXRoutes.SIGNUP,
            'registration',
        );

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'begin'], [seleniumX]);

        let submitButton = await seleniumX.findElementsAsync(StaticClassSeleniumX.Selenium.By.css('[type="submit"]'));

        let email = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="email"]'));
        let password = await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.css('[type="password"]'));

        seleniumX.eventEmitter.emitEvents(['test-no-authentication-registration', 'check', 'empty', 'values', 'validation'], [seleniumX]);
        let emptyValuesFormTest =
            await seleniumX.formTest(
                [
                    [email, StaticClassSeleniumX.Selenium.Key.chord(StaticClassSeleniumX.Selenium.Key.CONTROL, 'a'), StaticClassSeleniumX.Selenium.Key.DELETE],
                ],
                submitButton[0],
                () => {
                    return (async () => {
                        let emptyEmailValidationErrorElements = await seleniumX.findElementsAsync(
                            StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen eine E-Mail Adresse von Dir.')]"),
                            'Check: Looking for empty email validation error message HTML elements...',
                        );
                        let emptyPasswordValidationErrorElements = await seleniumX.findElementsAsync(
                            StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen ein Passwort um dich anzumelden.')]"),
                            'Check: Looking for empty password validation error message HTML elements...',
                        );

                        return (emptyEmailValidationErrorElements.length && emptyPasswordValidationErrorElements.length);
                    })()
                        .then(
                            (outcome) => {
                                if (outcome) {
                                    seleniumX.eventEmitter.emitEvents(['test-no-authentication-registration', 'check', 'empty', 'values', 'validation', 'passed'], [seleniumX]);

                                    return true;
                                } else {
                                    seleniumX.exceptionHandler.throwException(
                                        ClassSeleniumXException.ClassSeverity.FATAL,
                                        ClassSeleniumXException.ClassType.TEST,
                                        'Validation is not functioning properly.',
                                    );
                                }
                            }
                        );
                }
            );

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'before', 'email', 'field', 'validation'], [seleniumX]);
        let emailTestCases = [
            'test',
            'test@asas',
            'tesqw    et@abv.БГ',
        ];
        let emailTestCasesResults = [];
        while (emailTestCases && emailTestCases.length) {
            let text = emailTestCases.shift();
            emailTestCasesResults.push(
                await seleniumX.formTest(
                    [[email, text]],
                    submitButton[0],
                    () => {
                        return seleniumX.findElementsAsync(
                            StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Deine E-Mail Adresse sieht nicht richtig aus.')]"),
                            'Check: Testing the email field with value: ' + text + '. Looking for email validation error message HTML elements...',
                        )
                            .then(
                                (fieldErrorText) => {
                                    if (fieldErrorText.length) {
                                        seleniumX.eventEmitter.emitEvents(['test-no-authentication-registration', 'check', 'email', 'field', 'validation', 'passed'], [seleniumX, text]);

                                        return seleniumX.sendKeys(email, StaticClassSeleniumX.Selenium.Key.chord(StaticClassSeleniumX.Selenium.Key.CONTROL, 'a'), StaticClassSeleniumX.Selenium.Key.DELETE)
                                            .then(
                                                () => {
                                                    return true;
                                                }
                                            )
                                    } else {
                                        seleniumX.exceptionHandler.throwException(
                                            ClassSeleniumXException.ClassSeverity.FATAL,
                                            ClassSeleniumXException.ClassType.TEST,
                                            'Validation is not functioning properly.',
                                        );
                                    }
                                }
                            )
                    }
                )
            )
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'after', 'email', 'field', 'validation', 'success'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'before', 'password', 'field', 'validation'], [seleniumX]);
        let passwordTestCases = [
            '123',
            'тест',
        ];
        let passwordTestCasesResults = [];
        while (passwordTestCases && passwordTestCases.length) {
            let text = passwordTestCases.shift();
            passwordTestCasesResults.push(
                await seleniumX.formTest(
                    [[password, text]],
                    submitButton[0],
                    () => {
                        return seleniumX.findElementsAsync(
                            StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Dein Passwort muss mindestens 8 Zeichen lang sein.')]"),
                            'Check: Testing the password field with value: ' + text + '. Looking for email validation error message HTML elements...',
                        )
                            .then(
                                (fieldErrorText) => {
                                    if (fieldErrorText.length) {
                                        seleniumX.eventEmitter.emitEvents(['test-no-authentication-registration', 'check', 'email', 'field', 'validation', 'passed'], [seleniumX, text]);

                                        return seleniumX.sendKeys(password, StaticClassSeleniumX.Selenium.Key.chord(StaticClassSeleniumX.Selenium.Key.CONTROL, 'a'), StaticClassSeleniumX.Selenium.Key.DELETE)
                                            .then(
                                                () => {
                                                    return true;
                                                }
                                            )
                                    } else {
                                        seleniumX.exceptionHandler.throwException(
                                            ClassSeleniumXException.ClassSeverity.FATAL,
                                            ClassSeleniumXException.ClassType.TEST,
                                            'Validation is not functioning properly.',
                                        );
                                    }
                                }
                            )
                    }
                )
            )
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'after', 'password', 'field', 'validation', 'success'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'before', 'valid', 'registration'], [seleniumX]);
        let newUserEmail = process.env.NEW_USER_EMAIL;
        let testValidRegistration = await seleniumX.formTest(
                [
                    [email, newUserEmail],
                    [password, '1]2[3p4o'],
                ],
                submitButton[0],
                () => {
                    // DEBUUUUUUUUUUUUUUUUUUUUUUUUUUUGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG
                    return (
                        !seleniumX.existsElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen eine E-Mail Adresse von Dir.')]"))
                        &&
                        !seleniumX.existsElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Deine E-Mail Adresse sieht nicht richtig aus.')]"))
                        &&
                        !seleniumX.existsElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Wir benötigen ein Passwort um dich anzumelden.')]"))
                        &&
                        !seleniumX.existsElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//span[contains(text(), 'Dein Passwort muss mindestens 8 Zeichen lang sein.')]"))
                    );
                },
            );
        let outcome = await Promise.race([
            seleniumX.waitUntilUrlIs(StaticClassSeleniumX.ClassSeleniumXRoutes.ONBOARDING_PRODUCT_SELECT, 40000),
            seleniumX.driver.wait(new StaticClassSeleniumX.WebDriver.Condition('to see if user exists', async() => {
                let existsElement = await seleniumX.existsElementAsync(
                    StaticClassSeleniumX.Selenium.By.xpath(
                        "//div[contains(string(), 'Diese E-Mail Adresse existiert bereits bei uns. Willst Du dich lieber einloggen?')]",
                    ),
                );
                if(existsElement) {
                    return await seleniumX.findElementAsync(StaticClassSeleniumX.Selenium.By.xpath("//div[contains(string(), 'Diese E-Mail Adresse existiert bereits bei uns. Willst Du dich lieber einloggen?')]"));
                }

                return false;
            }), 40000),
        ]);
        if(outcome instanceof StaticClassSeleniumX.WebDriver.WebElement) {
            seleniumX.exceptionHandler.throwException(
                ClassSeleniumXException.ClassSeverity.FATAL,
                ClassSeleniumXException.ClassType.TEST,
                'Email ' + newUserEmail + ' is already registered. Cannot continue the registration functionality test. Aborting.',
            );
        }
        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'after', 'valid', 'registration', 'passed'], [seleniumX]);

        seleniumX.eventEmitter.emitEvents(['test', 'test-no-authentication-registration', 'success'], [seleniumX]);
        //await seleniumX.takeScreenshot(true, true);

        return seleniumX;
    }

    static async boot(seleniumX) {
        seleniumX = seleniumX || await new ClassSeleniumX();

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-registration', 'check', 'empty', 'values', 'validation'],
            (seleniumX) => {
                seleniumX.logger.log('Trying to submit the form with empty values for email and password...', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-registration', 'check', 'empty', 'values', 'validation', 'passed'],
            (seleniumX, text) => {
                seleniumX.logger.log('Field validation is working with empty values for email and password.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration', 'before', 'email', 'field', 'validation'],
            (seleniumX) => {
                seleniumX.logger.log('Trying to submit the form with invalid values for email...', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-registration', 'check', 'email', 'field', 'validation', 'passed'],
            (seleniumX, text) => {
                seleniumX.logger.log('Field validation is working. Tested with: ' + text, ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration', 'after', 'email', 'field', 'validation', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Email validation is working properly.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration', 'before', 'password', 'field', 'validation'],
            (seleniumX) => {
                seleniumX.logger.log('Trying to submit the form with invalid values for password...', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration', 'after', 'password', 'field', 'validation', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('Password validation is working properly.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test-no-authentication-registration', 'check', 'password', 'field', 'validation', 'passed'],
            (seleniumX, text) => {
                seleniumX.logger.log('Field validation is working. Tested with: ' + text, ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration', 'before', 'valid', 'registration'],
            (seleniumX) => {
                seleniumX.logger.log('Trying to register a new user with valid credentials...', ClassSeleniumXLogger.ClassLogType.CHECK);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration', 'after', 'valid', 'registration', 'passed'],
            (seleniumX) => {
                seleniumX.logger.log('Registered a new user successfully.', ClassSeleniumXLogger.ClassLogType.PASSED);
            },
        );

        seleniumX.eventEmitter.onEvents(
            ['test', 'test-no-authentication-registration', 'success'],
            (seleniumX) => {
                seleniumX.logger.log('The registration page functionality tests have been passed successfully.', ClassSeleniumXLogger.ClassLogType.SUCCESS);
            },
        );

        return seleniumX;
    }
}

module.exports = TestNoAuthenticationRegistration;