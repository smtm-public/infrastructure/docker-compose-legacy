casper.test.begin('Homepage is up?', 1, function(test) {
    casper.start('https://my-s.haftpflichthelden.de:16200/', function() {
        test.assertTitle('Haftpflicht Helden');
    });

    casper.run(function() {
        test.done();
    });
});