const spookyDir = '/usr/local/lib/node_modules/spooky';

let ClassSpooky = null;
try {
    ClassSpooky = require('spooky');
} catch (e) {
    ClassSpooky = require(spookyDir + '/lib/spooky');
}

let ClassU = require('./xutils');

let currentPort = 20000;

class ClassGhostInTheShell extends ClassSpooky
{
    constructor(config, callback) {
        config.child = config.child || {};
        config.child.command = 'C:\\casperjs\\bin\\casperjs.exe';
        config.child.transport = 'http';
        config.child['ignore-ssl-errors'] = 'yes';
        config.child['ssl-protocol'] = 'any';
        config.child['proxy-type'] = 'none';
        config.child.spawnOptions = {
            //stdio: 'inherit',
        };
        config.child.port = currentPort++;

        config.casper = config.casper || {};
        config.casper.logLevel = 'debug';
        config.casper.verbose = true;

        config.pageSettings = config.pageSettings || {};
        config.pageSettings.loadResources = config.pageSettings.loadResources || {};
        config.pageSettings.loadResources.loadImages = true;
        config.pageSettings.loadResources.loadPlugins = false;

        let callbackX = (err) => {
            if (err) {
                let e = new Error('Failed to initialize SpookyJS');
                e.details = err;
                throw e;
            }

            callback.bind(this)();
        };
        super(config, callbackX);

        this.p = require('path');
        this.f = this.p.basename(__filename);
        this.u = new ClassU();

        this.on('error', function (e, stack) {
            console.error(e);

            if (stack) {
                console.log(stack);
            }
        });

        // Uncomment this block to see all of the things Casper has to say.
        // There are a lot.
        // He has opinions.
        this.on('console', function (line) {
            console.log(line);
        });

        this.on('hello', function (greeting) {
            console.log(greeting);
        });

        this.on('log', function (log) {
            if (log.space === 'remote') {
                console.log(log.message.replace(/ \- .*/, ''));
            }
        });
    }
}

class ClassP extends ClassGhostInTheShell
{
    constructor(config, callback, url) {
        super(config, callback);

        this.on('navigation.requested', function (url, navigationType, navigationLocked, isMainFrame) {
            //console.log('NavRequested: ' + url);
            console.log('');
        });

        this.on('page.resource.requested', function (requestData, request) {
            //console.log('Requested: ' + requestData.url);
            // fetch only the requested page HTML (first request)
            //console.log(requestData);
            //console.log(request);
            console.log('');
        });

        this.on('resource.requested', function (requestData, request) {
            //console.log('Requested: ' + requestData.url);
            // fetch only the requested page HTML (first request)
            //console.log(requestData);
            //console.log(request);
            //console.log(this.u);
            //this.u.vdump(['request', request], null, 1, this.f, 96);
            if (requestData.url !== url) {
                //this.echo('Skipped loading: ' + requestData.url);
                //this.utils.dump(request);
                //console.log(typeof request.abort);
                console.log('');
                request.abort();
            } else {
                //console.log(requestData);
                //console.log(request);
            }
        });

        this.on('resource.error', function (resourceError) {
            //console.log('Error: ' + resourceError.errorCode + ', ' + resourceError.errorString + ', ' + resourceError.url + ', ' + resourceError.id);
            console.log('');
        });
    }
}

class ClassPinger
{
    ping(url) {
        //console.log(this);
        new ClassP(
            {},
            function() {
                //console.log(this);
                this.start(url);
                this.then(function () {
                    this.emit('hello', 'Hello, from ' + this.evaluate(function () {
                        return document.title;
                    }));
                    process.exit();
                });
                this.run();
            },
            url
        );
    }
}

class ClassS extends ClassGhostInTheShell
{
    constructor(config, callback) {
        super(config, callback);

        this.pinger = new ClassPinger();
    }
}

module.exports = ClassS;