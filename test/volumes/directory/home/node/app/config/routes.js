'use strict';

const Route = require('../lib/GhostInAShell/Web/Route');

const baseUrl = 'https://my-d.haftpflichthelden.de:16200';

module.exports = {
    'base': new Route(baseUrl),

    'login': new Route(baseUrl + '/login'),
    'signup': new Route(baseUrl + '/signup'),
    'forgot': new Route(baseUrl + '/forgot'),

    'onboarding-product-select': new Route(baseUrl + '/onboarding/product-select', true),
    'onboarding-confirm-race': new Route(baseUrl + '/onboarding/confirm-race', true),
    'onboarding-product-specific': new Route(baseUrl + '/onboarding/product-specific', true),
    'onboarding-step1': new Route(baseUrl + '/onboarding/step1', true),
    'onboarding-step2': new Route(baseUrl + '/onboarding/step2', true),
    'onboarding-step3': new Route(baseUrl + '/onboarding/step3', true),
    'onboarding-step4': new Route(baseUrl + '/onboarding/step4', true),
    'onboarding-step5': new Route(baseUrl + '/onboarding/step5', true),
    'onboarding-signature': new Route(baseUrl + '/onboarding/signature', true),
    'onboarding-welcome': new Route(baseUrl + '/onboarding/welcome', true),

    'dashboard': new Route(baseUrl + '/'),
    'dashboard-contract-select': new Route(baseUrl + '/contract-select', true),
    'dashboard-settings': new Route(baseUrl + '/settings', true),
    'dashboard-info-questions': new Route(baseUrl + '/info-questions', true),
    'dashboard-info-how': new Route(baseUrl + '/info-how', true),
    'dashboard-status': new Route(baseUrl + '/status', true),
    'dashboard-invite': new Route(baseUrl + '/invite', true),
    'dashboard-account-settings': new Route(baseUrl + '/account-settings', true),
    'dashboard-change-payment': new Route(baseUrl + '/change-payment', true),
    'dashboard-confirm-cancel': new Route(baseUrl + '/confirm-cancel', true),
};