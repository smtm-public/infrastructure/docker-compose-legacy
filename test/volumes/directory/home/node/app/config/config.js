'use strict';

module.exports = {
    browser: {
        adapter: 'AdapterSelenium',
        outerWidth: 1024,
        outerHeight: 768,
        innerWidth: 1016,
        innerHeight: 639,
    },
};