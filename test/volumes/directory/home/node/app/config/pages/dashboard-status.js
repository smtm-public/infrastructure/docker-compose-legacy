'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Invite Friends Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Status',
    routes['dashboard-status'],
    elements,
);