'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Redirect Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'JETZT HELDEN CODE TEILEN'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Info How',
    routes['dashboard-info-how'],
    elements,
);