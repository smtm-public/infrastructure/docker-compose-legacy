'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const ElementStyledCheckbox = require('../../lib/GhostInAShell/Web/Element/ElementStyledCheckbox');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Heading',
        [
            new Selector(Selector.Type.XPATH, '//h2[contains(string(), "Deine Beauftragung.")]'),
        ],
    ),
    new ElementStyledCheckbox(
        'Verify That The Provided Information Is Correct And Agree To All Terms And Conditions Checkbox',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Ich stimme diesen Informationen")]/../span[contains(@class, "checkbox")]'),
        ],
    ),
    new ElementStyledCheckbox(
        'Verify Familiarity With The Product To Be Purchased Checkbox',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Ich habe mich mit")]/../span[contains(@class, "checkbox")]'),
        ],
    ),
    new Element(
        'Liability Contract Conditions Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Bedingungswerk'),
        ],
    ),
    new Element(
        'Product Information Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Produktinformationsblatt'),
        ],
    ),
    new Element(
        'Terms Of Service Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'AGB'),
        ],
    ),
    new Element(
        'Data Protection Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Datenschutzerklärung'),
        ],
    ),
    new Element(
        'Commissioning Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Beauftragung'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.XPATH, '//button[contains(string(), "ZUR UNTERSCHRIFT")]'),
        ],
    ),

    new Element(
        'Must Verify That The Provided Information Is Correct And Agree To All Terms And Conditions Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Bitte bestätige die Beauftragung für uns.")]'),
        ],
    ),
    new Element(
        'Must Verify Familiarity With The Product To Be Purchased Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Bitte bestätige, dass du dich mit der Versicherung beschäftigt hast.")]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Step5',
    routes['onboarding-step5'],
    elements,
);