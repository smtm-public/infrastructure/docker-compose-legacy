'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Username',
        [
            new Selector(Selector.Type.CSS, '[type="text"]'),
        ],
    ),
    new Element(
        'Password',
        [
            new Selector(Selector.Type.CSS, '[type="password"]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Account Settings',
    routes['dashboard-account-settings'],
    elements,
);