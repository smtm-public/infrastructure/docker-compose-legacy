'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Go Back Link',
        [
            new Selector(Selector.Type.LINK_TEXT, 'NEIN, SORRY'),
        ],
    ),
    // TODO: MK - Finish
];

module.exports = new Page(
    __filename,
    'Onboarding Confirm Race',
    routes['onboarding-confirm-race'],
    elements,
);