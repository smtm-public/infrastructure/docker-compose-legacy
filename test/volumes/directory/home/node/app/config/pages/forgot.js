'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Email Input Field',
        [
            new Selector(Selector.Type.CSS, '[type="email"]'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Forgot Password',
    routes['forgot'],
    elements,
);