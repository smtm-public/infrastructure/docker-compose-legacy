'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Personal Liability Product Link',
        [
            new Selector(Selector.Type.XPATH, '//i[contains(@class, "icon-pair")]/..'),
        ],
    ),
    new Element(
        'Dog Liability Product Link',
        [
            new Selector(Selector.Type.XPATH, '//i[contains(@class, "icon-fill8")]/..'),
        ],
    ),
    new Element(
        'Horse Liability Product Link',
        [
            new Selector(Selector.Type.XPATH, '//i[contains(@class, "icon-pferde")]/..'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Product Select',
    routes['onboarding-product-select'],
    elements,
);