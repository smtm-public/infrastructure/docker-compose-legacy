'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [

];

module.exports = new Page(
    __filename,
    'Onboarding Welcome',
    routes['onboarding-welcome'],
    elements,
);