'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Yes Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'JA'),
        ],
    ),
    new Element(
        'No Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'NEIN'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Dashboard Confirm Cancel',
    routes['dashboard-confirm-cancel'],
    elements,
);