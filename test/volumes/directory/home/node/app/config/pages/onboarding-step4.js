'use strict';

const Route = require('../../lib/GhostInAShell/Web/Route');
const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const ElementStyledCheckbox = require('../../lib/GhostInAShell/Web/Element/ElementStyledCheckbox');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Payment Method SEPA Radio Button',
        [
            new Selector(Selector.Type.XPATH, '//img[contains(@src, "/images/logo-sepa.svg")]/../span[contains(@class, "radio")]'),
        ],
    ),
    new Element(
        'Payment Method SEPA Logo Image',
        [
            new Selector(Selector.Type.XPATH, '//img[contains(@src, "/images/logo-sepa.svg")]'),
        ],
    ),
    new Element(
        'Change Payment Method Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Zahlmethode ändern'),
        ],
    ),
    new Element(
        'Use Account Number/Bank Code Button',
        [
            new Selector(Selector.Type.LINK_TEXT, 'Kontonummer/BLZ statt IBAN eingeben'),
        ],
    ),
    new Element(
        'IBAN Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "IBAN")]/../input[contains(@type, "text")]'),
        ],
    ),
    new Element(
        'Account Holder Input Field',
        [
            new Selector(Selector.Type.XPATH, '//label[contains(string(), "Kontoinhaber")]/../input[contains(@type, "text")]'),
        ],
    ),
    new ElementStyledCheckbox(
        'Agree To The SEPA Terms Checkbox',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Ich erteile das SEPA-Lastschriftmandat zum Einzug der fälligen Beiträge von meinem Konto.")]/../span[contains(@class, "checkbox")]'),
        ],
    ),
    new Element(
        'Empty IBAN Message',
        [
            new Selector(Selector.Type.XPATH, '//div[contains(string(), "Bitte gib deine Bankdaten ein.")]'),
        ],
    ),
    new Element(
        'Invalid IBAN Message',
        [
            new Selector(Selector.Type.XPATH, '//div[contains(string(), "Deine IBAN sieht nicht richtig aus.")]'),
        ],
    ),
    new Element(
        'Must Agree To The SEPA Terms Message',
        [
            new Selector(Selector.Type.XPATH, '//span[contains(string(), "Bitte erteile uns das SEPA Mandat. ")]'),
        ],
    ),
    new Element(
        'Payment Method Paypal Radio Button',
        [
            new Selector(Selector.Type.XPATH, '//img[contains(@src, "/images/logo-paypal.png")]/../span[contains(@class, "radio")]'),
        ],
    ),
    new Element(
        'Payment Method Bank Card Radio Button',
        [
            new Selector(Selector.Type.XPATH, '//img[contains(@src, "/images/logo-visa-mastercard.png")]/../span[contains(@class, "radio")]'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.CSS, '[type="submit"]'),
        ],
    ),
    new Element(
        'Go Back To The Previous Page Link',
        [
            new Selector(Selector.Type.CSS, '//a[contains(@href, "' + Route.getRouteUrlRelative(routes['onboarding-step4'], routes['base']) + '")]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Step4',
    routes['onboarding-step4'],
    elements,
);