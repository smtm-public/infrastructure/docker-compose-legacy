'use strict';

const Page = require('../../lib/GhostInAShell/Web/Page');
const Element = require('../../lib/GhostInAShell/Web/Element');
const Selector = require('../../lib/GhostInAShell/Web/Element/Selector');
const routes = require('../routes');

const elements = [
    new Element(
        'Signature Canvas',
        [
            new Selector(Selector.Type.CSS, 'div.box-signature>canvas'),
        ],
    ),
    new Element(
        'Submit Button',
        [
            new Selector(Selector.Type.CSS, 'button[type="submit"]'),
        ],
    ),
];

module.exports = new Page(
    __filename,
    'Onboarding Signature',
    routes['onboarding-signature'],
    elements,
);