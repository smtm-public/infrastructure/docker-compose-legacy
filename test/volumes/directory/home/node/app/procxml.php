<?php
$output = $_SERVER['outputNode'];
/*
$output = <<< EOT
<assemblies>
  <assembly name="Login Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:41:23" time="42.865" total="3" passed="3" failed="0" skipped="0">
    <collection name="Load Page: Login" time="4.499" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/login" time="4.456" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.042" status="passed"/>
    </collection>
    <collection name="Valid login" time="38.366" total="1" passed="1" failed="0" skipped="0">
      <test name="Valid login" time="38.366" status="passed"/>
    </collection>
  </assembly>
  <assembly name="Onboarding Product Select Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:42:06" time="5.649" total="6" passed="6" failed="0" skipped="0">
    <collection name="Load Page: Onboarding Product Select" time="0.104" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/product-select" time="0.001" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.103" status="passed"/>
    </collection>
    <collection name="Enumerate Page Elements" time="0.202" total="1" passed="1" failed="0" skipped="0">
      <test name="Whether all defined page elements exist" time="0.202" status="passed"/>
    </collection>
    <collection name="Product buttons" time="5.343" total="3" passed="3" failed="0" skipped="0">
      <test name="Personal Liability Product Link" time="1.934" status="passed"/>
      <test name="Dog Liability Product Link" time="1.848" status="passed"/>
      <test name="Horse Liability Product Link" time="1.561" status="passed"/>
    </collection>
  </assembly>
  <assembly name="Onboarding Step1 Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:42:11" time="3.029" total="5" passed="5" failed="0" skipped="0">
    <collection name="Click on the Personal Liability Product Link" time="1.01" total="1" passed="1" failed="0" skipped="0">
      <test name="Click on the Personal Liability Product Link" time="1.01" status="passed"/>
    </collection>
    <collection name="Load Page: Onboarding Step1" time="0.058" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/step1" time="0.001" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.057" status="passed"/>
    </collection>
    <collection name="Enumerate Page Elements" time="0.331" total="1" passed="1" failed="0" skipped="0">
      <test name="Whether all defined page elements exist" time="0.331" status="passed"/>
    </collection>
    <collection name="Validation" time="1.629" total="1" passed="1" failed="0" skipped="0">
      <test name="Trying to submit the form with unchecked User Record Verification Checkbox" time="1.629" status="passed"/>
    </collection>
  </assembly>
  <assembly name="Onboarding Step2 Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:42:14" time="7.841" total="5" passed="5" failed="0" skipped="0">
    <collection name="Load Page: Onboarding Step2" time="0.046" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/step2" time="0" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.046" status="passed"/>
    </collection>
    <collection name="Enumerate Page Elements" time="2.154" total="1" passed="1" failed="0" skipped="0">
      <test name="Whether all defined page elements exist" time="2.154" status="passed"/>
    </collection>
    <collection name="Validation" time="5.641" total="2" passed="2" failed="0" skipped="0">
      <test name="Empty values validation" time="2.183" status="passed"/>
      <test name="Valid submission" time="3.458" status="passed"/>
    </collection>
  </assembly>
  <assembly name="Onboarding Step3 Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:42:22" time="1.446" total="4" passed="4" failed="0" skipped="0">
    <collection name="Load Page: Onboarding Step3" time="0.037" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/step3" time="0" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.037" status="passed"/>
    </collection>
    <collection name="Enumerate Page Elements" time="0.46" total="1" passed="1" failed="0" skipped="0">
      <test name="Whether all defined page elements exist" time="0.46" status="passed"/>
    </collection>
    <collection name="Valid submission" time="0.949" total="1" passed="1" failed="0" skipped="0">
      <test name="Valid submission" time="0.948" status="passed"/>
    </collection>
  </assembly>
  <assembly name="Onboarding Step4 Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:42:24" time="15.767" total="8" passed="8" failed="0" skipped="0">
    <collection name="Load Page: Onboarding Step4" time="0.086" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/step4" time="0.003" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.083" status="passed"/>
    </collection>
    <collection name="Enumerate Page Elements" time="0.873" total="1" passed="1" failed="0" skipped="0">
      <test name="Whether all defined page elements exist" time="0.873" status="passed"/>
    </collection>
    <collection name="Select payment method SEPA" time="0.811" total="1" passed="1" failed="0" skipped="0">
      <test name="Select payment method SEPA" time="0.811" status="passed"/>
    </collection>
    <collection name="Validation" time="13.996" total="4" passed="4" failed="0" skipped="0">
      <test name="Empty values validation" time="0.708" status="passed"/>
      <test name="Invalid IBAN validation" time="5.143" status="passed"/>
      <test name="Must agree to SEPA terms validation" time="5.079" status="passed"/>
      <test name="Valid submission" time="3.066" status="passed"/>
    </collection>
  </assembly>
  <assembly name="Onboarding Step5 Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:42:39" time="5.135" total="7" passed="7" failed="0" skipped="0">
    <collection name="Load Page: Onboarding Step5" time="0.099" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/step5" time="0" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.099" status="passed"/>
    </collection>
    <collection name="Enumerate Page Elements" time="1.141" total="1" passed="1" failed="0" skipped="0">
      <test name="Whether all defined page elements exist" time="1.141" status="passed"/>
    </collection>
    <collection name="Validation" time="3.895" total="4" passed="4" failed="0" skipped="0">
      <test name="Trying to submit the form with unchecked Checkboxes" time="0.811" status="passed"/>
      <test name="Trying to submit the form with unchecked Verify That The Provided Information Is Correct And Agree To All Terms And Conditions Checkbox" time="0.742" status="passed"/>
      <test name="Trying to submit the form with unchecked Verify Familiarity With The Product To Be Purchased Checkbox" time="1.151" status="passed"/>
      <test name="Valid submission" time="1.19" status="passed"/>
    </collection>
  </assembly>
  <assembly name="Onboarding Signature Page" test-framework="GhostInAShell" environment="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js" run-date="2019-02-09" run-time="14:42:45" time="0.444" total="4" passed="4" failed="0" skipped="0">
    <collection name="Load Page: Onboarding Signature" time="0.025" total="2" passed="2" failed="0" skipped="0">
      <test name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/signature" time="0.001" status="passed"/>
      <test name="Load Ember Liquid Container" time="0.024" status="passed"/>
    </collection>
    <collection name="Enumerate Page Elements" time="0.133" total="1" passed="1" failed="0" skipped="0">
      <test name="Whether all defined page elements exist" time="0.133" status="passed"/>
    </collection>
    <collection name="Sign and submit" time="0.285" total="1" passed="1" failed="0" skipped="0">
      <test name="Sign and submit" time="0.284" status="passed"/>
    </collection>
  </assembly>
</assemblies>
EOT;
*/

/*
$xslDoc = new DOMDocument();
$xslDoc->load(__DIR__ . "/schemas/xunitdotnet-2.0-to-junit.xsl");

$xmlDoc = new DOMDocument();
$xmlDoc->loadXML($output);

$proc = new XSLTProcessor();
$proc->importStylesheet($xslDoc);
try {
    $junitXml = $proc->transformToXML($xmlDoc);
} catch(Throwable $e) {
    echo $e;
}
echo $junitXml;
*/

/*
$xmlDoc = new DOMDocument();
$xslDoc = new DOMDocument();
$xsl = new XSLTProcessor();

$xmlDoc->loadXML($output);
$xslDoc->load(__DIR__ . "/schemas/xunitdotnet-2.0-to-junit.xsl");

libxml_use_internal_errors(true);
$result = $xsl->importStyleSheet($xslDoc);
if (!$result) {
    foreach (libxml_get_errors() as $error) {
        echo "Libxml error: {$error->message}\n";
    }
}
//libxml_use_internal_errors(false);

$transform = null;
if ($result) {
    $transform = $xsl->transformToXML($xmlDoc);
}

echo $transform;
*/

//$xmlfile = "xml/foo.xml";
$xslFile = __DIR__ . "/schemas/xunitdotnet-2.0-to-junit.xsl";
$xsdFile = __DIR__ . "/schemas/junit-10.xsd";
$saxonProc = new Saxon\SaxonProcessor();
$parsed = $saxonProc->parseXmlFromString($output);
$xsltProc = $saxonProc->newXsltProcessor();
//$version = $saxonProc->version();
//echo 'Saxon Processor version: '.$version;
//echo '<br/>';
//$xsltProc->setSourceFromFile($xmlfile);
$xsltProc->setSourceFromXdmValue($parsed);
$xsltProc->compileFromFile($xslFile);
//$result = $xsltProc->transformToValue();
$result = $xsltProc->transformToString();
//if($result != null) {
//    echo 'exampleSimple1:';
//    echo 'Output:'.$result;
//} else {
//    echo "Result is null";
//}
$xsltProc->clearParameters();
$xsltProc->clearProperties();

/*
// Saxon schema validation requires a paid EE license: http://www.saxonica.com/products/feature-matrix-9-9.xml
$schemaValidator = new Saxon\SchemaValidator();
//$schemaValidator = $saxonProc->newSchemaValidator();
$schemaValidator->setProperty('report', 'true');
$schemaValidator->setProperty('report-node', 'true');
$schemaValidator->setSourceNode($result);
$schemaValidator->registerSchemaFromFile($xsdFile);
$validated = $schemaValidator->validate();
// this next line results in null
$validationReport = $schemaValidator->getValidationReport();
//$validationReportString = $validationReport->getStringValue();
if($schemaValidator->exceptionOccurred()) {
    echo 'document is not valid';
}
echo $validated;
*/


$xmlDoc = new DOMDocument();
$xmlDoc->loadXML($result);
$ddValidationResult = $xmlDoc->schemaValidate($xsdFile);
//echo $ddValidationResult;
echo $result;

/*
$processed = <<< EOT
<?xml version="1.0" encoding="UTF-8"?>
<testsuites xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xunit="http://www.xunit.org"> <testsuite name
="" tests="3" time="16.410" timestamp="2019-02-12T13:03:17" failures="0" errors="" skipped="0"> <properties
> <property name="environment" value="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/
bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnod
e%2Fapp%2Fsuite-onboarding.js"/> <property name="test-framework" value="GhostInAShell"/> <property name="co
nfig-file" value=""/> </properties> <testcase classname="" name="Load Route: https://my-d.haftpflichthelden
.de:16200/login" time="2.284"/> <testcase classname="" name="Load Ember Liquid Container" time="0.042"/> <t
estcase classname="" name="Valid login" time="14.081"/> </testsuite> <testsuite name="" tests="6" time="4.0
60" timestamp="2019-02-12T13:03:33" failures="0" errors="" skipped="0"> <properties> <property name="enviro
nment" value="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11
.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboardi
ng.js"/> <property name="test-framework" value="GhostInAShell"/> <property name="config-file" value=""/> </
properties> <testcase classname="" name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/pro
duct-select" time="0.000"/> <testcase classname="" name="Load Ember Liquid Container" time="0.053"/> <testc
ase classname="" name="Whether all defined page elements exist" time="0.165"/> <testcase classname="" name=
"Personal Liability Product Link" time="1.299"/> <testcase classname="" name="Dog Liability Product Link" t
ime="1.466"/> <testcase classname="" name="Horse Liability Product Link" time="1.076"/> </testsuite> <tests
uite name="" tests="5" time="2.182" timestamp="2019-02-12T13:03:37" failures="0" errors="" skipped="0"> <pr
operties> <property name="environment" value="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/us
r/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fho
me%2Fnode%2Fapp%2Fsuite-onboarding.js"/> <property name="test-framework" value="GhostInAShell"/> <property
name="config-file" value=""/> </properties> <testcase classname="" name="Click on the Personal Liability Pr
oduct Link" time="0.660"/> <testcase classname="" name="Load Route: https://my-d.haftpflichthelden.de:16200
/onboarding/step1" time="0.000"/> <testcase classname="" name="Load Ember Liquid Container" time="0.059"/>
<testcase classname="" name="Whether all defined page elements exist" time="0.365"/> <testcase classname=""
 name="Valid submission" time="1.098"/> </testsuite> <testsuite name="" tests="5" time="5.583" timestamp="2
019-02-12T13:03:39" failures="0" errors="" skipped="0"> <properties> <property name="environment" value="la
ng=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=n
ode&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js"/> <proper
ty name="test-framework" value="GhostInAShell"/> <property name="config-file" value=""/> </properties> <tes
tcase classname="" name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/step2" time="0.000"
/> <testcase classname="" name="Load Ember Liquid Container" time="0.077"/> <testcase classname="" name="Wh
ether all defined page elements exist" time="1.537"/> <testcase classname="" name="Empty values validation"
 time="1.726"/> <testcase classname="" name="Valid submission" time="2.242"/> </testsuite> <testsuite name=
"" tests="4" time="1.050" timestamp="2019-02-12T13:03:45" failures="0" errors="" skipped="0"> <properties>
<property name="environment" value="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bi
n/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%
2Fapp%2Fsuite-onboarding.js"/> <property name="test-framework" value="GhostInAShell"/> <property name="conf
ig-file" value=""/> </properties> <testcase classname="" name="Load Route: https://my-d.haftpflichthelden.d
e:16200/onboarding/step3" time="0.000"/> <testcase classname="" name="Load Ember Liquid Container" time="0.
042"/> <testcase classname="" name="Whether all defined page elements exist" time="0.265"/> <testcase class
name="" name="Valid submission" time="0.743"/> </testsuite> <testsuite name="" tests="8" time="11.540" time
stamp="2019-02-12T13:03:46" failures="0" errors="" skipped="0"> <properties> <property name="environment" v
alue="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;version=v11.6.0&amp
;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js"/>
 <property name="test-framework" value="GhostInAShell"/> <property name="config-file" value=""/> </properti
es> <testcase classname="" name="Load Route: https://my-d.haftpflichthelden.de:16200/onboarding/step4" time
="0.000"/> <testcase classname="" name="Load Ember Liquid Container" time="0.046"/> <testcase classname=""
name="Whether all defined page elements exist" time="0.756"/> <testcase classname="" name="Select payment m
ethod SEPA" time="0.907"/> <testcase classname="" name="Empty values validation" time="0.554"/> <testcase c
lassname="" name="Invalid IBAN validation" time="4.095"/> <testcase classname="" name="Must agree to SEPA t
erms validation" time="3.202"/> <testcase classname="" name="Valid submission" time="1.980"/> </testsuite>
<testsuite name="" tests="7" time="3.619" timestamp="2019-02-12T13:03:58" failures="0" errors="" skipped="0
"> <properties> <property name="environment" value="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPa
th=/usr/local/bin/node&amp;version=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]
=%2Fhome%2Fnode%2Fapp%2Fsuite-onboarding.js"/> <property name="test-framework" value="GhostInAShell"/> <pro
perty name="config-file" value=""/> </properties> <testcase classname="" name="Load Route: https://my-d.haf
tpflichthelden.de:16200/onboarding/step5" time="0.000"/> <testcase classname="" name="Load Ember Liquid Con
tainer" time="0.029"/> <testcase classname="" name="Whether all defined page elements exist" time="0.898"/>
 <testcase classname="" name="Trying to submit the form with unchecked Checkboxes" time="0.694"/> <testcase
 classname="" name="Trying to submit the form with unchecked Verify That The Provided Information Is Correc
t And Agree To All Terms And Conditions Checkbox" time="0.673"/> <testcase classname="" name="Trying to sub
mit the form with unchecked Verify Familiarity With The Product To Be Purchased Checkbox" time="0.713"/> <t
estcase classname="" name="Valid submission" time="0.611"/> </testsuite> <testsuite name="" tests="4" time=
"27.316" timestamp="2019-02-12T13:04:01" failures="0" errors="" skipped="0"> <properties> <property name="e
nvironment" value="lang=node.js&amp;platform=linux&amp;arch=x64&amp;execPath=/usr/local/bin/node&amp;versio
n=v11.6.0&amp;argv0=node&amp;arg[0]=%2Fusr%2Flocal%2Fbin%2Fnode&amp;arg[1]=%2Fhome%2Fnode%2Fapp%2Fsuite-onb
oarding.js"/> <property name="test-framework" value="GhostInAShell"/> <property name="config-file" value=""
/> </properties> <testcase classname="" name="Load Route: https://my-d.haftpflichthelden.de:16200/onboardin
g/signature" time="0.000"/> <testcase classname="" name="Load Ember Liquid Container" time="0.023"/> <testc
ase classname="" name="Whether all defined page elements exist" time="0.064"/> <testcase classname="" name=
"Sign and submit" time="27.229"/> </testsuite> </testsuites>
EOT;
*/