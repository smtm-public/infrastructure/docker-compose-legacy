let ghost = require('ghostjs');
let assert = require('assert');

describe('Google', () => {
    it('has some content', async () => {
        await ghost.open('https://google.com');
        let pageTitle = await ghost.pageTitle();
        assert.equal(pageTitle, 'Google');

        // Get the content of the body
        let body = await ghost.findElement('body');
        assert.ok(await body.isVisible());
        console.log('got body content', await body.html());

        // Wait for the first link and click it
        let firstLink = await ghost.waitForElement('a');
        firstLink.click();
    });
});