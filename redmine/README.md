### Redmine

On the first run navigate to the web interface and click on "Sign In" in the top right corner of the page then
login with admin/admin and set a password for the root account.

#### Issues:
  - Apparently there is no way to setup Redmine's mailer to bypass SMTP SSL certificate verification (check lines (585-587) in /usr/local/lib/ruby/2.6.0/net/smtp.rb)
