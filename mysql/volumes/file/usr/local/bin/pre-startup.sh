#!/bin/bash

echo '|-/-\-||-/-\-||-/-\-||-/-\-|RUNNING pre-startup.sh|-/-\-||-/-\-||-/-\-||-/-\-|'

mkdir -p /init/ && \
rm -f /init/init-project-users-databases-plain.sql
pre-mysql_install_db.sh
cp -f /docker-entrypoint-initdb.d/00-init-project-users-databases.sql /init/init-project-users-databases-plain.sql
# This one removes all comments
sed -i -r 's/#.*//' /init/init-project-users-databases-plain.sql
# This one removes all blank lines
sed -i -r '/^[[:space:]]*$/d; /^[[:space:]]*$/d; :a;$!{N;s/\n//;ba;}; s/;/;\n/g' /init/init-project-users-databases-plain.sql
cat > /init/init-project-users-databases-plain.sql << EOL
EOL
cat /init/init-project-users-databases-plain.sql
