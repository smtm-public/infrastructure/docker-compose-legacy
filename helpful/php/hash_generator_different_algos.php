<?php

$string = 'organization0-owner';

$algos = [
    'crc32',
    'crc32b',
    'md2',
    'md4',
    'md5',
    'ripemd128',
    'ripemd160',
    'ripemd256',
    'ripemd320',
    'whirlpool',
    'tiger128,3',
    'tiger160,3',
    'tiger192,3',
    'tiger128,4',
    'tiger160,4',
    'tiger192,4',
    'snefru',
    'snefru256',
    'gost',
    'gost-crypto',
    'adler32',
    'fnv132',
    'fnv1a32',
    'fnv164',
    'fnv1a64',
    'haval128,3',
    'haval160,3',
    'haval192,3',
    'haval224,3',
    'haval256,3',
    'haval128,4',
    'haval160,4',
    'haval192,4',
    'haval224,4',
    'haval256,4',
    'haval128,5',
    'haval160,5',
    'haval192,5',
    'haval224,5',
    'haval256,5',
    'joaat',
    'fnv132',
    'fnv164',
    'sha1',
    'sha224',
    'sha3-224',
    'sha256',
    'sha3-256',
    'sha384',
    'sha3-384',
    'sha512',
    'sha3-512',
    'sha224',
    'sha256',
    'sha512/224',
    'sha512/256',
];

echo 'algo         | length | output' . "\n";
echo '------------------------------------------------------------------------------------' . "\n";
foreach ($algos as $algo) {
    $output = hash($algo, $string);
    $length = strlen($output);
    echo str_pad($algo, 12) . ' | ' . str_pad($length, 6) . ' | ' . $output . "\n";
}
echo "\n";
$output = password_hash($string, PASSWORD_BCRYPT);
$length = strlen($output);
echo str_pad('BCrypt', 12) . ' | ' . str_pad($length, 6) . ' | ' . $output . "\n";
$output = password_hash($string, PASSWORD_ARGON2I);
$length = strlen($output);
echo str_pad('Argon2i', 12) . ' | ' . str_pad($length, 6) . ' | ' . $output . "\n";
$output = password_hash($string, PASSWORD_ARGON2ID);
$length = strlen($output);
echo str_pad('Argon2id', 12) . ' | ' . str_pad($length, 6) . ' | ' . $output . "\n";
