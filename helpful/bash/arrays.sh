#!/usr/bin/env bash

# Initialize a numerically indexed array and loop through it
bashHistoryServices=( "alpine3" "ansible" "apache24" "backup_borg" "beats_filebeat" "beats_metricbeat" "cassandra" "centos7" "chronograf" "debian9_stretch" "debian10_buster" "elasticsearch" "fluxbox-vnc" "grafana" "haproxy" "influxdb" "jsdetox" "kafka" "kapacitor" "kibana" "logstash" "mail_dovecot" "mail_postfix" "mongo" "mysql" "nginx-unit1" "nodejs11" "openapi-gui" "openapigenerator" "openjdk14" "opensuse_leap" "opensuse_tumbleweed" "php73-fpm" "postgres" "python3" "rabbitmq" "redis" "redmine" "sftp" "sonarqube" "telegraf" "test" "tomcat" "traefik" "ubuntu18_cosmic" "ubuntu19_eoan" "xwiki" )
for ((j=0;j<${#bashHistoryServices[@]};j++)); do
    touch "${DIR}/${bashHistoryServices[${j}]}/volumes/file/root/.bash_history";
done

# Initialize an associative array and loop through it including multi-dimensional array simulation
declare -A hostsEntries;
hostsEntries["0:url"]="d.default.com";
hostsEntries["0:target"]="127.0.0.1";
hostsEntries["1:url"]="www.google-analytics.com";
hostsEntries["1:target"]="127.0.0.1";
hostsEntries["2:url"]="google-analytics.com";
hostsEntries["2:target"]="127.0.0.1";
hostsEntries["3:url"]="ssl.google-analytics.com";
hostsEntries["3:target"]="127.0.0.1";
j=0;
k=0;
for ((j=0;j<$((${#hostsEntries[@]} / 2));j++)); do
    target=${hostsEntries["${j}:target"]};
    url=${hostsEntries["${j}:url"]};
    if [[ ! $(echo "${hostsFileContent}" | grep -E "${target//\./\\.}[[:space:]]+${url//./\\.}") ]]; then
        echo "Adding entry: ${hostsEntries["${j}:target"]}"    "${hostsEntries["${j}:url"]}";
        hostsEntriesAdd[${j}]="${lineSeparator}${hostsEntries["${j}:target"]}    ${hostsEntries["${j}:url"]}";
        ((k++));
    fi
done;
