#!/usr/bin/env bash

# Unblock port
firewall-cmd --zone=public --add-port=81/tcp --permanent
firewall-cmd --reload
# Block IP address
firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='195.154.181.127' reject"
firewall-cmd --reload
# View all rules
firewall-cmd --list-all
