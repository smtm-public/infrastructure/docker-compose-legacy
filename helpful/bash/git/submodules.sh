#!/usr/bin/env bash

# Add a new submodule
git submodule add -b master git@github.com:username/repo.git path/to/submodule
# If the path is already a valid git repository (The default remote is the remote of the remote-tracking branch of the current
# branch. If no such remote-tracking branch exists or the HEAD is detached, "origin" is assumed to be the default remote.)
git submodule add -b master path/to/submodule
# Then to absorb its git dir into the superproject and replace it with a .git file instead
git submodule absorbgitdirs

# Checkout the default remote tracking branch of all submodules recursively
git submodule foreach -q --recursive 'branch="$(git config -f $toplevel/.gitmodules submodule.$name.branch)"; [ "$branch" = "" ] && git checkout master || git switch $branch' –
# Multi-line
git submodule foreach -q --recursive \
    'branch="$(git config -f $toplevel/.gitmodules submodule.$name.branch)"; \
     [ "$branch" = "" ] && \
     git checkout master || git switch $branch' –
# - or -
git submodule foreach -q --recursive 'git switch $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'
# Multi-line
git submodule foreach -q --recursive \
  'git switch \
  $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'

# Git switch available in Git >=2.23.0 otherwise use Git checkout
# Checkout the default remote tracking branch of all submodules recursively
git submodule foreach -q --recursive 'branch="$(git config -f $toplevel/.gitmodules submodule.$name.branch)"; [ "$branch" = "" ] && git checkout master || git checkout $branch' –
# Multi-line
git submodule foreach -q --recursive \
    'branch="$(git config -f $toplevel/.gitmodules submodule.$name.branch)"; \
     [ "$branch" = "" ] && \
     git checkout master || git checkout $branch' –
# - or -
git submodule foreach -q --recursive 'git checkout $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'
# Multi-line
git submodule foreach -q --recursive \
  'git checkout \
  $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'
