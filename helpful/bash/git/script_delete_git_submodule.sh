#!/bin/bash

if [[ -z $1 ]]; then
    echo 'You need to provide the path to the submodule. ex: delete_git_submodule.sh path/to/submodule'
    exit 1
fi

submodule=${1%/}

# Remove the submodule entry from .git/config
git submodule deinit -f "${submodule}"

# Remove the submodule directory from the superproject's .git/modules directory
rm -rf ".git/modules/${submodule}"

# Remove the entry in .gitmodules and remove the submodule directory located at path/to/submodule
git rm -f "${submodule}"
