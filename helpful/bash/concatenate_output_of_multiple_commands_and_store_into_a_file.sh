#!/usr/bin/env bash

# This creates a docker.compose.yml file from the docker-compose.yml.template.
# The first command removes everything from "services:" (including) to the end of the template file.
# The second command inserts the "services:\n" back.
# The third command joins all the *.yml occurrences in the template with the corresponding file contents
# The output of the three commands is concatenated and stored (overwritten) in a corresponding docker-compose.*.yml file
# The whole thing is repeated (this is one DRY fuck...) for each of the three templates.
# The next line is needed otherwise on Windows `grep -P` fails with the following error:
# "grep: -P supports only unibyte and UTF-8 locales"
# causing the compiled docker-compose files to have empty "services:" contents.
export LC_CTYPE="C.UTF-8"
{ \
    sed '/services:/,/\$/ d' docker-compose.yml.template; \
    echo 'services:'$'\n'; \
    #sed -r 's/^(?<=#).+\.yml$//' docker-compose.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
    grep -P '^(?!#).*\.yml$' docker-compose.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'; \
} | cat > docker-compose.yml
