#!/usr/bin/env bash

# List configured repos
zypper repos -d
# Add a repo
zypper addrepo http://download.opensuse.org/distribution/11.4/repo/oss/ oss
