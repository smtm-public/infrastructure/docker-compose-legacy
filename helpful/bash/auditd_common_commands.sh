#!/usr/bin/env bash

# add auditd rules to monitor directories recursively
auditctl -w /home/myCA/certs -p rwxa -k apache-access-certificates
auditctl -w /home/templay -p rwxa -k apache-access
# remove audit rules
auditctl -W /home/myCA/certs -p rwxa -k apache-access-certificates
auditctl -w /home/templay -p rwxa -k apache-access
# list audit rules
auditctl -l
