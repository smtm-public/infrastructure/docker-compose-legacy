#!/usr/bin/env bash

echo 'Creating the linux user ('${user}')...';
if getent passwd ${user} > /dev/null 2>&1; then
    echo "Notice: the user already exists.";
else
    useradd ${user};
    echo 'User created successfully.';
fi

echo 'Setting password for the linux user ('${user}') using the provided one...';
echo ${password} | passwd ${user} --stdin;
echo 'Adding the linux user ('${user}') to the sudoers group "wheel"...';
usermod -aG wheel ${user};
