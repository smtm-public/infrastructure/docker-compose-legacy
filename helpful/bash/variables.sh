#!/usr/bin/env bash

# Remove tab and newline characters from the value
${variable//[$'\t\r\n']}
