#!/usr/bin/env bash

# copy with overall progress indicator
/usr/local/bin/rsync -ah --info=progress2 /source /destination
# copy with single file progress indicator
/usr/local/bin/rsync -ah --progress /source /destination
