#!/usr/bin/env bash

# Install policycoreutils-python for the semanage command

# Change context of all files in a target directory
semanage fcontext -a -t httpd_sys_content_t "/path/to(/.*)?"
restorecon -R -v /path/to

# Change context of an individual file
chcon -t httpd_exec_t /path/to/file.ext

# Change context of a tcp port
semanage port -a -t http_port_t -p tcp 120
