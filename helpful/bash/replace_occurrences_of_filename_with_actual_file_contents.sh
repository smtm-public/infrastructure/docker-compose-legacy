#!/usr/bin/env bash

# Find all occurrences of strings ending with .yml which don't start with a hashtag and replace them with the contents of the file with the same name
grep -P '^(?!#).*\.yml$' docker-compose.yml.template | xargs awk 'FNR==1 && NR>1{print ""}1'
