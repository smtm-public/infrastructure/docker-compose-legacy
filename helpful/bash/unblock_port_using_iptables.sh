#!/usr/bin/env bash

iptables -A RULES_GROUP -p tcp -m tcp --dport 5432 -j ACCEPT
