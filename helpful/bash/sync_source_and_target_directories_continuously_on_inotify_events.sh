#!/usr/bin/env bash

# Build rsync from source
cd /tmp/rsync-3.1.2
./configure
make
make install

while true; do
  inotifywait -r -e modify,create,delete /home/vmail/
  rsync -avz /home/vmail/* /mail_storage/
done
