#!/usr/bin/env bash

# Find all *.sh files starting in the current directory and recurse in subdirectories
# then add executable permissions
find . -type f -name "*.sh" -exec chmod +x {} \;

# Find all *.sh files starting in the current directory and recurse in subdirectories
# then display them as augmented ls -l
find . -name "*.sh" -exec stat --format="%A %U %G %10s %y %F %n" {} \;

# Same as the above but using the -printf options instead of -exec stat
find . -name "*.sh" -printf "%M %u %g %10s %t %p\n"

# Same as the above but using -exec ls -l
find . -name "*.sh" -exec ls -l {} \;

# recursively search for a file by name
find / -name libclang_rt.san-x86_64.a 2>/dev/null
