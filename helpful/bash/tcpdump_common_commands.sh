#!/usr/bin/env bash

# Dump all traffic on all interfaces for a specific port
tcpdump -i any 'port 80'
