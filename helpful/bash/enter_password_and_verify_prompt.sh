#!/usr/bin/env bash

while true; do
    read -p 'Specify the linux user password: ' -s projectLinuxUserPassword;
    echo;
    read -p 'Specify the linux user password (again): ' -s projectLinuxUserPassword2;
    echo;
    [ "$projectLinuxUserPassword" = "$projectLinuxUserPassword2" ] && break;
    echo "The passwords do not match. Please try again!";
done;
