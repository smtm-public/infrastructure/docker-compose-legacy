## The MySQL 5.5 way...
#CREATE DATABASE IF NOT EXISTS ${DATABASE} CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci';
#GRANT ALL PRIVILEGES ON ${DATABASE}.* TO '${USERNAME}'@'%' IDENTIFIED BY '${PASSWORD}';
#FLUSH PRIVILEGES;

## The MySQL 8 way!
CREATE USER IF NOT EXISTS '${USERNAME}'@'%' IDENTIFIED BY '${PASSWORD}';
CREATE DATABASE IF NOT EXISTS ${DATABASE} CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_general_ci';
GRANT ALL PRIVILEGES ON ${DATABASE}.* TO '${USERNAME}'@'%';
FLUSH PRIVILEGES;

DROP USER '${USERNAME}';

REVOKE ALL PRIVILEGES ON ${DATABASE}.* FROM '${USERNAME}';
