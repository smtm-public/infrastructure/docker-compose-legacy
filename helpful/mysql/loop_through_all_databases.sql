START TRANSACTION;

DECLARE Finished BOOL DEFAULT FALSE;
DECLARE TableName VARCHAR(10);

DECLARE DatabasesCursor CURSOR FOR
SELECT `SCHEMA_NAME` FROM `information_schema`.`SCHEMATA` WHERE `SCHEMA_NAME` LIKE "%_packagingmanager_production";

DECLARE CONTINUE HANDLER FOR NOT FOUND SET Finished = TRUE;
Cd
OPEN DatabasesCursor;

MainLoop: LOOP
FETCH DatabasesCursor INTO DatabaseName;
IF Finished THEN
LEAVE MainLoop;
END IF;

# Create the new table
SET @queryText = CONCAT('
CREATE TABLE IF NOT EXISTS ', DatabaseName, '.`label_template_export_settings` (
      `label_template_id` INT(11) NOT NULL,
      `export_setting_id` INT(10) UNSIGNED NOT NULL,
      `created_at` INT UNSIGNED NOT NULL,
      PRIMARY KEY (`label_template_id`, `export_setting_id`),
      FOREIGN KEY (`label_template_id`) REFERENCES `label_templates`(`id`) ON DELETE CASCADE,
      FOREIGN KEY (`export_setting_id`) REFERENCES `export_settings`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
');
PREPARE createTableQuery FROM @queryText;
EXECUTE createTableQuery;
DEALLOCATE PREPARE createTableQuery;

# Create label_template_export_settings records for all label_template records which have an existing export_setting_id set
SET @queryText = CONCAT('
INSERT INTO
 ', DatabaseName, '.`label_template_export_settings` (`label_template_id`, `export_setting_id`, `created_at`)
SELECT
 `id` AS `label_template_id`, `export_setting_id`, UNIX_TIMESTAMP() AS `created_at`
FROM
 `label_templates`
WHERE
 `export_setting_id` IS NOT NULL
');
PREPARE insertQuery FROM @queryText;
EXECUTE insertQuery;
DEALLOCATE PREPARE insertQuery;

# Drop the legacy column
SET @queryText = CONCAT('
ALTER TABLE
 ', DatabaseName, '.`label_templates`
DROP COLUMN `export_setting_id`
');
PREPARE alterTableQuery FROM @queryText;
EXECUTE alterTableQuery;
DEALLOCATE PREPARE alterTableQuery;
END LOOP;

CLOSE DatabasesCursor;

COMMIT;
