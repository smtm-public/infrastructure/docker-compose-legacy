USE some_schema;
DELIMITER \$\$
DROP PROCEDURE IF EXISTS truncate_and_reset_auto_increment_if_exist\$\$

CREATE  PROCEDURE truncate_and_reset_auto_increment_if_exist(IN tbl_name VARCHAR(150) )
  BEGIN
    IF EXISTS( SELECT 1 FROM information_schema.TABLES WHERE table_name = tbl_name AND table_schema = DATABASE()) THEN
        SET @query = CONCAT('TRUNCATE ', tbl_name);
        PREPARE stmt FROM @query;
        SET FOREIGN_KEY_CHECKS = 0;
        EXECUTE stmt;
        SET FOREIGN_KEY_CHECKS = 1;
        DEALLOCATE PREPARE stmt;
        SET @query = CONCAT('ALTER TABLE ', tbl_name, ' AUTO_INCREMENT=1');
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;
  END \$\$

DELIMITER ;
