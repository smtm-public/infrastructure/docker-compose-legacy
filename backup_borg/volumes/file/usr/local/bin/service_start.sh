#!/bin/bash

#if [[ ${HEHE_ENVIRONMENT} != 'development' ]]; then
#    service cron start
#fi
#/usr/sbin/sshd -D

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
# or most of the time the line below is enough (if not a symlink)
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )";

rm -f /var/run/supervisor.pid
# Refer to the notes in /etc/supervisor/conf.d/supervisord.conf on the supervisorctl default lookup sock path as it
# varies across supervisor builds on different Linux distros
rm -f /var/run/supervisor.sock
/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf

exit 0

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )";

# Start the first process
${DIR}/start_ssh.sh
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start SSH: $status"
  exit $status
fi

if [[ ${HEHE_ENVIRONMENT} != 'development' ]]; then
    # Start the second process
    ${DIR}/start_cron.sh
    status=$?
    if [ $status -ne 0 ]; then
      echo "Failed to start Cron: $status"
      exit $status
    fi
fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds
while sleep 60; do
  ps aux |grep start_ssh.sh |grep -q -v grep
  PROCESS_1_STATUS=$?
  if [ $PROCESS_1_STATUS -ne 0 ]; then
    echo "The SSH process has already exited."
    exit 1
  fi
  if [[ ${HEHE_ENVIRONMENT} != 'development' ]]; then
      ps aux |grep start_cron.sh |grep -q -v grep
      PROCESS_2_STATUS=$?
      if [  $PROCESS_2_STATUS -ne 0 ]; then
        echo "The Cron process has already exited."
        exit 1
      fi
  fi
done
