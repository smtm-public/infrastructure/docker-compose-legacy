#!/bin/bash

echo 'Running backup procedure...'

echo 'Dumping database: coreapi...'
tm=$(date +"%Y%m%d%H%M%S")
ssh -i /root/.ssh/id_rsa root@helden-mysql "/usr/local/mysql/bin/mysqldump -u coreapi -p${COREAPI_MYSQL_PASSWORD} coreapi > /backups/dump/database/coreapi-${tm}.sql"

echo 'Backing up database: coreapi...'
sshfs root@insurance-hero_mysql:/backups/dump/database/ /borgbackup/mounts/mysql/dump/database/
cd /borgbackup/mounts/mysql/dump/database/
borg create /borgbackup/repos/mysql/dump/database/coreapi/::coreapi-${tm} .
rm -rf ./*
cd ..
fusermount -u /borgbackup/mounts/mysql/dump/database/
echo "Backup /borgbackup/repos/mysql/dump/database/coreapi/::coreapi-${tm} successful.";
