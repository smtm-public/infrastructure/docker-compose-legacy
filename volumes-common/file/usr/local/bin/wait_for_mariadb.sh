#!/bin/bash

echo "Waiting for: Host ${COMPOSE_PROJECT_NAME:-default}-mariadb to start the service on port 3306"
# Wait for the MySQL service to be fully up and running
while ! /usr/bin/mysqladmin ping --host="${COMPOSE_PROJECT_NAME:-default}-mariadb" --port=3306 --silent; do
    #echo 'Waiting for MariaDB to start...'
    sleep 1
done
echo "Done waiting for: Host ${COMPOSE_PROJECT_NAME:-default}-mariadb to start the service on port 3306"
