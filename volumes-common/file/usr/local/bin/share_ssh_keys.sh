#!/bin/bash

SSH_USER="root"
SSH_USER_DIRECTORY="/root"
SHARE_WITH_SERVICE=""
OPTIND=1
while getopts ":u:d:w:" o; do
    case "${o}" in
        u)
            SSH_USER=${OPTARG}
            ;;
        d)
            SSH_USER_DIRECTORY=${OPTARG}
            ;;
        w)
            SHARE_WITH_SERVICE="${OPTARG}/"
            ;;
    esac
done
shift $((OPTIND-1))

# Remove the trailing slash
#[[ "${SSH_USER_DIRECTORY}" == */ ]] && SSH_USER_DIRECTORY="${SSH_USER_DIRECTORY: : -1}" # Bash 4.2
length=${#SSH_USER_DIRECTORY}
last_char=${SSH_USER_DIRECTORY:length-1:1}
[[ $last_char == "/" ]] && SSH_USER_DIRECTORY=${SSH_USER_DIRECTORY:0:length-1}; :

mkdir -p "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}"
find "${SSH_USER_DIRECTORY}/.ssh/" -type f -name "id_rsa*" -maxdepth 1 -delete
find "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}" -type f -name "id_rsa*" -maxdepth 1 -delete
find "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}" -type f -name "ssh_host*" -maxdepth 1 -delete
ssh-keygen -t rsa -b 4096 -P '' -f "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}id_rsa"
cp -f "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}id_rsa.pub" "${SSH_USER_DIRECTORY}/.ssh/authorized_keys"
cp -f /etc/ssh/ssh_host_rsa_key.pub "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}ssh_host_rsa_key.pub"
cp -f /etc/ssh/ssh_host_dsa_key.pub "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}ssh_host_dsa_key.pub"
cp -f /etc/ssh/ssh_host_ecdsa_key.pub "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}ssh_host_ecdsa_key.pub"
cp -f /etc/ssh/ssh_host_ed25519_key.pub "${SSH_USER_DIRECTORY}/.ssh/keys/${SHARE_WITH_SERVICE}ssh_host_ed25519_key.pub"

chmod 644 /etc/ssh/sshd_config
chmod g-w "${SSH_USER_DIRECTORY}/"
# Changing file/directory permissions of volume contents on Windows hosts does not work. They seem to always be mounted as owner root and 777 permissions and chmod/chown silently fail
# https://forums.docker.com/t/cannot-change-owner-and-permissions-of-a-volume-inside-container/26797
# To work around this `StrictModes no` has been setup in the /etc/ssh/sshd_config file
chmod 700 "${SSH_USER_DIRECTORY}/.ssh/"
chmod 600 "${SSH_USER_DIRECTORY}/.ssh/authorized_keys"
# Needed for ${SSH_USER_DIRECTORY}/.ssh/ and all files within
chown -R "${SSH_USER}":"${SSH_USER}" "${SSH_USER_DIRECTORY}/"