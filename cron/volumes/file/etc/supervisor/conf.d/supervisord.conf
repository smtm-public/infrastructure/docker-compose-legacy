[unix_http_server]
# On Debian/Ubuntu this is the default sock path which supervisorctl looks up. By providing exactly this value,
# supervisorctl can be used without specifying -c/--configuration (where a [supervisorctl] section with an appropriate
# serverurl= directive should be present) or -s/--serverurl. This path varies across different supervisor builds on
# different Linux distros.
file=/var/run/supervisor.sock

[inet_http_server]
port=0.0.0.0:9001

[rpcinterface:supervisor]
supervisor.rpcinterface_factory=supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl=unix:///var/run/supervisor.sock

[supervisord]
nodaemon=true
logfile=/var/log/supervisor/supervisord.log
pidfile=/var/run/supervisord.pid



[program:rsyslogd]
# command lists "; && ||" are not supported in sh
command=bash -c 'rm -f /var/run/rsyslogd.pid -- && rsyslogd -n'
stdout_logfile=/dev/fd/1
stdout_logfile_maxbytes=0
stderr_logfile=/dev/fd/2
stderr_logfile_maxbytes=0
autorestart=true

[program:cron]
# command lists "; && ||" are not supported in sh
#
# Cron jobs run in a separate shell with a very limited environment:
# https://serverfault.com/questions/337631/crontab-execution-doesnt-have-the-same-environment-variables-as-executing-user
# To import the environment from the actual cron process either:
# 1. Save the environment variables to a file when starting the cron service (in /etc/supervisor/conf.d/supervisord.conf) using:
#    ```
#    env > /entrypoint_envs
#    ```
#    ...and then load them as part of the cron job command or the script which it may call using:
#    ```
#    env - `cat /entrypoint_envs` /bin/bash
#    ```
# - or -
# 2. Source the environment variables from /proc/${PID}/environ as part of the cron job command using:
#    ```
#    while IFS= read -r -d "" PWD; do export "$PWD"; done </proc/1/environ
#    ```
#
# Usage of `cron`: https://manpages.debian.org/buster/cron/cron.8.en.html
#
# 1.
# command=bash -c 'env > /entrypoint_envs && rm -f /var/run/crond.pid -- && cron -f -L 15'
# 2.
command=bash -c 'env > /entrypoint_envs && rm -f /var/run/crond.pid -- && cron -f -L 15'
environment=
    COMPOSE_PROJECT_NAME=%(ENV_COMPOSE_PROJECT_NAME)s
stdout_logfile=/dev/fd/1
stdout_logfile_maxbytes=0
stderr_logfile=/dev/fd/2
stderr_logfile_maxbytes=0
autorestart=true

[program:sshd]
command=/usr/sbin/sshd -D
stdout_logfile=/dev/fd/1
stdout_logfile_maxbytes=0
stderr_logfile=/dev/fd/2
stderr_logfile_maxbytes=0
autorestart=true
