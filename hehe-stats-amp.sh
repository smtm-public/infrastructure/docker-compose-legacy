#!/bin/bash

docker stats \
    default-portainer \
    default-traefik \
    default-apache24 \
    default-php73-fpm \
    default-mariadb \
    default-mysql \
